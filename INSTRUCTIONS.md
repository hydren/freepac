## Instructions

### How to play
-----------------

There are 2 game modes, the campaign (through **Play** option) and casual (through **Stages** option). The latter gives the player the option to choose the stage (available on */stages/* folder). The gameplay is slightly similar to Pacman series of games. 

### Customizing
-----------------

It is possible to customize the UI, the characters sprites and enemies. Its also possible to add custom stages.

##### 1. User interface

The UI can be customized by replacing the assets located on assets folder, or can be specified on the *resources.properties* file.

    bg_title=assets/ui/title.jpg
    bg_title_wide=assets/ui/title-wide.jpg
    ...
    mus_title=
    mus_intro=assets/ui/intro.ogg
    ...
    ui_ingame_menu_bg_color=255,0,0
    ui_ingame_menu_font_color=255,255,255
    ui_ingame_menu_selected_color=255,117,24

##### 2. Playable characters

On the *resources.properties* file, its possible to specify playable characters:

    anim_player1=assets/paccy.png
    anim_player1.size=36
    anim_player1.time=100
    anim_player1.name=Paccy
    anim_player1.type=semisymmetric
    anim_player1_char=assets/paccy.png
    anim_player1_char.size=36
    anim_player1_char.time=100
    ...

Currently, each character must be added using the `anim_playerX` notation, where X is a (unique) integer. Further properties are accessed using sufixes to the notation (i.e. `anim_playerX.size`). The following properties are supported:

- No suffix: specify the character sprite sheet filename.
- `.size`: specify the animation dimensions within the sprite sheet. It's expected pair of integers (width, height). When only one integer is provided, it's assumed that it is both the width and height (and width=height). The game will attempt to load all possible frames horizontaly within the sprite sheet image, each with the given dimensions.
- `.time`: specify total animation duration in milliseconds. The animation will loop, each one during the given amout of milliseconds.
- `.name`: The name shown on the main menu when selecting the character.
- `.type`: The *type* of sprite symmetry. Possible values are `symmetric` (default), `semisymmetric` and `asymmetric`. 
- `_char`: The character sprite sheet filename to be shown on the main menu (like a presentation mode).
- `_char.size`: Same as `.size`, but applies for the main menu presentation animation.
- `_char.time`: Same as `.time`, but applies for the main menu presentation animation.
- `.unlocked`: (Optional) Specify which level (number) unlocks this character.

###### Sprite symmetry type
Regarding the sprite symmetry type specified when specifying characters, using different values have different results: 

- Using `symmetric` makes the game assumes the animation is **horizontally** symmetric. 
- Using `semisymmetric` assumes it is **not horizontally** but **rotationally** symmetric. 
- Using `asymmetric` assumes that the sprite it **not symmetric** at all and that 4 rows of sprites are to be read on the sprite sheet, each one as the facing **right**, **down**, **left** and **up** animations, respectively.

##### 3. Enemies (types)

By default, the default enemies (*Default_0*, *Default_1*, *Default_2* and *Default_3*) plus *Dummy* enemy types are available to be used on the stages. On the *enemies.conf.properties* file, its possible to specify additional enemy types available to be used on the stages:

    custom_enemy0=Hulky
    custom_enemy0.behavior=dizzy
    custom_enemy0.anim=assets/enemies/hulky.png
    custom_enemy0.anim.size=40
    custom_enemy0.anim.time=100

Currently, each enemy type must be specified using the `custom_enemyX` notation, where **X** is a unique integer between enemy types. Further properties are accessed using sufixes to the notation (i.e. `custom_enemyX.behavior`). The following properties are supported currently:

- No suffix: specify the enemy type name.
- `.anim`: specify the enemy sprite sheet filename. Note: enemies animations are always assumed to be **asymmetric**.
- `.anim.size`: Same as `.size`, from characters.
- `.anim.time`: Same as `.time`, from characters.
- `.behavior`: specify the enemy behavior. Possible values are liste in the table below.
- `.behavior.fast`: (Optional) If specified as **true**, makes enemies with this type 50% faster.
- `.behavior.invulnerable`: (Optional) If specified as **true**, makes enemies with this type invulnerable even when the player gets power pellets.
- `.behavior.variable_speed`: (Optional) If specified as **true**, makes enemies with this type have variable speed, oscillating between 70% and 130% speed.

###### Enemy Behavior

All enemies follow a standard behavior set during gameplay. These are:

| Released | Beaten | Passed fence | Frightened | Scatter mode | Action                                        |
|:--------:|:-----:|:------------:|:----------:|:------------:|-----------------------------------------------|
|    no    |   -   |       -      |      -     |       -      | Stay inside enemy house until released          |
|    yes   |  yes  |      yes     |      -     |       -      | Return to enemy house (jumping eyes)            |
|    yes   |  yes  |      no      |      -     |       -      | Enter enemy house (slowly enter house)          |
|    yes   |   no  |      no      |      -     |       -      | Leave enemy house (slowly leave house)          |
|    yes   |   no  |      yes     |     yes    |       -      | Wander randomly (frightened mode)             |
|    yes   |   no  |      yes     |     no     |      yes     | Targets a map corner (scatter mode)           |
|    yes   |   no  |      yes     |     no     |      no      | Custom action (enemy type specified behavior) |

When the enemy is not performing an action from the standard behavior (enforced by the game rules), it performs a custom behavior depending on its enemy type. Therefore, each enemy type has its enemy behavior. When specifing a custom enemy type, you need to specify its enemy behavior.

The following enemy behaviors are supported currently:

- `chase`: chases the player.
- `scatter`: chases one the stages corners, according to the enemy order of release.
- `random`: wanders randomly.
- `avoid`: tries to stay away from the player as far as possible.
- `dizzy`: wanders randomly on the map without grip snapping restrictions. Movement is like a brownian motion.
- `chase_ahead`: chases one tile ahead the player (in the direction at which the player is faced).
- `chase_ahead_2`: chases two tiles ahead the player (in the direction at which the player is faced).
- `chase_projection_first_enemy_on_player`: targets a mirrored vector difference between first enemy and 2 tiles ahead the player.
- `pokey`: when 8 tiles away from player, chases the player. When closer to that, behaves like scatter mode.
- `pokey_2x`: when 16 tiles away from player, chases the player. When closer to that, behaves like scatter mode.
- `random_corner_scatter`: targets a point in one of the corners of the map, similar to `scatter` The target corner shifts every 1 minute. 
- `pokey_slower_when_close_faster_when_far`: same as `pokey`, but slows down (62%) when close and speeds up (160%) when far.
- `chase_slower_when_close`: same as `chase`, but slows down when closer to 8 tiles.
- `chase_ahead_slower_when_close`: same as `chase_ahead`, but slows down when closer to 8 tiles.
- `halt_when_lost_target`: stands still when player is out of sight. Otherwise chases the player at 2x speed.
- `chase_faster_when_acquire_target`: same as `chase`, but speeds up 2x when player is in sight.
- `chase_faster_when_acquire_target_intermitent_teleport`: (experimental) same as `chase_faster_when_acquire_target`, but intermitently teleports to another location in the map.
- `chase_passing_walls_slow`: chases the player passing through walls (except map borders).

Classic behaviors
- `classic_shadow`: a behavior similar to classic *Blinky*'s behavior. Similar to `chase`.
- `classic_speedy`: a behavior similar to classic *Pinky*'s behavior. Similar to `chase_ahead_2`, but with classic Pinky's target glitch.
- `classic_bashful`: a behavior similar to classic *Inky*'s behavior. Similar to `chase_projection_first_enemy_on_player`, but with classic Pinky's target glitch.
- `classic_pokey`: a behavior similar to classic *Clyde*'s behavior. Similar to `pokey`.

The following enemy types are embedded and available by default:

| Enemy type |               Enemy behavior             |
|:----------:|:----------------------------------------:|
|  Default_1 |                  `chase`                 |
|  Default_2 |              `chase_ahead_2`             |
|  Default_3 | `chase_projection_first_enemy_on_player` |
|  Default_4 |                  `pokey`                 |
|  Classic_1 |              `classic_shadow`            |
|  Classic_2 |              `classic_speedy`            |
|  Classic_3 |             `classic_bashful`            |
|  Classic_4 |              `classic_pokey`             |
|    Dummy   |                  `random`                |

##### 4. Stages

Its possible add any number of custom stages. All stages are stored in the /stages folder. A stage is composed of a `.def` file and a `.map` file. The `.def` file contains stage specifications:

    name=Classic Stage
    width=28
    height=31
    color=19,53,224
    enemies=classic

The `.map` file contains the stage map:

    ############################
    #............##............#
    #.####.#####.##.#####.####.#
    #*####.#####.##.#####.####*#
    #.####.#####.##.#####.####.#
    #..........................#
    #.####.##.########.##.####.#
    #.####.##.########.##.####.#
    #......##....##....##......#
    ######.##### ## #####.######
    /////#.##### ## #####.#/////
    /////#.##     @    ##.#/////
    /////#.## ###==### ##.#/////
    ######.## #      # ##.######
    !´´´´´.   # @ @ @#   .´´´´´!
    ######.## #      # ##.######
    /////#.## ######## ##.#/////
    /////#.##          ##.#/////
    /////#.## ######## ##.#/////
    ######.## ######## ##.######
    #............##............#
    #.####.#####.##.#####.####.#
    #.####.#####.##.#####.####.#
    #*..##....... $.......##..*#
    ###.##.##.########.##.##.###
    ###.##.##.########.##.##.###
    #......##....##....##......#
    #.##########.##.##########.#
    #.##########.##.##########.#
    #..........................#
    ############################

###### Stage definition properties

The stage definition file contains the following properties:

- `name`: the stage's name.
- `width`: the stage's width.
- `height`: the stage's height.
- `color`: the wall color. Can be in RGB notation (i.e. 224,32,32), hexadecimal (i.e. #EE5522) or literal (i.e. red)
- `enemies`: the enemies to be spawned on stage.
- `background`: (optional) the stage's background. Can be either an image or a color.
- `style`: (optional) the style of the wall's shape. Possible values are **rounded** (default), **sloped** and **squared**.
- `song`: (optional) a music filename to be played on stage.
- `nointrotune`: (optional) if specified as **true**, the intro melody is not played, skipping to the stage song (if any was specified).

Obs: prevent from setting the `color` property as `rainbow` if you are prone to epileptic seizures.

###### Stage enemies

The `enemies` property accepts either `classic`, `dummy` or a comma-separeted list of enemy types. 

- `classic` specifies that the stage should have the classic enemies. By default, they are the default enemies with classic behavior.
- `dummy` specifies that the stage should have "Dummy" enemies (same as Dummy,Dummy,Dummy,Dummy)
- A comma-separeted list of enemy types (name) specifies each enemy to be spawned. Be sure to only include enemy types registered on the *enemies.conf.properties* file, or the embedded types. Valid example: `enemy=default_1,default_3,dummy,custom1`. (`custom1` should be a enemy type specified in *enemies.conf.properties* file).

Obs: When the difficulty is set to "Classic", the `enemies` property is ignored and the classic enemies are spawned instead.
Obs.2: Specifying less than 2 enemies causes *undefined behavior*.

###### Stage map

The stage map is defined as a matrix of characters in a plain text file. Each char represents what is populated at that position. The valid characters are the following:

- `' '`: empty space is interpreted as a empty slot.
- `'#'`: a hash is interpreted as a wall block.
- `'/'`: a forward slash is interpreted as a empty slot outside walkable area.
- `'.'`: a dot is interpreted as a pellet-filled slot.
- `'*'`: an asterisk is interpreted as a power pellet-filled slot.
- ``'`'``: a grave accent is interpreted as a slot that is a enemy slow zone.
- `','`: a comma is interpreted as a pellet-filled slot that is also a enemy slow zone.
- `'='`: an equals sign is interpreted as a enemy house fence/door.
- `'!'`: an exclamation mark is interpreted as a horizontal warp.
- `'¡'`: an inverted exclamation mark is interpreted as a vertical warp.
- `'@'`: an "at" sign is interpreted as a enemy spawn. The enemies are spawned according to the order the '@' chars are read.
- `'$'`: a dolar sign is interpreted as a player start point.

Obs: Warps must have a axis-aligned corresponding warp, or else there will be *undefined behavior*.

The number of rows and columns must match the map dimensions. In other words, `width`·`height` = #rows · #columns, or else the game will **abort**.

###### Stage playlist

In the */stages/* folder there must be a file called *playlist.txt*, which should contain the career mode stages list as a list of `.def`s files. The following exemplifies:

    stage01.def
    stage02.def
    stage03.def
    stage04.def
    stage05.def
