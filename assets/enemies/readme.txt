Images drawn by Philipp Lenssen: scirita.png, skeleton.png, sorcerer.png, spider.png
Small edits made by hydren, mainly to fit the image size.

License:
http://creativecommons.org/licenses/by-nc/2.5/

For more info please see
http://blog.outer-court.com/archive/2006-08-08-n51.html