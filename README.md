# freepac

### What is it?
---------------

FreePac is a simple game inspired on [Pac-Man](https://en.wikipedia.org/wiki/Pac-Man). 

It is a open-source fork from personal code. Its coded in java, using [Slick2d](http://slick.ninjacave.com/) and [javatuples](http://www.javatuples.org/) libraries.

This game supports customization through configuration files and is highly themeable.

### Installation / Running
---------------------------

To run the game, it needs at least Java 6 (standard) installed.

Just extract the archive freepac-*version*.zip to a folder of your choice. Afterwards, run:

    java -jar freepac.jar

On Windows, just run the **freepac.exe** executable

### Source
---------------

The code is in a [Eclipse](https://www.eclipse.org/) project structure. Grab a clone of this repo and use the [Existing projects wizard](http://help.eclipse.org/juno/index.jsp?topic=%2Forg.eclipse.platform.doc.user%2Ftasks%2Ftasks-importproject.htm).

The game depends on the [Slick2d](http://slick.ninjacave.com/) and [javatuples](http://www.javatuples.org/) libraries. You need to download and set up them as [User libraries](http://help.eclipse.org/juno/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2Freference%2Fpreferences%2Fjava%2Fbuildpath%2Fref-preferences-user-libraries.htm).

##### For more information, look at the [instructions](https://gitlab.com/hydren/freepac/blob/master/INSTRUCTIONS.md).
