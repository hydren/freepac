package states;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import main.FreePacGame;
import util.ColorExtra;
import util.Util;
import util.drawing.ScrollingBackground;

public class StageClearState extends BasicGameState {
	@Override public int getID() { return StateSpec.STAGE_CLEAR_STATE.id; }

	ScrollingBackground bg;
	FreePacGame game;
	Font customFont, customFont2;
	String unlocks;
	Color colorScore, colorFlash1, colorFlash2;
	
	@Override
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		this.game = (FreePacGame) sbg;
		bg = new ScrollingBackground("bg_stage_clear", game.resources);
		customFont = Util.deriveFont((Font) game.resources.get("fnt_ui2"), java.awt.Font.BOLD, 64);
		customFont2 = Util.deriveFont((Font) game.resources.get("fnt_ui1"), java.awt.Font.BOLD, 32);
		colorScore = ColorExtra.toColor(game.resources.getProperty("ui_stage_clear_font_color"), Color.magenta);
		colorFlash1 = ColorExtra.toColor(game.resources.getProperty("ui_stage_clear_flash_color1"), ColorExtra.violet);
		colorFlash2 = ColorExtra.toColor(game.resources.getProperty("ui_stage_clear_flash_color2"), ColorExtra.getContrastColor(colorFlash1));
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		final float w = container.getWidth(), h = container.getHeight();
		bg.draw(container);
		g.setFont(customFont);
		g.setColor( System.currentTimeMillis()%125<62? colorFlash1 : colorFlash2);
		g.drawString("Stage cleared!", w*0.3f, h*0.4f);
		g.setColor(colorScore);
		g.drawString("Score: "+FreePacGame.score , w*0.25f + 100, h*0.5f);
		g.setFont(customFont2);
		g.setColor( System.currentTimeMillis()%125<62? colorFlash1 : colorFlash2);
		if(unlocks != null) g.drawString("Unlocked characters: "+unlocks+"!", w*0.23f, h*0.65f);
		Display.sync(60);
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException
	{
		if(PlayStageState.isCareer) FreePacGame.Settings.unlocked = (byte) PlayStageState.stageNumber;
		unlocks = ((MenuState) game.getState(StateSpec.MENU_STATE.id)).unlockedCharacter((FreePacGame) game);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		bg.update(delta, container);
	}
	
	@Override
	public void keyPressed(int key, char c)
	{
		switch (key) {
		    case Input.KEY_ESCAPE:
		    case Input.KEY_ENTER:
		    	if(game.getStageNumber(PlayStageState.stageFilename) == -1) //map skimirsh with non-campaign map
		    		game.enterState(StateSpec.MENU_STATE, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
		    	else if(PlayStageState.stageNumber == game.stagePlaylist.size())
		    		game.enterState(StateSpec.GAME_WON_STATE, new FadeOutTransition(Color.white), new FadeInTransition(colorScore));
		    	else
		    	{
		    		PlayStageState.stageFilename = game.stagePlaylist.get(++PlayStageState.stageNumber-1);
		    		game.enterState(StateSpec.PLAY_STAGE_STATE, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
		    	}
		    default:
		        break;
		}
	}
}
