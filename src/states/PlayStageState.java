package states;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Stack;

import javax.swing.JOptionPane;

import org.javatuples.Pair;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.util.FastTrig;

import entities.Actor;
import entities.Block;
import entities.Block.Type;
import entities.Enemy;
import entities.Enemy.CustomEnemySpecification;
import entities.Enemy.EnemyType;
import entities.Item;
import entities.PlayableCharacter;
import main.Main;
import main.FreePacGame;
import util.ColorExtra;
import util.Direction;
import util.Menu;
import util.Timer;
import util.Util;
import util.drawing.CustomGraphics;
import util.drawing.FreePacWallDrawer;
import util.drawing.SimpleWallDrawer;
import util.drawing.RoundedEdgeWallDrawer;
import util.drawing.SlopedEdgeWallDrawer;

public class PlayStageState extends BasicGameState
{
	@Override public int getID() { return StateSpec.PLAY_STAGE_STATE.id; } //STATE ID
	
	/** The default power mode duration (in milliseconds) */
	public static long POWER_MODE_DURATION = 6500;
	
	/** The default durations for scatter/chase mode alternations */
	private static final long[] DEFAULT_SCATTER_MODE_DURATIONS = {7000, 20000, 7000, 20000, 5000, 20000, 5000, -1 };
	
	/** The default score values for eating bonus pellets, according to levels */
	private static final int[] DEFAULT_BONUSES_VALUES = { 100, 250, 500, 750, 1000, 2000, 3000, 5000, 7000, 9000, 10000, 20000, 30000, 50000, 70000, 100000, 200000, 300000, 500000, 750000, 1000000, 2000000, 3000000, 4000000, 5000000 };
	
	private static final int[] DEFAULT_EXTRA_LIFE_SCORE_NEEDED = { 10000, 50000, 100000, 500000, 1000000, 5000000, 10000000, 50000000, 100000000 };
	
	/** The default pellet count to release next enemy, in order of release. */
	private static final int[] DEFAULT_ENEMY_PELLET_COUNT_FOR_RELEASE = { 0, 0, 30, 60, 90, 120, 150, 200, 250, 300};
	
	/** The default pellet abstinence timeout to release next enemy. 
	 * Pellet abstinence timeout (PAT) is the time limit for releasing the next enemy when player is not eating pellets. */
	public static long ENEMY_PELLET_ABSTINENCE_TIMEOUT = 4000;
	
	/** Block size parameter. */
	public static int BLOCK_SIZE = 24;
	
	/** The intensity of the hud's shaking movement, in pixels. This value should be able to tune in some kind of options */
	public static int HUD_SHAKE_INTENSITY = 10;
	
	public static int PELLET_REWARD = 10, ENERGIZER_REWARD = 50;
	
	/** The stage's map as a grid of positions/slots. */
	public Block[][] grid;
	
	public boolean[][] slowZones;
	
	public static Block OUTSIDE_MAP = new Block(-1, -1, -1, -1, null);
	
	public static float WAVE_EFFECT_MAGNETUDE = 1;
	
	/** The map's height, initialized at loadStage() */
	int mapHeight; 
	int mapWidth;
	
	/** The map's offset on screen */
	public Vector2f mapOffset = new Vector2f(0,0);
	
	public Vector2f mapLeftTopPosition, mapRightTopPosition, mapRightBottomPosition, mapLeftBottomPosition;
	
	public PlayableCharacter player;
	public List<Enemy> enemies;
	HashSet<Item> items;
	
	/** The direction wanted by the player */
	public Direction wantedDir = null;
	public Timer wantedDirTimer;
	
	int pelletsEaten, pelletCount, enemiesBeatenStreak;
	
	Timer shakeHUDTimer = new Timer(500);
	
	String stageName, stageTileStyle = "rounded";
	public Color mapCustomColor = Color.transparent, mapCustomColorContrasted = Color.transparent;
	Color scoreBonusShowTextColor1 = Color.cyan, scoreBonusShowTextColor2 = Color.cyan.darker();
	Color bgColor = Color.black;
	
	/** Flag to indicate whether the game is paused. Its useful to use when returning to this game state to know whether to resume or start a fresh level.*/
	public boolean paused = false;
	long pauseTime;
	boolean stageWon;
	
	public static boolean isCareer;
	
	/** <i> Used to flag that resources are not fully loaded, so no update is done during the delay. </i>*/
	boolean stateEnterDelayFlag = false;
	long stateEnterDelayTimer = 0;
	
	/** Switch to inform whether enemies are edible to player */
	boolean powerMode = false;
	/** Timer to power mode duration control. */
	public Timer powerModeTimer;
	
	/** Timer to control the switch between enemies' chase mode and scatter mode */
	Timer scatterModeTimer;
	int scatterModeCounter;
	long[] scatterModeIntervals;
	
	/** If true, the player death event is running. */
	public boolean playerDeathEventMode = false;	
	boolean playerDeathEventSoundPlayed = false;
	public Timer playerDeathEventTimer;
	
	/** If true, a enemy death event is running. */
	public boolean enemyDeathEventMode = false;
	Timer enemyDeathEventTimer;
	Enemy currentBeatenEnemy = null;
	
	/** If true, the match start event is running. */
	boolean matchStartEventMode = false;
	Timer matchStartEventTimer;
	
	/** A position in the grid that is the first block outside the enemy house. It is used to guide the enemies in or out of the enemy house. */
	public Pair<Integer, Integer> fenceExitSlot;
	
	/** The time from which the current life started. Each time player dies, this variable is updated. */
	long playerLifeStartTime=0;
	int enemyReleaseCounter;
	Timer pelletAbstinenceTimer, enemyReleaseMinimumDelayTimer;
	
	int bonusSpawnedCounter=0;
	Timer bonusAvailabilityTimer;
	Item bonusItem = null;
	Vector2f lastBonusEatenPosition = null;
	Timer lastBonusEatenScoreShowTimer = new Timer(2000);
	int lastBonusEatenScoreValue = 0;
	
	/** If true, skip playing the classic intro tune/song and plays the stage's song immediately after match start */
	boolean noIntroTune = false;
	
	//XXX dirty hack. There should be a setStage method.
	/** The current stage number. Change this to change the stage started next time. */
	public static int stageNumber;
	public static String stageFilename = null;
	
	/** Reference to the game instance, stored for later use */
	public FreePacGame game;
	
	public boolean ingameMenuMode=false;
	Menu ingameMenu;
	long ingameMenuEntererTime;
	
	public boolean rainbowColored = false;
	
	//============================= Resources ================================
	
	private Image bg; //background
	
	private Image uiReady;
	
	private Image uiPlayerLife;
	
	private Image imgOpcoes;
	
	/** The sound for player walking while eating a pellet */
	private Sound soundEatPellet;
	
	/** The sound for any actor entering a warp */
	private Sound soundWarped;
	
	/** The sound for player eating a power pellet */
	private Sound soundEatPowerPill;
	
	/** The sound for enemies wandering */
	private Sound soundEnemyBuzzer;
	
	/** The sound for enemies wandering in fright mode */
	private Sound soundEnemyScaredBuzzer;
	
	
	public Sound soundEnemyBeatenBuzzer;
	
	/** The sound for player being caught */
	private Sound soundPlayerCaught;
	
	/** The sound for player dying */
	private Sound soundPlayerDying;
	
	/** The sound for player eating a enemy */
	private Sound soundBeatEnemy;
	
	/** The sound played when the last pellet was eaten */
	private Sound soundLastPellet;
	
	/** The sound played when the stage is being faded into the StageClearState */
	private Sound soundVictoryFade;
	
	private Sound soundBonusPellet;
	
	private Sound soundLifeUp;

	/** The music for the match introduction */
	private Music musicIntro;

	/** The current music variable. The instance is different for each map. */
	private Music musicStage;
	private float volumeMusicStage;
	
	/** Sounds for ingame menu. */
	private Sound soundCursor, soundMenuIn, soundMenuOut;
	
	/** Custom "cute" font. */
	private Font fontHUD, fontHUD2;
	
	/** Animation to cast on player when he finishes the stage */
	private Animation animWinningStars;
	
	//=========================================================================
	
	public PlayStageState() {}
	
	/** Loads all stuff on program startup */
	@Override
	public void init(GameContainer container, StateBasedGame sbgame) throws SlickException 
	{
		this.game = (FreePacGame) sbgame;
		
		soundEatPellet =  (Sound) game.resources.get("snd_eat_pellet");
		soundWarped = (Sound) game.resources.get("snd_warped");
		soundEatPowerPill = (Sound) game.resources.get("snd_power_mode");
		soundEnemyBuzzer = (Sound) game.resources.get("snd_enemy_buzzer");
		soundEnemyScaredBuzzer = (Sound) game.resources.get("snd_enemy_frightebed_buzzer");
		soundEnemyBeatenBuzzer = (Sound) game.resources.get("snd_enemy_beaten_buzzer");
		soundPlayerCaught = (Sound) game.resources.get("snd_player_caught");
		soundPlayerDying = (Sound) game.resources.get("snd_player_dying");
		soundBeatEnemy = (Sound) game.resources.get("snd_enemy_beaten");
		soundLastPellet = (Sound) game.resources.get("snd_last_pellet");
		soundVictoryFade = (Sound) game.resources.get("snd_victory_fade");
		soundBonusPellet = (Sound) game.resources.get("snd_bonus_pellet");
		soundLifeUp = (Sound) game.resources.get("snd_life_up");
		
		soundCursor = (Sound) game.resources.get("snd_cursor");
		soundMenuIn = (Sound) game.resources.get("snd_menu_in");
		soundMenuOut = (Sound) game.resources.get("snd_menu_out");
		
		musicIntro = (Music) game.resources.get("mus_intro");
		
		uiReady = (Image) game.resources.get("img_txt_ready");
		
		imgOpcoes = (Image) game.resources.get("img_txt_options");
		
		fontHUD = Util.deriveFont((Font) game.resources.get("fnt_ui1"), java.awt.Font.PLAIN, 36);
		fontHUD2 = Util.deriveFont((Font) game.resources.get("fnt_sans"), java.awt.Font.BOLD, 16);
		
		animWinningStars = (Animation) game.resources.get("anim_fx_starry1");
		
		uiPlayerLife = null;
		
		ingameMenu = new Menu(new Rectangle(container.getWidth()/2 - 200, container.getHeight()/2 - 150, 400, 300));
		ingameMenu.entries.add(new Menu.Entry(" Resume"));
		ingameMenu.entries.add(new Menu.Entry(" Return to main menu"));
		ingameMenu.entries.add(new Menu.Entry(" Exit to desktop"));
		ingameMenu.setBgColor(ColorExtra.toColor(game.resources.getProperty("ui_ingame_menu_bg_color"), ColorExtra.red).addToCopy(new Color(0, 0, 0, -63)));
		ingameMenu.itemsHeight = fontHUD.getLineHeight()*1.2f;
		ingameMenu.setFontColor(ColorExtra.toColor(game.resources.getProperty("ui_ingame_menu_font_color"), Color.white));
		ingameMenu.setSelectedColor(ColorExtra.toColor(game.resources.getProperty("ui_ingame_menu_selected_color"), ColorExtra.pumpkin));
		ingameMenu.paddingTop = 80;
		
		scoreBonusShowTextColor1 = ColorExtra.toColor(game.resources.getProperty("game_bonus_font_color"), Color.cyan);
		scoreBonusShowTextColor2 = scoreBonusShowTextColor1.darker();
		
		for(int k=0; game.resources.get("img_goodie"+k) != null; k++); //pre-load goodies
	}
	
	/** Executes when entering game state */
	@Override
	public void enter(GameContainer container, StateBasedGame sbgame) throws SlickException
	{
		System.out.println("**********************************************************************************************");
		System.out.println("**********************************************************************************************");
		System.out.println("**********************************************************************************************");
		System.out.println("Prompted game match. Preparing...");
		System.out.println("Container size "+container.getWidth() + ", " + container.getHeight());
		
		if(paused)
		{
			System.out.println("Stage is loaded. Resumed match...");
			paused = false;
			stateEnterDelayFlag = true;
			return;
		}
		
		
		stageNumber = game.getStageNumber(stageFilename);
		System.out.println("Stage " + stageFilename + (stageNumber==-1? " is not a campaign stage" : " is campaign stage #" + stageNumber));
		if(stageNumber == -1) stageNumber = 1;
		
		try 
		{
			System.out.println("Loading stage " + stageFilename + "...");
			loadStage(stageFilename);
		} 
		catch (Exception e) 
		{
			System.out.println("Error while loading stage file! "+e.getLocalizedMessage());
			JOptionPane.showMessageDialog(null, "Error while loading stage file! "+e.getLocalizedMessage(), "Error while loading stage file!", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			Main.closeGame(game, 1);
		}
		
		System.out.println("Initializing parameters...");
		pelletsEaten = 0;
		enemiesBeatenStreak = 0;
		
		// set free first and second enemies (if they exists)
		enemyReleaseCounter = 0;
		releaseNextEnemy();
		releaseNextEnemy();
		pelletAbstinenceTimer = new Timer(computePelletAbstinenceTimeout());
		enemyReleaseMinimumDelayTimer = new Timer(pelletAbstinenceTimer.interval);
		System.out.println("Pellet abstinence timeout/minimum delay : " + pelletAbstinenceTimer.interval);
		
		stageWon = false;

		bonusAvailabilityTimer = new Timer(9000 + System.currentTimeMillis()%1000);
		bonusSpawnedCounter = 0;
		
		playerLifeStartTime = System.currentTimeMillis();
		mapOffset.y = (container.getHeight() - mapHeight)/2;
		mapOffset.x = (container.getWidth() - mapWidth)/2;
		
		powerMode = false;
		powerModeTimer = new Timer(POWER_MODE_DURATION);
		
		playerDeathEventMode = false;
		playerDeathEventTimer = new Timer(3000);
		
		enemyDeathEventMode = false;
		enemyDeathEventTimer = new Timer(1000);
		currentBeatenEnemy = null;
		
		matchStartEventMode = true;
		matchStartEventTimer = new Timer(1500);
		
		scatterModeCounter = 0;
		scatterModeIntervals = DEFAULT_SCATTER_MODE_DURATIONS; //default values
		scatterModeTimer = new Timer(scatterModeIntervals[scatterModeCounter++]);
		
		System.out.println("Game is ready.");
		
		stateEnterDelayTimer = System.currentTimeMillis();//kludge to halt the game update while loading assets
		stateEnterDelayFlag = true; //kludge to halt the game update while loading assets
		
		if(noIntroTune && !FreePacGame.Settings.muteMusic && musicStage != null)
			musicStage.play();
		else if(musicIntro != null)
			musicIntro.play();
		
		wantedDir = null;
		if(FreePacGame.Settings.wantedDirTimeout > 0)
			wantedDirTimer = new Timer(FreePacGame.Settings.wantedDirTimeout);
		else
			wantedDirTimer = null;
		
		System.gc();
	}
	
	@Override
	public void leave(GameContainer container, StateBasedGame game)
	{
		stateEnterDelayFlag = false; //kludge to halt the game update while loading assets
		ingameMenuMode = false;
		System.gc();
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics graphics) throws SlickException 
	{
		if(!stateEnterDelayFlag) return; //kludge to halt the game update while loading assets
		CustomGraphics g = new CustomGraphics(graphics);
		g.setFont(fontHUD);
		g.setAntiAlias(FreePacGame.Settings.useAntialiasing);
		
		if(rainbowColored)
		{
			mapCustomColor = ColorExtra.getRandomColor();
			mapCustomColorContrasted = ColorExtra.getContrastColor(mapCustomColor);
		}
		
		g.setColor(bgColor);
		g.fillRect(0,0,container.getWidth(), container.getHeight());
		
		if(bg != null)
			bg.draw(0,0,container.getWidth(), container.getHeight()); //draw scaled
		
		g.translate(mapOffset.x, mapOffset.y);
		
		for(Block[] blockColumn : grid) for(Block block : blockColumn)
		{
			if(block == null) continue;
			
			if(block == OUTSIDE_MAP) continue;
			
			if(Main.debugMode)
			{
				if(block.marked) { g.setColor(Color.red); block.marked = false; }
				else g.setColor(block.customColor);
				g.draw(block);
			}
			
			block.draw(this, g);
		}
		
		synchronized (items) 
		{
			for(Item i : items)
			{
				i.draw(g);
			}
		}
			
		for(Enemy enemy : enemies) if(enemy != currentBeatenEnemy)
		{
			enemy.draw();

			//DEBUG enemy body
//			g.setColor(ColorExtra.glassSmoke);
//			g.fillOval(enemy.body.getX(), enemy.body.getY(), enemy.body.radius*2, enemy.body.radius*2);
//			g.setColor(Color.black);
//			g.drawOval(enemy.body.getX(), enemy.body.getY(), enemy.body.radius*2, enemy.body.radius*2);
//			g.setColor(Color.pink);
//			g.fillOval(enemy.body.getCenterX()-1, enemy.body.getCenterY()-1, 2, 2);
			
			//DEBUG enemy status icon
//			g.setColor(enemy.debugColor);
//			g.fillOval(enemy.body.getX(), enemy.body.getY(), enemy.body.radius*0.5f, enemy.body.radius*0.5f);
//			g.setColor(Color.black);
//			g.drawOval(enemy.body.getX(), enemy.body.getY(), enemy.body.radius*0.5f, enemy.body.radius*0.5f);
	
			//DEBUG enemy target (slow)
//			g.setFont(fontHUD2);
//			Vector2f resolvedTarget = enemy.enemyType.behavior.getResolvedTargetLocation(enemy);
//			if(resolvedTarget!=null) 
//			{
//				switch (enemies.indexOf(enemy)) {
//					case 0: g.setColor(Color.red.addToCopy(new Color(0,0,0,-128))); break;
//					case 1: g.setColor(ColorExtra.rosePink.addToCopy(new Color(0,0,0,-128))); break;
//					case 2: g.setColor(Color.cyan.addToCopy(new Color(0,0,0,-128))); break;
//					case 3: g.setColor(Color.orange.addToCopy(new Color(0,0,0,-128))); break;
//					default: g.setColor(Util.getRandomColor());break;
//				}
//				g.setLineWidth(4);
//				g.drawLine(enemy.body.getCenterX(), enemy.body.getCenterY(), resolvedTarget.x, resolvedTarget.y);
//			}
			
			//DEBUG enemy A* path
//			if(enemy.aux1 != null)
//			{
//				@SuppressWarnings("unchecked")
//				final List<Pair<Integer, Integer>> path = (List<Pair<Integer, Integer>>) enemy.aux1;
//				g.setFont(fontHUD2);
//				g.setColor((enemies.indexOf(enemy)==0?Color.red:enemies.indexOf(enemy)==1?Color.pink:enemies.indexOf(enemy)==2?Color.cyan:Color.orange).addToCopy(new Color(0, 0, 0, -128)));
//				for(Pair<Integer, Integer> pair : path)
//				{
//					g.fillRect(pair.getValue1() * BLOCK_SIZE, pair.getValue0() * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
//					g.drawString(" "+(path.size()-path.indexOf(pair)), pair.getValue1() * BLOCK_SIZE, pair.getValue0() * BLOCK_SIZE);
//				}
//			}
		}
		else
		{
			Font prevFont = g.getFont();
			g.setFont(fontHUD2);
			g.setColor(System.currentTimeMillis()%250>125?scoreBonusShowTextColor1:scoreBonusShowTextColor2);
			final String txtScoreBonus = ""+((int)(Math.pow(2, enemiesBeatenStreak)*100));
			g.drawString(txtScoreBonus, enemy.body.getCenterX() - fontHUD2.getWidth(txtScoreBonus)/2, enemy.body.getCenterY() - fontHUD2.getLineHeight()/2);
			g.setFont(prevFont);
		}
		
		if(lastBonusEatenPosition != null && ! lastBonusEatenScoreShowTimer.timeCompleted())
		{
			Font prevFont = g.getFont();
			g.setFont(fontHUD2);
			g.setColor(System.currentTimeMillis()%250>125?scoreBonusShowTextColor1:scoreBonusShowTextColor2);
			final String txtScoreBonus = ""+lastBonusEatenScoreValue;
			g.drawString(txtScoreBonus, lastBonusEatenPosition.x - fontHUD2.getWidth(txtScoreBonus)/2, lastBonusEatenPosition.y - fontHUD2.getLineHeight()/2);
			g.setFont(prevFont);
		}
				
		
		//DEBUG player slot
//		g.setColor(new Color(Color.green).scaleCopy(0.5f));
//		g.fillRect(player.getSlotPosition().getValue1()*BLOCK_SIZE, player.getSlotPosition().getValue0()*BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
			
		//DEBUG player slot center point
//		g.setColor(Color.cyan);
//		g.drawOval(player.getSlotPositionCenterPoint().x-1, player.getSlotPositionCenterPoint().y-1, 2, 2);
			
		player.draw();
		if(stageWon) animWinningStars.draw(player.body.getX()-BLOCK_SIZE, player.body.getY()-BLOCK_SIZE, 64, 64);
			
		//DEBUG player body
//		g.setColor(Color.orange);
//		g.drawOval(player.body.getX(), player.body.getY(), player.body.radius*2f, player.body.radius*2f);
			
		//DEBUG player body center point
//		g.setColor(Color.red);
//		g.drawOval(player.body.getCenter()[0]-1, player.body.getCenter()[1]-1, 2, 2);
		
		if(matchStartEventMode)
		{
			Vector2f uiReadyPos = Util.getSlotCenterPoint(Util.getSlotAheadDir(fenceExitSlot, Direction.DOWN, 5));
			uiReady.draw(uiReadyPos.x-uiReady.getWidth()/2, uiReadyPos.y-uiReady.getHeight()/2);
		}
		
		g.translate(-mapOffset.x, -mapOffset.y);
		
		g.setColor(mapCustomColor.addToCopy(new Color(0,0,0,-100)));
		if(!shakeHUDTimer.timeCompleted()) g.setColor(g.getColor().brighter());
		Vector2f hudOffset = shakeHUDTimer.timeCompleted()? Util.ZERO_VECTOR : getShakedHUDOffset(); 
		g.fillRoundRect(container.getWidth() *0.79f + hudOffset.x, container.getHeight() *0.24f + hudOffset.y, container.getWidth() *0.18f, container.getHeight() *0.6f, (container.getWidth()*container.getHeight())/50000);
		g.setFont(fontHUD);
		g.setColor(mapCustomColorContrasted);
		g.drawString("Score", container.getWidth()*0.8f, container.getHeight() *0.26f);
		g.drawString("   "+FreePacGame.score, container.getWidth()*0.8f, container.getHeight() *0.26f + fontHUD.getLineHeight());
		g.drawString("Pellets", container.getWidth() *0.8f, container.getHeight()*0.26f + fontHUD.getLineHeight()*3);
		g.drawString("   " + pelletsEaten + " de " + pelletCount, container.getWidth() *0.8f, container.getHeight()*0.26f + fontHUD.getLineHeight()*4);
		g.drawString("Stage " + (stageNumber > 0? stageNumber : "-"), container.getWidth() *0.8f, container.getHeight()*0.26f + fontHUD.getLineHeight()*6);
		for(int k=0, kwl = 0, kwc = 0, maxInd = -1+(int)(container.getWidth() *0.18f) / BLOCK_SIZE; k < stageNumber; k++, kwl = k%maxInd, kwc = k/maxInd) 
			((Image) this.game.resources.get("img_goodie"+k)).draw(container.getWidth() *0.795f + kwl*BLOCK_SIZE, container.getHeight()*0.26f + fontHUD.getLineHeight()*(6.9f+kwc*0.8f));
		g.drawString("Lives ", container.getWidth()*0.8f, container.getHeight() *0.735f);
		final float prevRotation = uiPlayerLife.getRotation(); uiPlayerLife.setRotation(0); //XXX kludge to workaround a problem with rotation being set at image rather to be chosen as argument when calling the draw method.
		for(int i = 0; i < FreePacGame.lives; i++) uiPlayerLife.draw(container.getWidth()*0.8f + i*34, container.getHeight() *0.735f + fontHUD.getLineHeight());
		uiPlayerLife.setRotation(prevRotation); //XXX kludge to workaround a problem with rotation being set at image rather to be chosen as argument when calling the draw method.
		
		if(ingameMenuMode)
		{
			g.setColor(ColorExtra.glassSmoke);
			g.fillRect(0, 0, container.getWidth(), container.getHeight());
			g.setFont(fontHUD);
			ingameMenu.draw(game, g);
			imgOpcoes.setRotation(0);
			imgOpcoes.draw(ingameMenu.bounds.getX() + 96, ingameMenu.bounds.getY(), 192, 64);
		}
		
		//DEBUG show enemies behavior
//		for(int i = 0; i < enemies.size(); i++)
//			g.drawString("enemy " + enemies.indexOf(enemies.get(i)) + ": " + enemies.get(i).enemyType.behavior.debug_getCurrentBehaviorDescription(enemies.get(i)), 10, 10 + fontHUD.getLineHeight()*i);
		
		Display.sync(60);
	}
	
	//cheats variables
	private static int cheat1=0, cheat2=0;
	private static final int[] cheat1_keystrokes = new int[] { Input.KEY_3, Input.KEY_6, Input.KEY_0, Input.KEY_N, Input.KEY_O, Input.KEY_S, Input.KEY_C, Input.KEY_O, Input.KEY_P, Input.KEY_E },
								  cheat2_keystrokes = new int[] { Input.KEY_T, Input.KEY_A, Input.KEY_N, Input.KEY_G, Input.KEY_E, Input.KEY_R, Input.KEY_I, Input.KEY_N, Input.KEY_A };
		
		@Override
		public void keyPressed(int key, char c)
		{
			//cheats
			if(key==cheat1_keystrokes[cheat1] && !stageWon) { if(cheat1 == cheat1_keystrokes.length-1) { winStage(); cheat1=0; } else cheat1++;} else cheat1=0;
			if(key==cheat2_keystrokes[cheat2]) { if(cheat2 == cheat2_keystrokes.length-1) {FreePacGame.lives++; soundLifeUp.play(); cheat2=0;} else cheat2++;} else cheat2=0;
			
		switch (key) 
		{
		 	case Input.KEY_UP:
		 		if(ingameMenuMode)
		 		{
		 			if(!FreePacGame.Settings.muteSound) soundCursor.play();
		 			ingameMenu.cursorIndex--;
		 			if(ingameMenu.cursorIndex < 0) ingameMenu.cursorIndex = ingameMenu.entries.size()-1;
		 			break;
		 		}
			 	wantedDir = Direction.UP;
			 	break;
			case Input.KEY_DOWN:
				if(ingameMenuMode)
		 		{
		 			if(!FreePacGame.Settings.muteSound) soundCursor.play();
		 			ingameMenu.cursorIndex++;
		 			if(ingameMenu.cursorIndex > ingameMenu.entries.size()-1) ingameMenu.cursorIndex = 0;
		 			break;
		 		}
		    	wantedDir = Direction.DOWN;
		    	break;
		    	
	        case Input.KEY_LEFT:
		    	wantedDir = Direction.LEFT;
		    	break;
		    	
		    case Input.KEY_RIGHT:
		    	wantedDir = Direction.RIGHT;
		    	break;
	    	
	    	case Input.KEY_PAUSE:
				paused=!paused;
				if(paused)
					pauseTime = System.currentTimeMillis();
				else try
				{
					for(java.lang.reflect.Field f : PlayStageState.class.getDeclaredFields()) if(f.getType().equals(Timer.class))
					{
						f.setAccessible(true);
						((Timer) f.get(this)).lastTime += System.currentTimeMillis() - pauseTime;
						f.setAccessible(false);
						System.out.println("Adjusted (due to pause) "+f.getName());
					}
				} 
				catch(Exception e) { e.printStackTrace(); }
				break;
			
			case Input.KEY_ENTER:
				if(ingameMenuMode) 
				{
					if(!FreePacGame.Settings.muteSound) soundMenuIn.play();
					switch(ingameMenu.cursorIndex)
					{
						case 0:
							resumeGameplayFromIngameMenu();
							break;
						case 1:
							for(Field f : PlayStageState.class.getDeclaredFields())
								if(f.getType().equals(Sound.class)) try 
									{ ((Sound)f.get(this)).stop(); } catch(Exception e) { e.printStackTrace(); }
							for(Field f : PlayStageState.class.getDeclaredFields())
								if(f.getType().equals(Music.class)) try 
									{ if(f.get(this)!=null) ((Music)f.get(this)).stop(); } catch(Exception e) { e.printStackTrace(); }
							paused = false;
							game.enterState(StateSpec.MENU_STATE.id, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
							break;
						case 2:
							Main.closeGame(game, 0);
		
						default:
							break;
					}
				}
				break;
			
			case Input.KEY_ESCAPE:
				if(!matchStartEventMode && !stageWon)
				{
					if(!FreePacGame.Settings.muteSound) soundMenuOut.play();
					if(ingameMenuMode == false)
						enterIngameMenuMode();
					else
						resumeGameplayFromIngameMenu();
				}
				break;
				
	    	default:break;
		}
	}

	//aux var to count items to remove
	final HashSet<Item> aux_itemsToRemove = new HashSet<Item>();
	@Override
	public void update(GameContainer container, StateBasedGame sbgame, int delta) throws SlickException 
	{
		if(!rainbowColored) applyGlowToWallColors();
		
		if(paused) return;  // no update is done while paused

		// kludge to halt the game update while loading assets
		if(!stateEnterDelayFlag) return; 
		if(stateEnterDelayTimer > 0)  // game startup
		{ 
			if(System.currentTimeMillis()-stateEnterDelayTimer < 4500) 	
				return; 
			delta = 0; 	
			stateEnterDelayTimer = 0;
		}
		
		/** The variation of time, in float precision. */
		final float dt = (float) delta;
		final FreePacGame game = (FreePacGame) sbgame;
		
		// the inicial ready scene
		if(matchStartEventMode)
		{
			if(matchStartEventTimer.timeCompletedAndReset()) // immediately after showing ready
			{
				matchStartEventMode = false;
				scatterModeTimer.lastTime += matchStartEventTimer.interval;
				if(musicStage != null && !musicStage.playing() && !FreePacGame.Settings.muteMusic) musicStage.loop(1f, 0.5f);
				if(!FreePacGame.Settings.muteSound) soundEnemyBuzzer.loop();
				pelletAbstinenceTimer.reset();
				enemyReleaseMinimumDelayTimer.reset();
			}
			return; // while showing ready, don't update
		}
		
		// show player death event while dying
		if(playerDeathEventMode)
		{
			if(!playerDeathEventTimer.timeCompletedAndReset())
			{
				if(!playerDeathEventSoundPlayed && playerDeathEventTimer.thirdPartTimeCompleted())
				{
					if(!FreePacGame.Settings.muteSound) soundPlayerDying.play();
					player.dyingSprite.setCurrentFrame(0);
					playerDeathEventSoundPlayed = true;
				}
				return;  // stops all update
			}
			else  // finished death event, reset from death
			{
				if(!FreePacGame.Settings.muteSound) soundPlayerDying.stop();
				if(FreePacGame.lives == 0) 
					game.enterState(StateSpec.GAME_OVER_STATE);
				else
				{
					resetFromDeath();
					return;
				}
			}
		}
		
		if(enemyDeathEventMode)
		{
			if(!enemyDeathEventTimer.timeCompletedAndReset())  // no update during enemy death event
				return;
			else  // after enemy death event
			{
				currentBeatenEnemy = null;
				enemyDeathEventMode = false;
				
				//fix timers
				scatterModeTimer.lastTime += enemyDeathEventTimer.interval;
				powerModeTimer.lastTime += enemyDeathEventTimer.interval;
				enemyReleaseMinimumDelayTimer.lastTime += enemyDeathEventTimer.interval;
				pelletAbstinenceTimer.lastTime += enemyDeathEventTimer.interval;
				bonusAvailabilityTimer.lastTime += enemyDeathEventTimer.interval;
//				lastBonusEatenScoreShowTimer.lastTime += enemyDeathEventTimer.interval; //better not
				
				if(powerMode && !FreePacGame.Settings.muteSound) soundEnemyScaredBuzzer.loop();
				else if(!FreePacGame.Settings.muteSound) soundEnemyBuzzer.loop();
			}
		}
		
		//play eaten enemy buzzer (eye run)
		if(!FreePacGame.Settings.muteSound){
			boolean haveBeatenEnemies = false;
			for(Enemy enemy : enemies)  if(enemy.eaten) { haveBeatenEnemies = true; break; }
			if(haveBeatenEnemies && !soundEnemyBeatenBuzzer.playing()) soundEnemyBeatenBuzzer.loop();
			else if(!haveBeatenEnemies) soundEnemyBeatenBuzzer.stop();
		}
		
		// disable power mode when duration time's up
		if(powerMode && powerModeTimer.timeCompletedAndReset())
		{
			scatterModeTimer.lastTime += powerModeTimer.interval;  // simulate the pause on scatter mode timer
			setPowerMode(false);
		}
		
		// release enemies in time and situation
		
		if(pelletsEaten >= DEFAULT_ENEMY_PELLET_COUNT_FOR_RELEASE[enemyReleaseCounter] && enemyReleaseMinimumDelayTimer.timeCompletedAndReset() && enemyReleaseCounter < enemies.size())
		{
			System.out.println("Released enemy " + enemyReleaseCounter + " due to pellet count="+pelletsEaten);
			releaseNextEnemy();
		}
		
		if(pelletAbstinenceTimer.timeCompleted() && enemyReleaseCounter < enemies.size())
		{
			System.out.println("Released enemy " + enemyReleaseCounter + " due to pellet abstinence = " + pelletAbstinenceTimer.difference()+"ms");
			pelletAbstinenceTimer.reset();
			releaseNextEnemy();
		}
		
		// alternate scatter/chase modes
		if(powerMode==false && scatterModeTimer.timeCompletedAndReset() && scatterModeTimer.interval != -1)
		{
			scatterModeTimer.interval = scatterModeIntervals[scatterModeCounter++];
			for(Enemy enemy : enemies)
			{
				enemy.scatter = !enemy.scatter;
				if(enemy.eaten) continue;
				enemy.reverseSafely();
				enemy.snapToGrid();
				System.out.println("enemy "+enemies.indexOf(enemy)+" is in "+(enemy.scatter?"SCATTER mode":"CHASE mode"));
			}
//			System.out.println("Changing to " + (scatterModeCounter%2==0?"CHASE mode":"SCATTER mode"));
		}
		
		player.move(dt);
		
//		player.speed = 0.5f*((dt - 1f) / dt) * (player.speed + (powerMode? Actor.SPEED_FACTOR * 1.2f : Actor.SPEED_FACTOR));
		player.speed = 0.5f * (player.speed + (powerMode? Actor.SPEED_FACTOR * 1.2f : Actor.SPEED_FACTOR));
		
//		{
//			StringBuilder s = new StringBuilder();
//			int i;
//			for(i = 0; i < (int) (100f*player.speed/Actor.SPEED_FACTOR); i++)
//				s.append('#');
//			for(; i < 100; i++)
//				s.append('_');
//			System.out.println(s.toString());
//		}
		
		//eat pellets if close enough
		for(Item i : items) if(Util.getSlotPosition(i.body.getCenter()).equals(player.getSlotPosition())) 
		{
			switch (i.type) 
			{
				case PELLET:
				{
					if(!FreePacGame.Settings.muteSound && !soundEatPellet.playing()) 
						soundEatPellet.play();

					pelletsEaten++;
					player.speed = (powerMode? Actor.SPEED_FACTOR * 0.8f : Actor.SPEED_FACTOR * 0.5f);

					FreePacGame.score += PELLET_REWARD; // add score
					break;
				}
				case ENERGIZER:
				{
					pelletsEaten++;
					player.speed = (powerMode? Actor.SPEED_FACTOR * 0.5f : 0);
					setPowerMode(true);
					
					shakeHUDTimer.reset();
					FreePacGame.score += ENERGIZER_REWARD; // add score
					break;
				}
				case BONUS_PELLET:
				{
					if(!FreePacGame.Settings.muteSound) 
						soundBonusPellet.play();
					
					lastBonusEatenScoreValue = DEFAULT_BONUSES_VALUES[(stageNumber >= DEFAULT_BONUSES_VALUES.length? DEFAULT_BONUSES_VALUES.length-1 : stageNumber-1)];
					lastBonusEatenPosition = new Vector2f(i.body.getCenterX(), i.body.getCenterY());
					lastBonusEatenScoreShowTimer.reset();
					
					player.speed = (powerMode? Actor.SPEED_FACTOR * 0.5f : 0);
					shakeHUDTimer.reset();				
					FreePacGame.score += lastBonusEatenScoreValue; // add score
					break;
				}
				default:break;
			}
			
			aux_itemsToRemove.add(i);
//			items.remove(i);			
			pelletAbstinenceTimer.reset();
		}
		
		for(Item i : aux_itemsToRemove)	items.remove(i);
		aux_itemsToRemove.clear();
		
		// check if it's time to spawn a bonus pellet
		if((bonusSpawnedCounter == 0 && pelletsEaten > 69)
		|| (bonusSpawnedCounter == 1 && pelletsEaten > 169) )
			spawnBonusPellet();
		
		// check if it's time to remove a spawned bonus pellet
		if(bonusItem != null && bonusAvailabilityTimer.timeCompleted())
		{
			items.remove(bonusItem);
			bonusItem = null;
		}
		
		if(lastBonusEatenPosition != null && lastBonusEatenScoreShowTimer.timeCompleted())
			lastBonusEatenPosition = null;
		
		//if all pellets were eaten, end stage
		if(pelletsEaten == /*10)//*/ pelletCount)
			winStage();
		
		if(FreePacGame.score > DEFAULT_EXTRA_LIFE_SCORE_NEEDED[FreePacGame.extraLifeCounter])
		{
			if(!FreePacGame.Settings.muteSound) soundLifeUp.play();
			FreePacGame.lives ++;
			FreePacGame.extraLifeCounter ++;
		}
		
		checkCollisionWithWarp(player);
		
		for(Enemy enemy : this.enemies)
		{
			enemy.move(dt); 
			checkCollisionWithWarp(enemy);
		}
		
		if(playerCollidesWithEnemy()!=null)
		{
			shakeHUDTimer.reset();
			Enemy g = playerCollidesWithEnemy();
			if(g.eaten == false)
			{
				if(g.scared)
				{
					System.out.println("enemy "+enemies.indexOf(g)+" eaten!");
					soundEnemyBuzzer.stop();
					soundEnemyScaredBuzzer.stop();
					soundEnemyBeatenBuzzer.stop();
					if(!FreePacGame.Settings.muteSound) soundBeatEnemy.play();
					g.eaten = true;
					g.scared = false;
					enemyDeathEventMode = true;
					enemyDeathEventTimer.reset();
					currentBeatenEnemy = g;
					FreePacGame.score += ((int)(Math.pow(2, ++enemiesBeatenStreak)*100));
					return;
				}
				else // died
				{
					System.out.println("Caught by enemy "+enemies.indexOf(g)+"!");
					if(!FreePacGame.Settings.muteSound) soundPlayerCaught.play();
					killPlayer();
					return;
				}
			}
			else; //what to do when colliding with a dead enemy... (time will show...)
		}
	}
	
	
	// ============================================= SPECIFIC ================================================================
	
	void setPowerMode(boolean enabled)
	{
		if(enabled) System.out.println("Power Mode!");
		else System.out.println("...power mode wears off");
		powerMode = enabled;
		for(Enemy g : enemies) if(!g.eaten && !g.houseStaying && g.passedFence && !g.invulnerable)
		{
			g.scared = enabled;
			g.reverseSafely();  // changing direction immediately when entering power mode
			g.snapToGrid();
			System.out.println("enemy "+enemies.indexOf(g)+" is "+(g.scared?"now scared!":"not scared anymore!"));
		}
		if(enabled)
		{
			powerModeTimer.reset();
			if(!FreePacGame.Settings.muteSound) soundEatPowerPill.play();
			soundEnemyBuzzer.stop();
			if(!soundEnemyScaredBuzzer.playing() && !FreePacGame.Settings.muteSound) soundEnemyScaredBuzzer.loop();
		}
		else
		{
			soundEnemyScaredBuzzer.stop();
			if(!FreePacGame.Settings.muteSound) soundEnemyBuzzer.loop();
			enemiesBeatenStreak = 0;
		}
	}
	
	void checkCollisionWithWarp(Actor actor)
	{
		Block warp = null;
		if((warp = actorCollidesWithWarp(actor))!=null)
		{
			if(!FreePacGame.Settings.muteSound) soundWarped.play();
			warp = warp.twin;
			Vector2f newLoc = new Vector2f(warp.getLocation());
			if(actor.dir == Direction.RIGHT) 	
				newLoc.x += BLOCK_SIZE * 0.666666666667f;
			else if(actor.dir == Direction.LEFT) 
				newLoc.x -= BLOCK_SIZE * 0.666666666667f;
			else if(actor.dir == Direction.UP) 
				newLoc.y -= BLOCK_SIZE * 0.666666666667f;
			else if(actor.dir == Direction.DOWN) 
				newLoc.y += BLOCK_SIZE * 0.666666666667f;
			
			actor.body.setLocation(newLoc);
			actor.snapToGrid();
		}
	}
	
	/** Returns the warp block that the actor collides with. */
	public Block actorCollidesWithWarp(Actor actor)
	{
		final Pair<Integer, Integer> slot = actor.getSlotPosition();
		if(isSlotGridBoundsSafe(slot)==false) return null;
		final Block block = grid[slot.getValue0()][slot.getValue1()];
		if(block != null && block.type == Type.TELEPORT)
			return block;
		return null;
	}
	
	/** Returns the enemy which that the shape collides with. */
	private Enemy playerCollidesWithEnemy()
	{
		for(Enemy enemy : enemies) if(player.getSlotPosition().equals(enemy.getSlotPosition()))
			return enemy;
		
		return null;
	}
	
	public boolean isSlotGridBoundsSafe(Pair<Integer, Integer> slot)
	{
		return isSlotGridBoundsSafe(slot.getValue0(), slot.getValue1());
	}
	
	public boolean isSlotGridBoundsSafe(int i, int j)
	{
		if(i < 0 || j < 0)
			return false;
		
		if(i > grid.length-1)
			return false;
		
		if(j > grid[i].length-1)
			return false;
		
		return true;
	}
	
	public boolean isSlotFreeForActor(Pair<Integer, Integer> slot, Actor actor)
	{
		return isSlotFreeForActor(slot.getValue0(), slot.getValue1(), actor);
	}
	
	/** Returns whether the specified slot is free for this actor. 
	 *  This method returns true if:
	 * <li> The specified slot is not out of the grid's bounds <b>and</b>; </li>
	 * <li> The specified slot doesn't have a block <b>or</b>; </li>
	 * <li> The specified slot contains a block, but this actor is allowed to pass through it (types of wall are accounted). </li> 
	 * <br> FIXME this method should be reworked to <b>NOT</b> use reflection. */
	public boolean isSlotFreeForActor(int i, int j, Actor actor)
	{
		if(!isSlotGridBoundsSafe(i, j))
			return false;
		
		Block block = grid[i][j];
		
		//no block means free
		if(block == null) 
			return true;
		
		//warp allows everyone
		if(block.type == Block.Type.TELEPORT) 
			return true;
		
		//fence allows enemies to pass once per enemy life
		if(block.type == Block.Type.FENCE && actor instanceof Enemy && ((Enemy) actor).passedFence==false && ((Enemy) actor).houseStaying==false) 
			return true;
		
		if(actor instanceof Enemy && ((Enemy) actor).noClip) // no-clipping enemies can pass thru
			return true;
		
		block.marked = true; //for debug purposes
		return false; //else blocks all
	}
	
	//TODO improve this. needs an animation
	private void resetFromDeath()
	{
		FreePacGame.lives--;
		player.body.setCenterX(player.spawnLocation.x);
		player.body.setCenterY(player.spawnLocation.y);
		player.dir = Direction.LEFT;
		for(Enemy enemy : enemies)
		{
			enemy.body.setCenterX(enemy.spawnLocation.x);
			enemy.body.setCenterY(enemy.spawnLocation.y);
			if(enemies.indexOf(enemy) == 0)
				enemy.dir = Direction.LEFT;
			else
				enemy.dir = enemies.indexOf(enemy)%2 == 0 ? Direction.UP: Direction.DOWN;
			enemy.houseStaying = true;
			enemy.passedFence = false;
			enemy.scared = false;
			enemy.scatter = true;
			enemy.eaten = false;
			enemy.aux1 = null;
		}
		enemyReleaseCounter = 0;
		releaseNextEnemy();
		releaseNextEnemy();
		enemies.get(0).passedFence = true;
		playerLifeStartTime = System.currentTimeMillis();
		scatterModeCounter = 0;
		scatterModeTimer.interval = scatterModeIntervals[scatterModeCounter++];
		powerMode = false;
		playerDeathEventMode = false;
		scatterModeTimer.reset();
		
		if(bonusItem != null)
		{
			items.remove(bonusItem);
			bonusItem = null;
		}
		
		matchStartEventMode = true;
		matchStartEventTimer.reset();		
		if(musicStage != null && !musicStage.playing() && !FreePacGame.Settings.muteMusic) musicStage.resume();
		System.out.println("Died! Resetting all!");
	}
	
	private void winStage()
	{
		if(musicStage != null) musicStage.stop();
		soundEnemyBuzzer.stop();
		soundEnemyScaredBuzzer.stop();
		soundEnemyBeatenBuzzer.stop();
		soundEatPellet.stop();
		soundEatPowerPill.stop();
		soundWarped.stop();
		if(!FreePacGame.Settings.muteSound) soundLastPellet.play();
		paused = true;
		stageWon = true;
		
		new Thread(new Runnable() 
		{
			@Override
			public void run() {
				
				try { Thread.sleep(1500); } catch (InterruptedException e) { e.printStackTrace(); }
				paused = false;
				if(!FreePacGame.Settings.muteSound) soundVictoryFade.play();
				game.enterState(StateSpec.STAGE_CLEAR_STATE, new FadeOutTransition(Color.white), new FadeInTransition(Color.white));
			}
		}).start();
	}
	
	public void killPlayer()
	{
		playerDeathEventMode = true;
		playerDeathEventTimer.reset();
		playerDeathEventSoundPlayed = false;
		if(musicStage != null) musicStage.pause();
		soundEnemyScaredBuzzer.stop();
		soundEnemyBuzzer.stop();
		soundEnemyBeatenBuzzer.stop();
	}
	
	Vector2f getShakedHUDOffset()
	{
		long delta = System.currentTimeMillis() - shakeHUDTimer.lastTime;
		switch ((int)(delta % 50)/12) {
		case 0: return new Vector2f();
		case 1: return new Vector2f(HUD_SHAKE_INTENSITY, 0);
		case 2: return new Vector2f(0, HUD_SHAKE_INTENSITY);
		case 3: return new Vector2f(HUD_SHAKE_INTENSITY, HUD_SHAKE_INTENSITY);
		default: return new Vector2f();
		}
	}
	
	/** Compute a offset to the given shape position (as a special effect). 
	 * 	Returns null if no effect is applied for the current stage. */
	public static Vector2f computeOffsetSpecialEffect(Shape shape)
	{
		// TODO it could be nice there was per-stage effects from this offset (floating if water stage, shaking as quakes, etc)
		return new Vector2f(0, (float) FastTrig.cos(shape.getX() + System.currentTimeMillis()/100d)*PlayStageState.WAVE_EFFECT_MAGNETUDE); // cool offset!!!
	}
	
	public void applyGlowToWallColors()
	{
		final float mapCustomColor_dt = (float) FastTrig.sin((System.currentTimeMillis()-playerLifeStartTime)/200);
		Block.wallDrawer.wallColor.r += 0.01f * mapCustomColor_dt;
		Block.wallDrawer.wallColor.g += 0.01f * mapCustomColor_dt;
		Block.wallDrawer.wallColor.b += 0.01f * mapCustomColor_dt;
		
		Block.wallDrawer.borderColor.r += 0.005f * mapCustomColor_dt;
		Block.wallDrawer.borderColor.g += 0.005f * mapCustomColor_dt;
		Block.wallDrawer.borderColor.b += 0.005f * mapCustomColor_dt;
	}
	
	void spawnBonusPellet()
	{
		Pair<Integer, Integer> slot = fenceExitSlot;
		do try
		{
			slot = Util.getSlotAheadDir(slot, Direction.DOWN);
			if(grid[slot.getValue0()][slot.getValue1()] != null && grid[slot.getValue0()][slot.getValue1()].type == Block.Type.WALL)
				break;
		}
		catch(Exception e) { e.printStackTrace(); }
		while(true);
		
		slot = Util.getSlotAheadDir(slot, Direction.DOWN); // get slot immediately under
		bonusItem = new Item(Util.getSlotCenterPoint(slot), BLOCK_SIZE/3, stageNumber-1, game.resources);
		items.add(bonusItem);
		bonusSpawnedCounter++;
		bonusAvailabilityTimer.reset();
	}
	
	void releaseNextEnemy()
	{
		if(enemyReleaseCounter >= enemies.size()) //all enemies have been released. stop
			return;
			
		enemies.get(enemyReleaseCounter).houseStaying = false;
		enemyReleaseCounter++;
	}
	
	long computePelletAbstinenceTimeout()
	{
		return (long) ((ENEMY_PELLET_ABSTINENCE_TIMEOUT/16d) * (5d/stageNumber + 11d));
	}
	
	void enterIngameMenuMode()
	{
		paused = true;
		ingameMenuMode = true;
		if(musicStage != null) 
		{
			volumeMusicStage = musicStage.getVolume();
			musicStage.setVolume(0.5f);
		}
		ingameMenuEntererTime = System.currentTimeMillis();
	}
	
	void resumeGameplayFromIngameMenu()
	{
		ingameMenuMode = false;
		paused = false;
		if(musicStage != null) musicStage.setVolume(volumeMusicStage);
		
		long ingameMenuTimeStayed = System.currentTimeMillis() - ingameMenuEntererTime;
		for(Field f : PlayStageState.class.getDeclaredFields())
			if(f.getType().equals(Timer.class)) try 
				{ ((Timer)f.get(this)).lastTime += ingameMenuTimeStayed; }
		catch(Exception e) { e.printStackTrace(); }
	}
	
	public FreePacWallDrawer getWallDrawer(CustomGraphics g)
	{	
		if(stageTileStyle == null)
		{ /* do nothing */ }
			
		else if(stageTileStyle.equalsIgnoreCase("rounded"))
			return new RoundedEdgeWallDrawer(g, mapCustomColor, mapCustomColor.darker(0.2f));
		
		else if(stageTileStyle.equalsIgnoreCase("sloped"))
			return new SlopedEdgeWallDrawer(g, mapCustomColor, mapCustomColor.darker(0.2f));
		
		else if(stageTileStyle.equalsIgnoreCase("squared"))
			return new SimpleWallDrawer(g, mapCustomColor, mapCustomColor.darker(0.2f));
		
		//default
		return new RoundedEdgeWallDrawer(g, mapCustomColor, mapCustomColor.darker(0.2f));
	}
	
	public void loadStage(String ref) throws FileNotFoundException, Exception
	{
		if(ref.endsWith(".def")) ref = ref.substring(0, ref.lastIndexOf("."));
		//read stage properties
		Properties prop = new Properties();
		FileInputStream fis_prop = new FileInputStream(new File(ref+".def"));
		prop.load(fis_prop);
		fis_prop.close();
		
		int lines = Integer.parseInt(prop.getProperty("height").trim());
		int columns = Integer.parseInt(prop.getProperty("width").trim());
		stageName = prop.getProperty("name"); if(stageName != null) stageName = stageName.trim();
		stageTileStyle = prop.getProperty("style"); if(stageTileStyle != null) stageTileStyle = stageTileStyle.trim();
		String bgRef = prop.getProperty("background"); if(bgRef != null) bgRef = bgRef.trim();
		String musicName = prop.getProperty("song"); if(musicName != null) musicName = musicName.trim();
		String customColorStr = prop.getProperty("color"); if(customColorStr != null) customColorStr = customColorStr.trim();
		noIntroTune = Boolean.parseBoolean(prop.getProperty("nointrotune"));
		
		String enemiesDefStr = prop.getProperty("enemies"); if(enemiesDefStr != null) enemiesDefStr = enemiesDefStr.trim();
		
		Object[] classicSet = new Enemy.EnemyType[4];
		classicSet[0] = Enemy.EnemyType.Classic_1;
		classicSet[1] = Enemy.EnemyType.Classic_2;
		classicSet[2] = Enemy.EnemyType.Classic_3;
		classicSet[3] = Enemy.EnemyType.Classic_4;
		
		Object[] enemyDefs = null;
		
		if(FreePacGame.Settings.difficulty == 0)
			enemyDefs = classicSet;
		else
		{
			if(enemiesDefStr == null || enemiesDefStr.trim().length() == 0 || enemiesDefStr.trim().equalsIgnoreCase("default"))
			{
				enemyDefs = new Enemy.EnemyType[4];
				enemyDefs[0] = Enemy.EnemyType.Default_1;
				enemyDefs[1] = Enemy.EnemyType.Default_2;
				enemyDefs[2] = Enemy.EnemyType.Default_3;
				enemyDefs[3] = Enemy.EnemyType.Default_4;
			}
			
			else if(enemiesDefStr.trim().equalsIgnoreCase("classic"))
				enemyDefs = classicSet;
			
			else if(enemiesDefStr.trim().equalsIgnoreCase("dummy"))
			{
				enemyDefs = new Enemy.EnemyType[4];
				enemyDefs[0] = enemyDefs[1] = enemyDefs[2] = enemyDefs[3] = Enemy.EnemyType.Dummy;
			}
			else //specify enemies
			{
				//TODO implement functionality to load custom enemies types
				ArrayList<Object> types = new ArrayList<Object>(); //lets gather all specified types
				String[] tokens = enemiesDefStr.split(","); //split the field by commas to get all specified types
				for(String token : tokens) 
				{
					token = token.trim();
					if(token.length()==0) continue; //ignore empty space
					Object specifiedType = EnemyType.Dummy; //default is dummy
					for(EnemyType gtype : EnemyType.values()) 
						if(gtype.name().equalsIgnoreCase(token))
							specifiedType = gtype;
					for(CustomEnemySpecification spec : game.customEnemySpecs)
						if(spec.name.equalsIgnoreCase(token))
							specifiedType = spec;
					types.add(specifiedType);
				}
				enemyDefs = types.toArray(new Object[0]);
			}
		}
		int enemyFoundCount=0; //count enemies as we find
		
		//read stage map (chars)
		Scanner scanner = new Scanner(new InputStreamReader(new FileInputStream(new File(ref+".map")), "UTF-8"));
		char[][] char_grid = new char[lines][columns];
		ArrayList<Character> chars = new ArrayList<Character>();
		while(scanner.hasNextLine()) for(char c : scanner.nextLine().toCharArray())
		{
			chars.add(c);
		}
		scanner.close(); //chars were read
		
		if(chars.size() != lines*columns) throw new Exception("Ill formatted map: expected "+(lines*columns)+" slots but "+chars.size()+" were found!");
		
		//parse chars to char grid
		for(int i = 0, k = 0; i < lines; i++) for(int j = 0; j < columns; j++)
		{
			char_grid[i][j] = chars.get(k++);
		}
		
		System.out.println("Loaded map:");
		Util.printGrid(char_grid);
		
		grid = new Block[lines][columns];
		slowZones = new boolean[lines][columns];
		enemies = new ArrayList<Enemy>();
		items = new HashSet<Item>();
		pelletCount = 0;
		fenceExitSlot=null;
		
		mapCustomColor = Color.transparent;
		rainbowColored = false;
		if(customColorStr.equalsIgnoreCase("rainbow"))
			rainbowColored = true;
		else mapCustomColor = ColorExtra.toColor(customColorStr, Color.transparent);
		if(!rainbowColored) mapCustomColorContrasted = ColorExtra.getContrastColor(mapCustomColor);

		//build map
		for(int i = 0; i < lines; i++) for(int j = 0, t=-1; j < columns; j++)
		{
			slowZones[i][j] = false;
			switch(char_grid[i][j])
			{
				case '#':
				{
					grid[i][j] = new Block(j*BLOCK_SIZE, i*BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, Block.Type.WALL);
					grid[i][j].customColor = mapCustomColor;
					break;
				}
				case '=':
				{
					grid[i][j] = new Block(j*BLOCK_SIZE, i*BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, Block.Type.FENCE);
					fenceExitSlot = new Pair<Integer, Integer>(i, j);
					break;
				}
				case '!':
				{
					grid[i][j] = new Block(j*BLOCK_SIZE, i*BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, Block.Type.TELEPORT);
					if(t == -1) t = j; //mark that a horizontal warp was found
					else 
					{	//marks the twin of this warp
						grid[i][t].twin = grid[i][j];
						grid[i][j].twin = grid[i][t];
					}
					break;
				}
				case ',': slowZones[i][j] = true;
				case '.':
				{
					items.add(new Item(j*BLOCK_SIZE + BLOCK_SIZE/2, i*BLOCK_SIZE + BLOCK_SIZE/2, BLOCK_SIZE/8, Item.Type.PELLET, game.resources));
					pelletCount++;
					break;
				}
				case '*':
				{
					items.add(new Item(j*BLOCK_SIZE + BLOCK_SIZE/2, i*BLOCK_SIZE + BLOCK_SIZE/2, BLOCK_SIZE/3 * 0.9f, Item.Type.ENERGIZER, game.resources));
					pelletCount++;
					break;
				}
				case '@':
				{
					Vector2f spawnPoint = new Vector2f(j*BLOCK_SIZE, i*BLOCK_SIZE + BLOCK_SIZE/2);
					Enemy enemy = null;
					if(enemyDefs != null && enemyFoundCount < enemyDefs.length)
					{
						Object enemyType = enemyDefs[enemyFoundCount++]; //get the enemy type according to preset
						if(enemyType instanceof EnemyType)
						{
							enemy = new Enemy(this, (EnemyType) enemyType, new Circle(spawnPoint.x, spawnPoint.y, (BLOCK_SIZE/2.0f)*0.9f), spawnPoint); 
						}
						else
						{
							enemy = new Enemy(this, (CustomEnemySpecification) enemyType, new Circle(spawnPoint.x, spawnPoint.y, (BLOCK_SIZE/2.0f)*0.9f), spawnPoint);  
						}
					}
					else //default type just in case its not specified
					{
						enemy = new Enemy(this, EnemyType.Dummy, new Circle(spawnPoint.x, spawnPoint.y, (BLOCK_SIZE/2.0f)*0.9f), spawnPoint);
					}
					
					enemies.add(enemy);
					break;
				}					
				case '$':
				{	
					Vector2f spawnPoint = new Vector2f(j*BLOCK_SIZE, i*BLOCK_SIZE + BLOCK_SIZE/2);
					player = new PlayableCharacter(this, new Circle(spawnPoint.x, spawnPoint.y , BLOCK_SIZE/2), spawnPoint, game.playableCharactersRefs.get(MenuState.charChoice));
					break;
				}
				
				case '/':
				{
					grid[i][j] = OUTSIDE_MAP;
					break;
				}
				
				case '´':
				{
					slowZones[i][j] = true;
					break;
				}
				
				case ' ':
				default:
					break;
			}
		}
		
		//custom code to find vertical warps
		for(int j = 0; j < columns; j++) for(int i = 0, t=-1; i < lines; i++) 
		{
			if(char_grid[i][j] == '¡')
			{
				grid[i][j] = new Block(j*BLOCK_SIZE, i*BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE, Block.Type.TELEPORT);
				grid[i][j].isVerticalWarp = true;
				if(t == -1) t = i; //mark that a vertical warp was found
				else 
				{	//marks the twin of this warp
					grid[t][j].twin = grid[i][j];
					grid[i][j].twin = grid[t][j];
				}
			}
		}
		
		if(player == null) throw new Exception("No start point for player!");
		
		//processing enemy house walls
		Stack<Pair<Integer, Integer>> fenceWalls = new Stack<Pair<Integer, Integer>>(); // used to keep track of enemy house walls
		Pair<Integer, Integer> leftmostExitSlot = fenceExitSlot; // the (candidate) leftmost fence exit slot
		while(true)
		{
			if(grid[leftmostExitSlot.getValue0()][leftmostExitSlot.getValue1()].type == Type.FENCE)
				leftmostExitSlot = Util.getSlotAheadDir(leftmostExitSlot, Direction.LEFT);
			else break;
		}
		fenceWalls.push(leftmostExitSlot);  //hardcoded, I know, sounds kludgy. now leftmostExitSlot should be the first enemy house wall
		while(!fenceWalls.isEmpty()) 
		{
			Pair<Integer, Integer> slot = fenceWalls.pop(), neighborSlot;
			Block block = grid[slot.getValue0()][slot.getValue1()]; //lets mod this block, if it is a wall
			if(block.type == Type.WALL && !block.adFence) //avoind revisiting modded blocks
			{
				block.adFence = true; //mark as adjacent (indirectly) to the fence
				for(Direction direction : Direction.values()) //check all neighbors for walls
				{
					neighborSlot = Util.getSlotAheadDir(slot, direction);
					if(grid[neighborSlot.getValue0()][neighborSlot.getValue1()] != null 
					&& grid[neighborSlot.getValue0()][neighborSlot.getValue1()].type == Type.WALL)
						fenceWalls.push(neighborSlot);
				}
			}
			else continue;
		}
		
		for(Enemy enemy : enemies) 
		{
			enemy.setTarget(player);  // needs to be here because its needs that player is found before setting it as target
			if(enemies.indexOf(enemy) != 0) 
				enemy.dir = enemies.indexOf(enemy)%2 == 0 ? Direction.UP: Direction.DOWN;
		}
		enemies.get(0).passedFence = true;
		
		if(bgRef == null || bgRef.trim().length() == 0) bg = null;
		else bg = (Image) game.resources.get(bgRef);
		bgColor = ColorExtra.toColor(bgRef, Color.black);
		if(musicName == null || musicName.trim().length()==0) musicStage = null;
		else musicStage = (Music) game.resources.get(musicName);
		mapHeight = lines*BLOCK_SIZE;
		mapWidth = columns*BLOCK_SIZE;
		mapLeftTopPosition = new Vector2f(0, 0);
		mapRightTopPosition = new Vector2f(columns*BLOCK_SIZE, 0);
		mapRightBottomPosition = new Vector2f(columns*BLOCK_SIZE, lines*BLOCK_SIZE);
		mapLeftBottomPosition = new Vector2f(0, lines*BLOCK_SIZE);
		uiPlayerLife = player.getLifeIcon();
		Block.wallDrawer = null;
	}
}
