package states;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeMap;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import main.FreePacGame;
import util.ColorExtra;
import util.Direction;
import util.Timer;
import util.Util;
import util.drawing.ScrollingBackground;

public class StageSelectState extends BasicGameState
{
	@Override public int getID() { return StateSpec.STAGE_SELECT_STATE.id; }
	
	FreePacGame game;
	Sound soundCursor, soundMenuIn, soundMenuOut;
	Image txt_fases;
	Font font;
	Color colorSelectedEntry, colorEntry, colorFontSelectedEntry, colorFontEntry;
	ScrollingBackground bg;
	TreeMap<String, String> stageCatalog;
	float menu_offset=0, offset_variation=0;
	int choice;
	Direction cursorPressed = null;
	Timer cursorPressedTimer = new Timer(125);
	
	private static FilenameFilter dot_def_files = new FilenameFilter() { @Override public boolean accept(File dir, String name) { if(name.endsWith(".def")) return true; else return false;} };

	@Override
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		this.game = (FreePacGame) sbg;
		bg = new ScrollingBackground("bg_loading", game.resources);
		txt_fases = (Image) game.resources.get("img_txt_stages_s");
		soundCursor = (Sound) game.resources.get("snd_cursor");
		soundMenuIn = (Sound) game.resources.get("snd_menu_in");
		soundMenuOut = (Sound) game.resources.get("snd_menu_out");
		font = Util.deriveFont((Font) game.resources.get("fnt_ui2"), java.awt.Font.PLAIN, 48);
		
		colorSelectedEntry = ColorExtra.toColor(game.resources.getProperty("ui_stage_select_selected_entry_color"), ColorExtra.violet).addToCopy(new Color(0, 0, 0, -55));
		colorEntry = ColorExtra.toColor(game.resources.getProperty("ui_stage_select_entry_color"), Color.magenta.addToCopy(new Color(50,50,50))).addToCopy(new Color(0,0,0,-55));
		colorFontSelectedEntry = ColorExtra.toColor(game.resources.getProperty("ui_stage_select_selected_entry_font_color"), Color.pink);
		colorFontEntry = ColorExtra.toColor(game.resources.getProperty("ui_stage_select_entry_font_color"), Color.magenta.darker());

		stageCatalog = new TreeMap<String, String>(); 
		for(File f : new File("stages").listFiles(dot_def_files)) try
		{
			Properties properties = new Properties();
			FileInputStream stream = new FileInputStream(f);
			properties.load(stream);
			stream.close();
			stageCatalog.put(f.getName(), new String(properties.getProperty("name").getBytes("ISO-8859-1"), "UTF-8"));
		}
		catch (Exception e) { System.out.println("Failed to read stage file "+f.getName()); e.printStackTrace(); }
		choice = 0;
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException
	{
		cursorPressed = null;
	}

	@Override
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		if(!MenuState.noBG) bg.draw(container);
		g.translate(0, -menu_offset);
		int middle = container.getWidth()/2;
		txt_fases.draw(middle - 90, 20);
		
		int offset = 120, index = 0;
		for(Entry<String, String> e : stageCatalog.entrySet())
		{
			g.setFont(font);
			g.setColor(choice == index? colorSelectedEntry : colorEntry);
			g.fillRoundRect(20, offset, container.getWidth(), g.getFont().getLineHeight(), 20); //highlight current selected menu item
			g.setColor(choice == index? colorFontSelectedEntry : colorFontEntry);
			g.drawString(e.getValue() + " (" + e.getKey() + ")", 30, offset);
			offset += g.getFont().getLineHeight()*1.1;
			index++;
		}
		g.translate(0, menu_offset);
		Display.sync(60);
	}

	@Override
	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		float dt = (float) delta;
		bg.update(delta, container);
		if(offset_variation != 0)
		{
			menu_offset += offset_variation > 0? Math.min(dt/2, offset_variation) : Math.max(-dt/2, offset_variation);
			if(offset_variation > 0)
			{
				offset_variation -= dt/2;
				if(offset_variation < 0) offset_variation = 0;
			}
			else
			{
				offset_variation += dt/2;
				if(offset_variation > 0) offset_variation = 0;
			}
		}
		
		if(cursorPressed != null && cursorPressedTimer.timeCompletedAndReset())
		{
			moveCursor(cursorPressed == Direction.UP);
		}
			
	}
	
	@Override
	public void keyPressed(int key, char c) {
		switch(key) {
	    case Input.KEY_DOWN:
	    	moveCursor(false);
	    	cursorPressed = Direction.DOWN;
	    	cursorPressedTimer.reset();
	        break;
	    case Input.KEY_UP:
	    	moveCursor(true);
	    	cursorPressed = Direction.UP;
	    	cursorPressedTimer.reset();
	        break;
	    case Input.KEY_ENTER:
	    	if(!FreePacGame.Settings.muteSound) soundMenuIn.play();
	    	Music menuStateMusic = ((MenuState) game.getState(StateSpec.MENU_STATE.id)).music;
	    	if(menuStateMusic != null)
	    		menuStateMusic.stop();
	    	
	    	FreePacGame.resetGlobalGameStatus();
	    	PlayStageState.isCareer = false;
	    	
	    	int k = 0; 
	    	for(Entry<String, String> e : stageCatalog.entrySet()) 
	    		if(k++ == choice) 
	    			PlayStageState.stageFilename = "stages/"+e.getKey();
	    	
			game.enterState(StateSpec.PLAY_STAGE_STATE, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
			break;
	    case Input.KEY_ESCAPE:
	    	if(!FreePacGame.Settings.muteSound) soundMenuOut.play();
	    	game.enterState(StateSpec.MENU_STATE);
	    default:
	        break;
	    }
	}
	
	@Override
	public void keyReleased(int key, char c)
	{
		switch(key) {
	    case Input.KEY_DOWN:
	    case Input.KEY_UP:
	    	cursorPressed = null;
	        break;
		}
	}
	
	public void moveCursor(boolean up)
	{
		if(offset_variation != 0) return;
		
		if(!FreePacGame.Settings.muteSound) 
			soundCursor.play();
		
		if(up)
		{
	    	choice--;
	        if(choice < 0) 
	        	choice = 0;
	        
	        if(font.getLineHeight()*1.1f*choice+120f < menu_offset) 
	        	offset_variation = -font.getLineHeight()*(choice==0?2.2f:1.1f);
		}
		else
		{
	        choice++;
	        if(choice > stageCatalog.size()-1) 
	        	choice = stageCatalog.size()-1;
	        
	        if(font.getLineHeight()*1.1f*(choice+1)+120f > game.getContainer().getHeight() + menu_offset) 
	        	offset_variation = font.getLineHeight()*1.1f;
		}
	}
}
