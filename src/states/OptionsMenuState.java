package states;

import javax.swing.JOptionPane;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import main.FreePacGame;
import util.ColorExtra;
import util.Menu;
import util.Util;

public class OptionsMenuState extends BasicGameState  {

	@Override public int getID() { return StateSpec.OPTIONS_MENU_STATE.id; } //STATE ID
	Image bg, img_opcoes;
	Sound soundCursor, soundMenuIn, soundMenuOut;
	Menu menu;
	Font font;
	float previousMenuMusicVolume;
	Music menuStateMusic;
	
	private FreePacGame game; // stored for later use
	
	@Override
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		this.game = (FreePacGame) sbg;
		final float w = container.getWidth(), h = container.getScreenHeight();
		
		soundCursor = (Sound) game.resources.get("snd_cursor");
		soundMenuIn = (Sound) game.resources.get("snd_menu_in");
		soundMenuOut = (Sound) game.resources.get("snd_menu_out");
		
		bg = container.getAspectRatio() == 4f/3f ? (Image) game.resources.get("bg_title") : (Image) game.resources.get("bg_title_wide");
		img_opcoes = (Image) game.resources.get("img_txt_options");
		
		menu = new Menu(new Rectangle(w * 0.05f, 128f, w * 0.9f, h-64f));
		menu.entries.add(new Menu.Entry("Mute sound: " + (FreePacGame.Settings.muteSound? "Yes" : "No")));
		menu.entries.add(new Menu.Entry("Mute music: " + (FreePacGame.Settings.muteMusic? "Yes" : "No")));
		menu.entries.add(new Menu.Entry("Antialiasing: " + (FreePacGame.Settings.useAntialiasing? "Yes" : "No")));
		menu.entries.add(new Menu.Entry("Fullscreen: " + (FreePacGame.Settings.windowedMode? "No" : "Yes")));
		menu.entries.add(new Menu.Entry("V-Sync: " + (FreePacGame.Settings.vsync? "Yes" : "No")));
		menu.entries.add(new Menu.Entry("Difficulty: " + generateDifficultyLabel()));
		menu.entries.add(new Menu.Entry("Return to menu"));

		font = Util.deriveFont((Font) game.resources.get("fnt_ui1"), java.awt.Font.PLAIN, container.getHeight() / (menu.entries.size()*2));
		menu.itemsHeight = font.getLineHeight();
		menu.setFontColor(ColorExtra.toColor(game.resources.getProperty("ui_options_menu_color"), ColorExtra.red));
		menu.setSelectedColor(ColorExtra.toColor(game.resources.getProperty("ui_options_menu_select_color"), ColorExtra.white));
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException
	{
		menuStateMusic = ((MenuState) game.getState(StateSpec.MENU_STATE.id)).music;
		if(menuStateMusic != null)
		{
			previousMenuMusicVolume = menuStateMusic.getVolume();
			menuStateMusic.setVolume(0.25f);  // lower volume
		}
	}
	
	@Override
	public void leave(GameContainer container, StateBasedGame game)
	{
		if(menuStateMusic != null)
			menuStateMusic.setVolume(FreePacGame.Settings.muteMusic? 0.001f : previousMenuMusicVolume);  // restore volume
	}


	@Override
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		final float w = container.getWidth(), h = container.getScreenHeight();
		g.setAntiAlias(FreePacGame.Settings.useAntialiasing);

		if(!MenuState.noBG) 
			bg.draw(0, 0, container.getWidth(), container.getHeight());

		g.setColor(ColorExtra.glassSmoke);
		g.fillRect(0, 0, w, h);
		
		img_opcoes.draw((w - img_opcoes.getWidth())/2, h * 0.01f);
		
		g.setFont(font);
		menu.draw(sbg, g);
		Display.sync(60);
	}

	@Override
	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {}

	@Override
	public void keyPressed(int key, char c) {
		switch(key) {
	    case Input.KEY_DOWN:
	    	if(!FreePacGame.Settings.muteSound) soundCursor.play();
	    	menu.cursorIndex++;
	        if(menu.cursorIndex > menu.entries.size()-1) menu.cursorIndex = 0;
	        break;
	    case Input.KEY_UP:
	    	if(!FreePacGame.Settings.muteSound) soundCursor.play();
	    	menu.cursorIndex--;
	        if(menu.cursorIndex < 0) menu.cursorIndex = menu.entries.size()-1;
	        break;
	    case Input.KEY_ENTER:
	    	if(!FreePacGame.Settings.muteSound) soundMenuIn.play();
	        switch (menu.cursorIndex) 
	        {
				case 0: 	FreePacGame.Settings.muteSound = !FreePacGame.Settings.muteSound; adjustMenuItemLabelBoolean(); break;
				case 1: 	FreePacGame.Settings.muteMusic = !FreePacGame.Settings.muteMusic;	adjustMenuItemLabelBoolean(); break;
				case 2: 	FreePacGame.Settings.useAntialiasing = !FreePacGame.Settings.useAntialiasing; adjustMenuItemLabelBoolean(); break;
				case 3: 	FreePacGame.Settings.windowedMode = !FreePacGame.Settings.windowedMode; adjustMenuItemLabelBoolean(); break;
				case 4:		FreePacGame.Settings.vsync = !FreePacGame.Settings.vsync; adjustMenuItemLabelBoolean(); break;
				case 5:		FreePacGame.Settings.difficulty = (byte) ((++FreePacGame.Settings.difficulty)%3); menu.entries.get(menu.cursorIndex).label = "Difficulty: " + generateDifficultyLabel(); break;
				case 6:		game.enterState(StateSpec.MENU_STATE); break;
				default:	break;
			}
	        
	        if(menuStateMusic != null)
	        {
	        	if(FreePacGame.Settings.muteMusic)
	        		menuStateMusic.stop();
	        	else
	        	{
	        		if(menuStateMusic.playing()==false)
	        			menuStateMusic.play();
	        		menuStateMusic.setVolume(0.25f);
	        	}
	        }
	        
	        
	        Display.setVSyncEnabled(FreePacGame.Settings.vsync);
	        
	        if(game.getContainer().isFullscreen() != !FreePacGame.Settings.windowedMode) try 
	        {
				game.getContainer().setFullscreen(!FreePacGame.Settings.windowedMode);
				font = Util.deriveFont((Font) game.resources.get("fnt_ui1"), java.awt.Font.PLAIN, game.getContainer().getHeight() / (menu.entries.size()*2));
				menu.itemsHeight = font.getLineHeight();
			} 
	        catch (SlickException e) 
	        {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, e.getLocalizedMessage(), "Windowed mode on/off error", JOptionPane.ERROR_MESSAGE);	
			}
	        
	        break;
	    case Input.KEY_ESCAPE:
	    	if(!FreePacGame.Settings.muteSound) soundMenuOut.play();
	    	game.enterState(StateSpec.MENU_STATE);
	    default:
	        break;
	    }
	}
	
	private String generateDifficultyLabel()
	{
		return FreePacGame.Settings.difficulty==0? "Classic" : Integer.toString(FreePacGame.Settings.difficulty);
	}
	
	private void adjustMenuItemLabelBoolean()
	{
		String lbl = menu.entries.get(menu.cursorIndex).label;
		menu.entries.get(menu.cursorIndex).label = lbl.replace(lbl.endsWith("Yes")? "Yes" : "No", lbl.endsWith("Yes")? "No" : "Yes");
	}
	
}
