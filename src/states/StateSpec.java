package states;

import org.newdawn.slick.state.BasicGameState;

public enum StateSpec 
{
	MENU_STATE(1, MenuState.class),
	PLAY_STAGE_STATE(2, PlayStageState.class),
	STAGE_CLEAR_STATE(3, StageClearState.class),
	GAME_WON_STATE(4, GameWonState.class),
	GAME_OVER_STATE(5, GameOverState.class),
	OPTIONS_MENU_STATE(6, OptionsMenuState.class),
	LOADING_STATE(7, LoadingState.class),
	STAGE_SELECT_STATE(8, StageSelectState.class);
	
	public int id;
	public Class<? extends BasicGameState> clazz;
	
	private StateSpec(int id, Class<? extends BasicGameState> clazz)
	{
		this.id = id; this.clazz = clazz;
	}
}
