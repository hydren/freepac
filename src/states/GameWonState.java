package states;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import main.FreePacGame;
import util.ColorExtra;
import util.Util;
import util.drawing.ScrollingBackground;

public class GameWonState extends BasicGameState {
	@Override public int getID() { return StateSpec.GAME_WON_STATE.id; } //STATE ID
	ScrollingBackground bg;
	FreePacGame game;
	Music music;
	Font font;
	Color fontColor;
	
	float captionOffset = 0;
	
	public GameWonState() { }
	
	@Override
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		// TODO Auto-generated method stub
		this.game = (FreePacGame) sbg;
		bg = new ScrollingBackground("bg_game_won", game.resources);
		music = (Music) game.resources.get("mus_endgame");
		font = Util.deriveFont((Font) game.resources.get("fnt_ui2"), java.awt.Font.PLAIN, 48);
		fontColor = ColorExtra.toColor(game.resources.getProperty("ui_game_won_font_color"), Color.red);
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException
	{
		try 
		{
			 BufferedReader in
			   = new BufferedReader(new FileReader("assets/endgame.txt"));
			 
			 StringBuilder sb = new StringBuilder();
			 String s = in.readLine();
			 
			 while(s != null)
			 {
				 sb.append(s+"\n");
				 s = in.readLine();
			 }
			 
			 in.close();
			 text = sb.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(!FreePacGame.Settings.muteMusic && music != null) music.play();
	}
	
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		bg.draw(container);
		g.setFont(font);
		g.setColor(fontColor);
		g.drawString(text, container.getWidth()/4, container.getHeight() - captionOffset);
		Display.sync(60);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		float dt = delta;
		
		bg.update(delta, container);
		captionOffset += dt * 0.05f;
		
	}
	
	@Override
	public void keyPressed(int key, char c)
	{
		switch (key) {
		    case Input.KEY_ESCAPE:
		    case Input.KEY_ENTER:
		    	game.enterState(StateSpec.MENU_STATE, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
		    default:
		        break;
		}
	}
	
	private static String text = "";
}
