package states;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import main.FreePacGame;
import util.drawing.ScrollingBackground;

public class LoadingState extends BasicGameState 
{
	@Override public int getID() { return StateSpec.LOADING_STATE.id; } //STATE ID
		
	FreePacGame game;
	ScrollingBackground bg;
	
	@Override
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException 
	{
		game = (FreePacGame) sbg;
		bg = new ScrollingBackground("bg_loading", game.resources);
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException
	{
		bg.resetOffset();
	}

	@Override
	public void render(GameContainer container, StateBasedGame sbg, Graphics g) throws SlickException {
		
		// render scrolling repeated
		bg.draw(container);
		
		g.setColor(Color.darkGray);
		g.drawString("L O A D I N G . . .", container.getWidth()*0.1f, container.getHeight()*0.9f);
		Display.sync(60);
	}

	@Override
	public void update(GameContainer container, StateBasedGame sbg, int delta) throws SlickException {
		bg.update(delta, container);
	}
}
