package states;

import java.lang.reflect.Field;
import java.util.ArrayList;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.geom.Point;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.util.FastTrig;

import main.Main;
import main.FreePacGame;
import util.ColorExtra;
import util.Util;

public class MenuState extends BasicGameState 
{
	@Override public int getID() { return StateSpec.MENU_STATE.id; } //STATE ID
	Image bg, img_logo, img_play, img_stages, img_options, img_exit, img_play_s, img_stages_s, img_options_s, img_exit_s, img_stub;
	PlayableCharacterEntry[] chars;
	Sound soundCursor, soundMenuIn, soundMenuOut;
	Color colorStub;
	Point stubOffset, selectStubOffset;
	public Music music;
	int choice = 0;
	public static int charChoice = 0;
	private FreePacGame game; // stored for later use
	private Font fontUI;
	
	public static boolean noBG = false; //for debug-embarrassment-avoidance purposes
	
	public MenuState() {}
	
	@Override
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException 
	{
		this.game = (FreePacGame) sbg;
		bg = container.getAspectRatio() == 4f/3f ? (Image) game.resources.get("bg_title") : (Image) game.resources.get("bg_title_wide");
		img_stub = (Image) game.resources.get("img_stub");
		
		img_logo = (Image) game.resources.get("img_txt_logo");
		img_play = (Image) game.resources.get("img_txt_play");
		img_stages = (Image) game.resources.get("img_txt_stages");
		img_options = (Image) game.resources.get("img_txt_options");
		img_exit = (Image) game.resources.get("img_txt_exit");
		
		img_play_s = (Image) game.resources.get("img_txt_play_s");
		img_stages_s = (Image) game.resources.get("img_txt_stages_s");
		img_options_s = (Image) game.resources.get("img_txt_options_s");
		img_exit_s = (Image) game.resources.get("img_txt_exit_s");
		
		chars = PlayableCharacterEntry.load(game);
		if(charChoice > chars.length-1)
			charChoice = chars.length-1;
		ajustCharAnimSpeed();
		
		
		fontUI = Util.deriveFont((Font) game.resources.get("fnt_ui2"), java.awt.Font.ITALIC, 14);
		
		soundCursor = (Sound) game.resources.get("snd_cursor");
		soundMenuIn = (Sound) game.resources.get("snd_menu_in");
		soundMenuOut = (Sound) game.resources.get("snd_menu_out");
		music = (Music) game.resources.get("mus_title");
		colorStub = ColorExtra.toColor(game.resources.getProperty("ui_main_menu_char_select_stub_color"), ColorExtra.pumpkin);
		
		double[] aux = Util.parse(game.resources.getProperty("ui_main_menu_char_stub_offset"), 2, null);
		stubOffset = new Point((int) aux[0], (int) aux[1]);
		
		aux = Util.parse(game.resources.getProperty("ui_main_menu_char_select_stub_offset"), 2, null);
		selectStubOffset = new Point((int) aux[0], (int) aux[1]);
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException
	{
		if(!FreePacGame.Settings.muteMusic && music != null && !music.playing())
			music.loop(1f, 0.5f);
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		if(!noBG) bg.draw(0, 0, container.getWidth(), container.getHeight());
		g.setAntiAlias(FreePacGame.Settings.useAntialiasing);
		
		float w = container.getWidth(), h = container.getHeight();
		Font fontMono = g.getFont(); //grabbing the default font
		
		for(Field f : MenuState.class.getDeclaredFields()) if(f.getType().equals(Image.class) && !f.getName().equals("bg"))
		{
			f.setAccessible(true);
			try { ((Image) f.get(this)).setRotation((float) FastTrig.sin(((double) System.currentTimeMillis())*(f.getName().equals("img_logo")?0.001:0.005))*5); } 
			catch (Exception e) { e.printStackTrace(); }
			f.setAccessible(false);
		}
		
		img_logo.draw((w - img_logo.getWidth())/2, h*0.25f);
		
		if(choice==0)
			img_play_s.draw(w*0.5f, h*0.50f);
		else
			img_play.draw(w*0.5f, h*0.50f);
		
		if(choice==1)
			img_stages_s.draw(w*0.525f, h*0.60f);
		else
			img_stages.draw(w*0.525f, h*0.60f);
		
		if(choice==2)
			img_options_s.draw(w*0.55f, h*0.70f);
		else
			img_options.draw(w*0.55f, h*0.70f);
		
		if(choice==3)
			img_exit_s.draw(w*0.575f, h*0.80f);
		else
			img_exit.draw(w*0.575f, h*0.80f);
		
		img_stub.draw(w*0.21f + stubOffset.getX(), 0.75f*h + stubOffset.getY());
		g.setColor(colorStub);
		
		for(int i = Math.max(0, charChoice-2), offset=charChoice==0?chars[i].anim.getWidth()*2:charChoice==1?chars[i].anim.getWidth():0; i < Math.min(chars.length, charChoice+3); offset += chars[i].anim.getWidth(), i++)			if(i == charChoice) 
			if(i == charChoice)
				g.fillOval(w*0.25f + offset + selectStubOffset.getX(), 0.75f*h+20 + selectStubOffset.getY(), 70, 20);
		
		for(int i = Math.max(0, charChoice-2), offset=charChoice==0?chars[i].anim.getWidth()*2:charChoice==1?chars[i].anim.getWidth():0; i < Math.min(chars.length, charChoice+3); offset += chars[i].anim.getWidth(), i++)
		{
			if(i == charChoice) 
				chars[i].anim.draw(w*0.255f + offset, 0.74f*h - chars[i].anim.getHeight()/2);
			else
				chars[i].anim.draw(w*0.255f + offset, 0.75f*h - chars[i].anim.getHeight()/2);
		}
		
		g.setFont(fontUI);
		g.setColor(Color.white);
		g.drawString(chars[charChoice].name, w*0.31f, 0.8f*h);
		
		g.setFont(fontMono);
		g.drawString("v"+Main.APP_VERSION + (Main.debugMode?" (Debug mode)":""), w*0.01f, h*0.95f);
		Display.sync(60);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		if(Main.splash.isActive())
			Main.splash.dispose();
	}
	
	@Override
	public void keyPressed(int key, char c) {
		switch(key) {
	    case Input.KEY_DOWN:
	    	if(!FreePacGame.Settings.muteSound) soundCursor.play();
	        choice++;
	        if(choice > 3) choice = 0;
	        break;
	    case Input.KEY_UP:
	    	if(!FreePacGame.Settings.muteSound) soundCursor.play();
	    	choice--;
	        if(choice < 0) choice = 3;
	        break;
	    case Input.KEY_LEFT:
	    	if(!FreePacGame.Settings.muteSound) soundCursor.play();
	    	charChoice--;
	    	if(charChoice < 0) charChoice = chars.length-1;
	    	ajustCharAnimSpeed();
	    	break;
	    case Input.KEY_RIGHT:
	    	if(!FreePacGame.Settings.muteSound) soundCursor.play();
	    	charChoice++;
	        if(charChoice > chars.length-1) charChoice = 0;
	        ajustCharAnimSpeed();
	        break;
	    case Input.KEY_ENTER:
	    	if(!FreePacGame.Settings.muteSound) soundMenuIn.play();
	        select();
	        break;
	    case Input.KEY_ESCAPE:
	    	if(!FreePacGame.Settings.muteSound) soundMenuOut.play();
	    		Main.closeGame(game, 0);
//    	
//	    case Input.KEY_HOME:
//	    	game.enterState(StateSpec.LOADING_STATE); //XXX DEBUG
//	    	break;
//	    case Input.KEY_PRIOR:
//	    	game.enterState(StateSpec.STAGE_CLEAR_STATE);//XXX DEBUG
//	    	break;
//	    case Input.KEY_NEXT:
//	    	game.enterState(StateSpec.GAME_WON_STATE);//XXX DEBUG
//	    	break;
//	    case Input.KEY_END:
//	    	game.enterState(StateSpec.GAME_OVER_STATE);//XXX DEBUG
//	    	break;
	    	
	    default:
	        break;
	    }
	}
	
	private void select()
	{
		switch (choice) {
		case 0:
			if(music != null) music.stop();
			FreePacGame.resetGlobalGameStatus();
			PlayStageState.isCareer = true;
			PlayStageState.stageFilename = game.stagePlaylist.get(0);
			if(PlayStageState.stageNumber == -1337) PlayStageState.stageFilename = game.stagePlaylist.get(1); //XXX PUBLIC DEV LINE
			game.enterState(StateSpec.PLAY_STAGE_STATE, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
			break;
			
		case 1:
			game.enterState(StateSpec.STAGE_SELECT_STATE);
			break;
			
		case 2:
			game.enterState(StateSpec.OPTIONS_MENU_STATE.id);
			break;
			
		default:
			Main.closeGame(game, 0);
			break;
		}
	}
	
	private static class PlayableCharacterEntry
	{
		public String name;
		public Animation anim;
		public static PlayableCharacterEntry[] load(FreePacGame game)
		{
			ArrayList<PlayableCharacterEntry> list = new ArrayList<MenuState.PlayableCharacterEntry>();
			for(int i = 0; i < game.playableCharactersRefs.size(); i++)
			{
				if(game.resources.getProperty(game.playableCharactersRefs.get(i)+".unlock") != null 
				&& Byte.parseByte(game.resources.getProperty(game.playableCharactersRefs.get(i)+".unlock")) > FreePacGame.Settings.unlocked) 
					continue;
				PlayableCharacterEntry pc = new PlayableCharacterEntry();
				pc.name = game.resources.getProperty(game.playableCharactersRefs.get(i)+".name");
				pc.anim = (Animation) game.resources.get(game.playableCharactersRefs.get(i)+"_char");
				list.add(pc);
			}
			return list.toArray(new PlayableCharacterEntry[0]);
		}
	}
	
	public String unlockedCharacter(FreePacGame game)
	{
		String unlockeds = "";
		char1: for(int i = 0; i < game.playableCharactersRefs.size(); i++)
		{
			if(game.resources.getProperty(game.playableCharactersRefs.get(i)+".unlock") != null)
			{
				for(PlayableCharacterEntry pc : chars)
					if(pc.name.equals(game.resources.getProperty(game.playableCharactersRefs.get(i)+".name")))
						continue char1;
				if(FreePacGame.Settings.unlocked >= Byte.parseByte(game.resources.getProperty(game.playableCharactersRefs.get(i)+".unlock")))
					unlockeds += ", " + game.resources.getProperty(game.playableCharactersRefs.get(i)+".name");
			}
		}
		if(unlockeds.equals(""))
			return null;
		
		chars = PlayableCharacterEntry.load(game);
		return unlockeds.substring(2);
	}
	
	void ajustCharAnimSpeed()
	{
		for(PlayableCharacterEntry pc : chars) pc.anim.setSpeed(0.0001f);
		chars[charChoice].anim.setSpeed(1);
	}

}
