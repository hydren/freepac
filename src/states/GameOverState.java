package states;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import main.FreePacGame;
import util.ColorExtra;
import util.Timer;

public class GameOverState extends BasicGameState 
{
	@Override public int getID() { return StateSpec.GAME_OVER_STATE.id; } //STATE ID
	
	Image bg;
	Image loomy;
	FreePacGame game;
	Music music;
	Color fontColor;
	
	Timer loomyTimer = new Timer(13332);
	float loomyScale = 0.5f;
	Music loomyMusic;
	public static int loomyCount = 0; 
	
	
	@Override
	public void init(GameContainer container, StateBasedGame sbg) throws SlickException {
		this.game = (FreePacGame) sbg;
		bg = (Image) game.resources.get("bg_game_over");
		music = (Music) game.resources.get("mus_gameover");
		loomy = (Image) game.resources.get("img_loomy");
		loomyMusic = (Music) game.resources.get("mus_loomy");
		fontColor = ColorExtra.toColor(game.resources.getProperty("ui_game_over_font_color"), Color.red);
	}
	
	@Override
	public void enter(GameContainer container, StateBasedGame game) throws SlickException
	{
		loomyCount++;
		loomyTimer.reset();
		loomyScale = 0.5f;
		if(!FreePacGame.Settings.muteMusic && music != null) music.loop();
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		bg.draw(0, 0, container.getWidth(), container.getHeight());
		if(loomyTimer.timeCompleted() && loomyCount%6==1)
		{
			g.setColor(ColorExtra.glassSmoke);
			g.fillRect(0, 0, container.getWidth(), container.getHeight());
			loomy.draw(container.getWidth()/2 - loomyScale*loomy.getWidth()/2, container.getHeight()/2 - loomyScale*loomy.getHeight()/2, loomyScale);
		}
		Display.sync(60);
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException 
	{
		if(loomyTimer.timeCompleted() && loomyCount%6==1)
		{
			loomyScale += 0.005 * (float)delta/(float)PlayStageState.BLOCK_SIZE;
			if(loomyMusic.playing()==false)
				loomyMusic.loop();
		}
	}
	
	@Override
	public void keyPressed(int key, char c)
	{
		switch (key) {
		    case Input.KEY_ESCAPE:
		    case Input.KEY_ENTER:
		    	game.enterState(StateSpec.MENU_STATE, new FadeOutTransition(Color.black), new FadeInTransition(Color.black));
		    default:
		        break;
		}
	}
}
