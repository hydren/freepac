package main;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import states.PlayStageState;

public class Main 
{
	public static final String APP_VERSION = "1.3.0";
	
	public static boolean debugMode = false;
	
	public static GameSplash splash;
	
	public static void main(String[] args)
	{
		try
		{
			FreePacGame.Settings.load("game.properties");
			
			splash = new GameSplash();
			splash.show();
			
			//int width, height;
			//scalable code, kinda buggy
			//Game game = new ScalableGame(new FreePacGame(), 1600, 900, false);
			//AppGameContainer appgc = new AppGameContainer(game);
			//width = (int) (0.9f * (float) appgc.getScreenWidth());
			//height = (int) (0.9f * (float) appgc.getScreenHeight());
			////width = 800; height = 600;
			
			FreePacGame game = new FreePacGame();
			game.loadStagesList();
			game.loadPlayableCharacters();
			game.loadCustomEnemies();
			game.loadMiscSettings();
			AppGameContainer appgc = new AppGameContainer(game);
			
			appgc.setMaximumLogicUpdateInterval(PlayStageState.BLOCK_SIZE-1); //experimental
			appgc.setDisplayMode(FreePacGame.Settings.preferredDisplayMode.getWidth(), FreePacGame.Settings.preferredDisplayMode.getHeight(), !FreePacGame.Settings.windowedMode);
			appgc.setIcon("assets/icon.png");
			appgc.start();
		}
		catch (SlickException ex)
		{
			JOptionPane.showMessageDialog(null, "Critical error:\n"+ex.getLocalizedMessage(), "Critical error", JOptionPane.ERROR_MESSAGE);
			Logger.getLogger(FreePacGame.class.getName()).log(Level.SEVERE, null, ex);
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(null, "Critical error:\n"+e.getLocalizedMessage(), "Critical error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		finally {
			if(Main.splash != null && Main.splash.isActive())
				Main.splash.dispose();
		}
	}
	
	public static void closeGame(FreePacGame game, int statusCode)
	{
		FreePacGame.Settings.save("game.properties");
		if(game != null) ((AppGameContainer) game.getContainer()).destroy();
		System.exit(statusCode);
	}

}
