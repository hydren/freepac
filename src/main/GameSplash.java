package main;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class GameSplash 
{
	private JFrame window;
	private boolean isDisposed = false;
	public void show() throws IOException
	{
		Image splashImage = ImageIO.read(new File("assets/ui/splash.jpg"));
		window = new JFrame("Carregando...");
		window.setIconImage(ImageIO.read(new File("assets/icon.png")));
		window.setSize(splashImage.getWidth(null), splashImage.getHeight(null));
		
		JLabel image = new JLabel(new ImageIcon(splashImage));
		window.add(image);
		window.setUndecorated(true);
		window.setLocationRelativeTo(null);
		window.setAlwaysOnTop(true);
		window.setVisible(true);
	}
	
	public void dispose()
	{
		window.dispose();
		isDisposed = true;
	}
	
	public boolean isActive()
	{
		return !isDisposed;
	}
}
