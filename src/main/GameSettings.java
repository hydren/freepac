package main;

import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import states.MenuState;
import util.Util;

public class GameSettings 
{
	public boolean 
	useAntialiasing = true, 
	muteSound = false,
	muteMusic = false,
	windowedMode = false,
	vsync = false;
	public byte unlocked=0, difficulty=0;
	public long wantedDirTimeout=-1;
	public DisplayMode preferredDisplayMode=null;
	
	public void load(String filename) throws UnsupportedEncodingException, FileNotFoundException, IOException, HeadlessException, LWJGLException, IllegalArgumentException, IllegalAccessException
	{
		File gameSettingsFile = new File(filename);
		
		//default options
		Properties p = new Properties();
		p.setProperty("debugmode", "false");
		p.setProperty("character", "0");
		p.setProperty("publicdev", "false"); //special option

		if(gameSettingsFile.isFile())
		{
			InputStreamReader reader = new InputStreamReader(new FileInputStream(gameSettingsFile), "UTF-8");
			p.load(reader);
			reader.close();
		}
		
		//~~~~~~~~~~~~ override settings ~~~~~~~~~~~~~~~~~
		
		for(Field field : GameSettings.class.getFields())
		{
			String property = p.getProperty(field.getName());
			if(property != null && (property=property.trim()).length() > 0)
			{
				if(field.getType() == String.class)	 field.set(this, property);
				if(field.getType() == boolean.class) field.set(this, Boolean.parseBoolean(property));
				else try
				{
					if(field.getType() == byte.class) 	field.set(this, Byte.parseByte(property));
					if(field.getType() == short.class) 	field.set(this, Short.parseShort(property));
					if(field.getType() == int.class) 	field.set(this, Integer.parseInt(property));
					if(field.getType() == long.class) 	field.set(this, Long.parseLong(property));
				}
				catch (NumberFormatException e) 
				{
					System.out.println("Could not load " + field.getName()+": "+property);
				}
			}
		}

		//~~~~~~~~~~~~ custom override settings ~~~~~~~~~~~~~~~~~

		Main.debugMode = Boolean.parseBoolean(p.getProperty("debugmode"));

		try{ MenuState.charChoice = Integer.parseInt(p.getProperty("character")); } catch (NumberFormatException e) { System.out.println("Cant load character index!"); e.printStackTrace(); }

		preferredDisplayMode = null;
		if(p.getProperty("resolution") != null) // width x resolution
		{
			if(p.getProperty("resolution").split("x").length==2)
			{
				String[] res = p.getProperty("resolution").split("x");
				preferredDisplayMode = new DisplayMode(Integer.parseInt(res[0].trim()), Integer.parseInt(res[1].trim()));
			}
			else if(p.getProperty("resolution").trim().equalsIgnoreCase("auto"))
				preferredDisplayMode = Display.getDesktopDisplayMode();
		}
		
		//could not make sense of resolution, ask user
		if(preferredDisplayMode == null)
		{
			ArrayList<DisplayMode> modes = new ArrayList<DisplayMode>();
			for(DisplayMode dm : Display.getAvailableDisplayModes()) modes.add(dm);
			Collections.sort(modes, Util.DISPLAY_MODE_DIAGONAL_COMPARATOR);
			ArrayList<String> list = new ArrayList<String>();
			list.add(0, "Auto");
			for(DisplayMode dm : modes) list.add(dm.toString());
			String[] values = list.toArray(new String[0]);
			String selection = (String) JOptionPane.showInputDialog(null, "Select game resolution", "Select game resolution", JOptionPane.QUESTION_MESSAGE, null, values, null);
			if(selection == null) //leave if cancel
				Main.closeGame(null, 0);
			if(selection.equalsIgnoreCase("auto"))  //selected auto
			{
				preferredDisplayMode = Display.getDesktopDisplayMode();
				windowedMode = false;
			}
			else
			{
				for(DisplayMode dm : Display.getAvailableDisplayModes()) 
					if(dm.toString().equals(selection))
					{
						preferredDisplayMode = dm;
						break;
					}
				windowedMode = JOptionPane.showConfirmDialog(null, "Run in fullscreen?", "Select windowing", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION;
			}
		}

		Display.setVSyncEnabled(FreePacGame.Settings.vsync);

		if(Boolean.parseBoolean(p.getProperty("publicdev")))
			Util.avoidDeveloperEmbarrassment(); //for embarrassment-avoidance purposes
	}
	
	public void save(String filename)
	{
		File gameSettingsFile = new File(filename);
		
		if(!gameSettingsFile.isFile())
			try {
				gameSettingsFile.createNewFile();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		Properties p = new Properties();
		try // load current properties on file
		{
			InputStreamReader reader = new InputStreamReader(new FileInputStream(gameSettingsFile), "UTF-8");
			p.load(reader);
			reader.close();
		}
		catch(Exception e) { e.printStackTrace(); }
		
		for(Field field : GameSettings.class.getFields()) try
		{
			String property = null;
			if(field.getType() == String.class)	 property = (String) field.get(this);
			if(field.getType() == boolean.class) property = Boolean.toString(field.getBoolean(this));
			else try
			{
				if(field.getType() == byte.class) property = Byte.toString(field.getByte(this));
				if(field.getType() == short.class) property = Short.toString(field.getShort(this));
				if(field.getType() == int.class) property = Integer.toString(field.getInt(this));
				if(field.getType() == long.class) property = Long.toString(field.getLong(this));
			}
			catch (NumberFormatException e) 
			{
				System.out.println("Could not save " + field.getName()+": "+e.getLocalizedMessage());
			}
			if(property != null) p.setProperty(field.getName(), property);
		}
		catch (IllegalArgumentException e) {
			System.out.println("Could not save settings " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		catch (IllegalAccessException e) {
			System.out.println("Could not save settings " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		
		p.setProperty("debugmode", Boolean.toString(Main.debugMode));
		p.setProperty("character", Integer.toString(MenuState.charChoice));
		if(preferredDisplayMode != null) p.setProperty("resolution", preferredDisplayMode.getWidth()+"x"+preferredDisplayMode.getHeight());

		try
		{
			FileOutputStream stream = new FileOutputStream(gameSettingsFile);
			p.store(stream, "Automatically generated settings file");
			stream.close();
		}
		catch(Exception e) { e.printStackTrace(); }
	}
}
