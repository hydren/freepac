package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Scanner;
import java.util.TreeSet;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.Transition;

import entities.Enemy.CustomEnemySpecification;
import entities.EnemyBehavior;
import entities.Item;
import states.PlayStageState;
import states.StateSpec;
import util.ColorExtra;
import util.Util;
import util.memory.CachedResourceManager;
import util.memory.ResourceManager;

public class FreePacGame extends StateBasedGame
{
	public final ResourceManager resources;
	public final ArrayList<String> stagePlaylist = new ArrayList<String>();
	public final ArrayList<String> playableCharactersRefs = new ArrayList<String>();
	public final ArrayList<CustomEnemySpecification> customEnemySpecs = new ArrayList<CustomEnemySpecification>();
	
	public static final GameSettings Settings = new GameSettings();
	
	/** Current player score */
	public static int score = 0, lives = 5, extraLifeCounter = 0;
	
	public FreePacGame() throws FileNotFoundException, IOException, SlickException 
	{
		super("Free-Pac ("+Main.APP_VERSION+")");
		resources = new CachedResourceManager();
	}
	
	@Override
	public void initStatesList(GameContainer container) throws SlickException {
		try{ for(StateSpec state : StateSpec.values()) addState(state.clazz.newInstance()); }
		catch(InstantiationException e) { throw new SlickException(e.getMessage(), e); }
		catch(IllegalAccessException e) { throw new SlickException(e.getMessage(), e); }
	}
	
	public void enterState(StateSpec state)
	{
		this.enterState(state.id);
	}
	
	/** Enter a particular game state with the transitions provided */
	public void enterState(StateSpec state, Transition leave, Transition enter)
	{
		this.enterState(state.id, leave, enter);
	}
	
	/** Resets all variables related to the game globaly, like remaining lives count, score, etc. */
	public static void resetGlobalGameStatus()
	{
		FreePacGame.score = 0;
    	FreePacGame.lives = 5;
    	FreePacGame.extraLifeCounter = 0;
	}
	
	public int getStageNumber(String stageFilename)
	{
		for(String stageFile : stagePlaylist) 
			if(stageFile.equals(stageFilename)) 
				return stagePlaylist.indexOf(stageFile)+1; 
		
		return -1;
	}
	
	public void loadStagesList() throws FileNotFoundException, Exception
	{
		File playlistFile = new File("stages/playlist.txt");
		Scanner scanner = new Scanner(playlistFile);
		try
		{
			System.out.println("Loading stage playlist...");
			while(scanner.hasNextLine())
			{
				File stageFile = new File("stages/"+scanner.nextLine());
				if(stageFile.isFile())
				{
					System.out.println("Found " + stageFile.getPath());
					stagePlaylist.add(stageFile.getPath());
				}
				else System.out.println("Ignoring invalid stage filename "+stageFile.getPath());
			}
		}
		catch(Exception e) { throw e; }
		finally { scanner.close(); }
		
		if(stagePlaylist.size() < 1) throw new Exception("No stages found on playlist!");
		System.out.println("Stage playlist loaded. Found "+stagePlaylist.size());
	}
	
	public void loadPlayableCharacters() throws Exception
	{
		System.out.println("Loading playable characters...");
		for(int i = 0; i < 32; i++) //XXX hard-coded 32 characters limit
		{
			if(resources.getProperty("anim_player"+i) == null) continue;
			if(resources.getProperty("anim_player"+i+".name") == null) continue;
			if(resources.getProperty("anim_player"+i+"_char") == null) continue;
			playableCharactersRefs.add("anim_player"+i);
			System.out.println("Loaded playable character "+i + " named \""+resources.getProperty("anim_player"+i+".name")+"\"");
		}
		if(playableCharactersRefs.size() < 1) throw new Exception("No readable playable characters characters found!");
	}
	
	public void loadCustomEnemies() throws IOException
	{
		System.out.println("Loading custom enemies...");
		Properties p = new Properties();
		InputStreamReader reader = new InputStreamReader(new FileInputStream(new File("enemies.conf.properties")), "UTF-8");
		p.load(reader);
		reader.close();
		
		String label = "custom_enemy";
		TreeSet<Integer> usedIndexes = new TreeSet<Integer>();
		for(Entry<Object, Object> e : p.entrySet()) try{
			if(((String) e.getKey()).startsWith(label) && !((String) e.getKey()).contains("."))
				usedIndexes.add(Integer.parseInt(((String) e.getKey()).substring(label.length()).split("=")[0]));
		}catch (Exception e2) { e2.printStackTrace(); }
		
		for(Integer i : usedIndexes) try
		{
			CustomEnemySpecification spec = new CustomEnemySpecification();
			spec.name = p.getProperty(label+i);
			spec.behavior = (EnemyBehavior) EnemyBehavior.class.getField(p.getProperty(label+i+".behavior").toUpperCase()).get(null);
			spec.isFaster = Boolean.parseBoolean(p.getProperty(label+i+".behavior.fast"));
			spec.isInvulnerable = Boolean.parseBoolean(p.getProperty(label+i+".behavior.invulnerable"));
			spec.isVariableSpeed = Boolean.parseBoolean(p.getProperty(label+i+".behavior.variable_speed"));
			spec.sprite_ref = p.getProperty(label+i+".anim");
			spec.sprite_at = Integer.parseInt(p.getProperty(label+i+".anim.time"));
			String[] tokens = p.getProperty(label+i+".anim.size").split(",");
			if(tokens.length == 2)
			{
				spec.sprite_tw = Integer.parseInt(tokens[0]);
				spec.sprite_th = Integer.parseInt(tokens[1]);
			}
			else spec.sprite_tw = spec.sprite_th = Integer.parseInt(p.getProperty(label+i+".anim.size"));
			customEnemySpecs.add(spec);
			System.out.println("Loaded custom enemy named \"" + spec.name + "\"");
		}
		catch (Exception e) { e.printStackTrace(); }
		System.out.println(customEnemySpecs);
	}
	
	public void loadMiscSettings()
	{
		System.out.println("Loading misc. settings...");
		
		Item.PELLET_COLOR = ColorExtra.toColor(resources.getProperty("game_pellet_color"), Color.green);
		Item.ENERGIZER_COLOR = ColorExtra.toColor(resources.getProperty("game_energizer_color"), ColorExtra.pumpkin);
		Item.WAVE_PELLETS = Boolean.parseBoolean(resources.getProperty("cfg_wave_pellets"));
		Item.WAVE_BONUS_PELLETS = Boolean.parseBoolean(resources.getProperty("cfg_wave_bonus_pellets"));

		//careful with this one
		PlayStageState.BLOCK_SIZE = (int) Util.parse(resources.getProperty("cfg_block_size"), PlayStageState.BLOCK_SIZE);
		
		PlayStageState.HUD_SHAKE_INTENSITY = (int) Util.parse(resources.getProperty("cfg_hud_shake_intensity"), PlayStageState.HUD_SHAKE_INTENSITY);
		PlayStageState.WAVE_EFFECT_MAGNETUDE = (float) Util.parse(resources.getProperty("cfg_wave_magnetude"), PlayStageState.WAVE_EFFECT_MAGNETUDE);
		PlayStageState.ENERGIZER_REWARD = (int) Util.parse(resources.getProperty("cfg_energizer_reward"), PlayStageState.ENERGIZER_REWARD);
		PlayStageState.PELLET_REWARD = (int) Util.parse(resources.getProperty("cfg_pellet_reward"), PlayStageState.PELLET_REWARD);
		PlayStageState.POWER_MODE_DURATION = (long) Util.parse(resources.getProperty("cfg_power_mode_duration"), PlayStageState.POWER_MODE_DURATION);
		PlayStageState.ENEMY_PELLET_ABSTINENCE_TIMEOUT = (long) Util.parse(resources.getProperty("cfg_enemy_pellet_abstinence_timeout"), PlayStageState.ENEMY_PELLET_ABSTINENCE_TIMEOUT);
	}
} 
