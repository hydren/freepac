package util.drawing;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.opengl.TextureImpl;
import org.newdawn.slick.opengl.renderer.SGL;
import org.newdawn.slick.util.FastTrig;

public class CustomGraphics extends Graphics
{
	Color currentColor;
	boolean antialias;
	
	public CustomGraphics(Graphics g)
	{
		super();
		currentColor = g.getColor();
		antialias = g.isAntiAlias();
	}
	
	/**
	 * Fill the complement of an arc to the canvas (a curved slope)
	 * 
	 * @param x1
	 *            The x coordinate of the top left corner of a box containing
	 *            the arc
	 * @param y1
	 *            The y coordinate of the top left corner of a box containing
	 *            the arc
	 * @param width
	 *            The width of the arc
	 * @param height
	 *            The height of the arc
	 * @param segments
	 *            The number of line segments to use when filling the arc
	 * @param start
	 *            The angle the arc starts at
	 * @param end
	 *            The angle the arc ends at
	 */
	public void fillArcComplement(float x1, float y1, float width, float height,
			int segments, float start, float end) {
		predraw();
		TextureImpl.bindNone();
		currentColor.bind();

		while (end < start) {
			end += 360;
		}

		final float cx = x1 + (width / 2.0f);
		final float cy = y1 + (height / 2.0f);
		
//		GL.glBegin(SGL.GL_TRIANGLE_FAN);
		final int step = 360 / segments;

//		GL.glVertex2f(cx, cy);
		short currentCorner = 0;

		for (int a = (int) start; a < (int) (end + step); a += step) {
			float ang = a;
			if (ang > end) {
				ang = end;
			}
			
			//specify corner point to be gl triangle fan center
			int b = a % 360;
			if(b >= 0 && b < 91 && currentCorner != 1) 
			{
				currentCorner = 1;
				GL.glEnd();
				GL.glBegin(SGL.GL_TRIANGLE_FAN);
				GL.glVertex2f(x1 + width, y1+ height);
			}
			else if(b > 90 && b < 181 && currentCorner != 2) 
			{
				currentCorner = 2;
				GL.glEnd();
				GL.glBegin(SGL.GL_TRIANGLE_FAN);
				GL.glVertex2f(x1, y1 + height);
			}
			else if(b > 180 && b < 271 && currentCorner != 3) 
			{
				currentCorner = 3;
				GL.glEnd();
				GL.glBegin(SGL.GL_TRIANGLE_FAN);
				GL.glVertex2f(x1, y1);
			}
			else if(b > 270 && b < 360 && currentCorner != 4) 
			{
				currentCorner = 4;
				GL.glEnd();
				GL.glBegin(SGL.GL_TRIANGLE_FAN);
				GL.glVertex2f(x1 + width, y1);
			}

			final float x = (float) (cx + (FastTrig.cos(Math.toRadians(ang)) * width / 2.0f));
			final float y = (float) (cy + (FastTrig.sin(Math.toRadians(ang)) * height / 2.0f));

			GL.glVertex2f(x, y);
		}
		GL.glEnd();

//		if (antialias) {
//			GL.glBegin(SGL.GL_TRIANGLE_FAN);
//			GL.glVertex2f(cx, cy);
//			if (end != 360) {
//				end -= 10;
//			}
//
//			for (int a = (int) start; a < (int) (end + step); a += step) {
//				float ang = a;
//				if (ang > end) {
//					ang = end;
//				}
//
//				float x = (float) (cx + (FastTrig.cos(Math.toRadians(ang + 10))
//						* width / 2.0f));
//				float y = (float) (cy + (FastTrig.sin(Math.toRadians(ang + 10))
//						* height / 2.0f));
//
//				GL.glVertex2f(x, y);
//			}
//			GL.glEnd();
//		}

		postdraw();
	}
	
	/**
	 * Fill the complement of an arc to the canvas (a curved slope)
	 * 
	 * @param x1
	 *            The x coordinate of the top left corner of a box containing
	 *            the arc
	 * @param y1
	 *            The y coordinate of the top left corner of a box containing
	 *            the arc
	 * @param width
	 *            The width of the arc
	 * @param height
	 *            The height of the arc
	 * @param start
	 *            The angle the arc starts at
	 * @param end
	 *            The angle the arc ends at
	 */
	public void fillArcComplement(float x1, float y1, float width, float height,
			float start, float end) {
		fillArcComplement(x1, y1, width, height, 50, start, end);
	}
	
	public void fillPartialDisk(float x1, float y1, float width, float height, 
			int segments, float start, float end, float innerWidth, float innerHeight) {
		predraw();
		TextureImpl.bindNone();
		currentColor.bind();

		while (end < start) {
			end += 360;
		}

		final float cx = x1 + (width / 2.0f);
		final float cy = y1 + (height / 2.0f);

		GL.glBegin(GL11.GL_QUAD_STRIP);
		final int step = 360 / segments;

		for (int a = (int) start; a < (int) (end + step); a += step) {
			float ang = a;
			if (ang > end) {
				ang = end;
			}

			final float x_inner = (float) (cx + (FastTrig.cos(Math.toRadians(ang)) * innerWidth / 2.0f));
			final float y_inner = (float) (cy + (FastTrig.sin(Math.toRadians(ang)) * innerHeight / 2.0f));

			GL.glVertex2f(x_inner, y_inner);
			
			final float x = (float) (cx + (FastTrig.cos(Math.toRadians(ang)) * width / 2.0f));
			final float y = (float) (cy + (FastTrig.sin(Math.toRadians(ang)) * height / 2.0f));

			GL.glVertex2f(x, y);
		}
		GL.glEnd();
		postdraw();
	}
	
	public void fillPartialDisk(float x1, float y1, float width, float height, 
			float start, float end, float innerWidth, float innerHeight) {
		fillPartialDisk(x1, y1, width, height, 50, start, end, innerWidth, innerHeight);
	}
	
	/**
	 * XXX COPIED FROM org.newdawn.slick.Graphics DUE TO SCOPE LIMITATIONS<br>
	 * Must be called before all OpenGL operations to maintain context for
	 * dynamic images
	 */
	private void predraw() {
		setCurrent(this);
	}
	
	/**
	 * XXX COPIED FROM org.newdawn.slick.Graphics DUE TO SCOPE LIMITATIONS<br>
	 * Must be called after all OpenGL operations to maintain context for
	 * dynamic images
	 */
	private void postdraw() {
	}
	
	@Override
	public void setColor(Color c)
	{
		super.setColor(c);
		this.currentColor.r = c.r;
		this.currentColor.g = c.g;
		this.currentColor.b = c.b;
		this.currentColor.a = c.a;
	}
	
	@Override
	public void setAntiAlias(boolean choice)
	{
		super.setAntiAlias(choice);
		this.antialias = choice;
	}

}
