package util.drawing;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Vector2f;

import util.memory.ResourceManager;

public class ScrollingBackground 
{
	Image bg;
	Vector2f offset;
	public float speed;
	
	public ScrollingBackground(String bg_ref, ResourceManager manager) 
	{
		offset = new Vector2f();
		bg = (Image) manager.get(bg_ref);
		speed = 1;
	}
	
	public void update(float delta, GameContainer container)
	{
		float dt = (float) delta;
		offset.x += dt/10f * speed;
		offset.y += dt/10f * speed;
		
		if(offset.x >= 0)
		{
			offset.x -= bg.getWidth();
		}
		if(offset.y >= 0)
		{
			offset.y -= bg.getHeight();
		}
	}
	
	public void draw(GameContainer container)
	{
		// render scrolling repeated
		renderRepeaded(offset.x, offset.y, container.getWidth(), container.getHeight());
	}
	
	public void resetOffset()
	{
		offset.x = 0; offset.y = 0;
	}
	
	private void renderRepeaded(float x, float y, int width, int height)
	{
		for(int i = 0; i < max(width, height) / bg.getWidth() + 2; i++) for(int j = 0; j < max(width, height) / bg.getHeight() + 2; j++)
			bg.draw(x + i*bg.getWidth(), y + j*bg.getHeight());
	}
	
	private float max(float a, float b)
	{
		return a > b ? a : b;
	}
	
}
