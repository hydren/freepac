package util.drawing;

import static states.PlayStageState.BLOCK_SIZE;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import entities.Block;

public class SimpleWallDrawer extends FreePacWallDrawer
{
	public SimpleWallDrawer(Graphics g, Color wallColor, Color borderColor) {
		super(g, wallColor, borderColor);
	}
	
	public void drawFirstQuadrant(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE/2);
		
		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x+BLOCK_SIZE/2, y, x+BLOCK_SIZE/2, y+BLOCK_SIZE/2);
		g.drawLine(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, x+BLOCK_SIZE, y+BLOCK_SIZE/2);
		g.setLineWidth(prevThickness);
	}
	
	public void drawSecondQuadrant(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE/2);
		
		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x+BLOCK_SIZE/2, y, x+BLOCK_SIZE/2, y+BLOCK_SIZE/2);
		g.drawLine(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, x, y+BLOCK_SIZE/2);
		g.setLineWidth(prevThickness);
	}
	
	public void drawThirdQuadrant(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2);

		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x, y+BLOCK_SIZE/2, x+BLOCK_SIZE/2, y+BLOCK_SIZE/2);
		g.drawLine(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, x+BLOCK_SIZE/2, y+BLOCK_SIZE);
		g.setLineWidth(prevThickness);
	}
	
	public void drawFourthQuadrant(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2);

		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, x+BLOCK_SIZE, y+BLOCK_SIZE/2);
		g.drawLine(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, x+BLOCK_SIZE/2, y+BLOCK_SIZE);
		g.setLineWidth(prevThickness);
	}
	
	public void drawUpperHalf(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x, y, BLOCK_SIZE, BLOCK_SIZE/2);
		
		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x, y+BLOCK_SIZE/2, x+BLOCK_SIZE, y+BLOCK_SIZE/2);
		g.setLineWidth(prevThickness);
	}
	
	public void drawBottomHalf(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE/2);
		
		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x, y+BLOCK_SIZE/2, x+BLOCK_SIZE, y+BLOCK_SIZE/2);
		g.setLineWidth(prevThickness);
	}
	
	public void drawLeftHalf(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE);
		
		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x+BLOCK_SIZE/2, y, x+BLOCK_SIZE/2, y+BLOCK_SIZE);
		g.setLineWidth(prevThickness);
	}
	
	public void drawRightHalf(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE);
		
		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x+BLOCK_SIZE/2, y, x+BLOCK_SIZE/2, y+BLOCK_SIZE);
		g.setLineWidth(prevThickness);
	}
	
	public void drawExceptFirstQuadrant(float x, float y, boolean fillOutward)
	{
		g.setColor(wallColor);
		//g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE/2);  //1st
		g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE/2);				//2nd
		g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2);	//3rd
		g.fillRect(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2); //4th
		
		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x+BLOCK_SIZE/2, y, x+BLOCK_SIZE/2, y+BLOCK_SIZE/2);
		g.drawLine(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, x+BLOCK_SIZE, y+BLOCK_SIZE/2);
		g.setLineWidth(prevThickness);
	}
	
	public void drawExceptSecondQuadrant(float x, float y, boolean fillOutward)
	{
		g.setColor(wallColor);
		g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE/2);  //1st
		//g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE/2);				//2nd
		g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2);	//3rd
		g.fillRect(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2); //4th
		
		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x+BLOCK_SIZE/2, y, x+BLOCK_SIZE/2, y+BLOCK_SIZE/2);
		g.drawLine(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, x, y+BLOCK_SIZE/2);
		g.setLineWidth(prevThickness);
	}
	
	public void drawExceptThirdQuadrant(float x, float y, boolean fillOutward)
	{
		g.setColor(wallColor);
		g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE/2);  //1st
		g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE/2);				//2nd
		//g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2);	//3rd
		g.fillRect(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2); //4th

		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x, y+BLOCK_SIZE/2, x+BLOCK_SIZE/2, y+BLOCK_SIZE/2);
		g.drawLine(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, x+BLOCK_SIZE/2, y+BLOCK_SIZE);
		g.setLineWidth(prevThickness);
	}
	
	public void drawExceptFourthQuadrant(float x, float y, boolean fillOutward)
	{
		g.setColor(wallColor);
		g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE/2);  //1st
		g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE/2);				//2nd
		g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2);	//3rd
		//g.fillRect(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2); //4th

		//border
		float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, x+BLOCK_SIZE, y+BLOCK_SIZE/2);
		g.drawLine(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, x+BLOCK_SIZE/2, y+BLOCK_SIZE);
		g.setLineWidth(prevThickness);
	}
	
	public void drawFull(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x, y, BLOCK_SIZE, BLOCK_SIZE);
	}

}
