package util.drawing;

import static states.PlayStageState.BLOCK_SIZE;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import entities.Block;

public class RoundedEdgeWallDrawer extends FreePacWallDrawer
{
	public RoundedEdgeWallDrawer(Graphics g, Color wallColor, Color borderColor) {
		super(g, wallColor, borderColor);
	}
	
	public void drawFirstQuadrant(float x, float y)
	{
		g.setColor(wallColor);
		g.fillArc(x+BLOCK_SIZE/2, y-BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 90, 180);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawArc(x+BLOCK_SIZE/2, y-BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 8, 90, 180);
		g.setLineWidth(prevThickness);
	}
	
	public void drawSecondQuadrant(float x, float y)
	{
		g.setColor(wallColor);
		g.fillArc(x-BLOCK_SIZE/2, y-BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 0, 90);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawArc(x-BLOCK_SIZE/2, y-BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 8, 0, 90);
		g.setLineWidth(prevThickness);
	}
	
	public void drawThirdQuadrant(float x, float y)
	{
		g.setColor(wallColor);
		g.fillArc(x-BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 270, 0);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawArc(x-BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 8, 270, 0);
		g.setLineWidth(prevThickness);
	}
	
	public void drawFourthQuadrant(float x, float y)
	{
		g.setColor(wallColor);
		g.fillArc(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 180, 270);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawArc(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 8, 180, 270);
		g.setLineWidth(prevThickness);
	}
	
	public void drawUpperHalf(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x, y, BLOCK_SIZE, BLOCK_SIZE/2);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x, y+BLOCK_SIZE/2, x+BLOCK_SIZE, y+BLOCK_SIZE/2);
		g.setLineWidth(prevThickness);
	}
	
	public void drawBottomHalf(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE/2);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x, y+BLOCK_SIZE/2, x+BLOCK_SIZE, y+BLOCK_SIZE/2);
		g.setLineWidth(prevThickness);
	}
	
	public void drawLeftHalf(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x+BLOCK_SIZE/2, y, x+BLOCK_SIZE/2, y+BLOCK_SIZE);
		g.setLineWidth(prevThickness);
	}
	
	public void drawRightHalf(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawLine(x+BLOCK_SIZE/2, y, x+BLOCK_SIZE/2, y+BLOCK_SIZE);
		g.setLineWidth(prevThickness);
	}
	
	public void drawExceptFirstQuadrant(float x, float y, boolean fillOutward)
	{
		g.setColor(wallColor);
		if(fillOutward)
		{
			//g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE/2);  //1st
			g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE/2);				//2nd
			g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2);	//3rd
			g.fillRect(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2); //4th
			//slope
			g.fillArcComplement(x+BLOCK_SIZE/2, y-BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 90, 180);
		}
		else
			g.fillPartialDisk(x, y-BLOCK_SIZE, 2*BLOCK_SIZE, 2*BLOCK_SIZE, 90, 180, BLOCK_SIZE, BLOCK_SIZE);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawArc(x+BLOCK_SIZE/2, y-BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 8, 90, 180);
		g.setLineWidth(prevThickness);
	}
	
	public void drawExceptSecondQuadrant(float x, float y, boolean fillOutward)
	{
		g.setColor(wallColor);
		if(fillOutward)
		{
			g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE/2);  //1st
			//g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE/2);				//2nd
			g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2);	//3rd
			g.fillRect(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2); //4th
			//slope
			g.fillArcComplement(x-BLOCK_SIZE/2, y-BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 0, 90);
		}
		else
			g.fillPartialDisk(x-BLOCK_SIZE, y-BLOCK_SIZE, 2*BLOCK_SIZE, 2*BLOCK_SIZE, 0, 90, BLOCK_SIZE, BLOCK_SIZE);

		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawArc(x-BLOCK_SIZE/2, y-BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 8, 0, 90);
		g.setLineWidth(prevThickness);
	}
	
	public void drawExceptThirdQuadrant(float x, float y, boolean fillOutward)
	{
		g.setColor(wallColor);
		if(fillOutward)
		{
			g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE/2);  //1st
			g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE/2);				//2nd
			//g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2);	//3rd
			g.fillRect(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2); //4th
			//slope
			g.fillArcComplement(x-BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 270, 0);
		}
		else
			g.fillPartialDisk(x-BLOCK_SIZE, y, 2*BLOCK_SIZE, 2*BLOCK_SIZE, 270, 0, BLOCK_SIZE, BLOCK_SIZE);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawArc(x-BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 8, 270, 0);
		g.setLineWidth(prevThickness);
	}
	
	public void drawExceptFourthQuadrant(float x, float y, boolean fillOutward)
	{
		g.setColor(wallColor);
		if(fillOutward)
		{
			g.fillRect(x+BLOCK_SIZE/2, y, BLOCK_SIZE/2, BLOCK_SIZE/2);  //1st
			g.fillRect(x, y, BLOCK_SIZE/2, BLOCK_SIZE/2);				//2nd
			g.fillRect(x, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2);	//3rd
			//g.fillRect(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE/2, BLOCK_SIZE/2); //4th
			//slope
			g.fillArcComplement(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 180, 270);
		}
		else
			g.fillPartialDisk(x, y, 2*BLOCK_SIZE, 2*BLOCK_SIZE, 180, 270, BLOCK_SIZE, BLOCK_SIZE);
		
		//border
		final float prevThickness = g.getLineWidth();
		g.setLineWidth(Block.wallLineThickness);
		g.setColor(borderColor);
		g.drawArc(x+BLOCK_SIZE/2, y+BLOCK_SIZE/2, BLOCK_SIZE, BLOCK_SIZE, 8, 180, 270);
		g.setLineWidth(prevThickness);
	}
	
	public void drawFull(float x, float y)
	{
		g.setColor(wallColor);
		g.fillRect(x, y, BLOCK_SIZE, BLOCK_SIZE);
	}

}
