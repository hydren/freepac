package util.drawing;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public abstract class FreePacWallDrawer 
{
	protected CustomGraphics g;
	public Color wallColor, borderColor;

	@Deprecated
	public FreePacWallDrawer(Graphics g) {
		this(g, Color.gray, Color.darkGray);
	}
	
	public FreePacWallDrawer(Graphics g, Color wallColor, Color borderColor) {
		super();
		this.g = new CustomGraphics(g);
		this.wallColor = new Color(wallColor);
		this.borderColor = new Color(borderColor);
	}
	
	public abstract void drawFirstQuadrant(float x, float y);
	
	public abstract void drawSecondQuadrant(float x, float y);
	
	public abstract void drawThirdQuadrant(float x, float y);
	
	public abstract void drawFourthQuadrant(float x, float y);
	
	public abstract void drawUpperHalf(float x, float y);
	
	public abstract void drawBottomHalf(float x, float y);
	
	public abstract void drawLeftHalf(float x, float y);
	
	public abstract void drawRightHalf(float x, float y);
	
	public abstract void drawExceptFirstQuadrant(float x, float y, boolean fillOutward);
	
	public abstract void drawExceptSecondQuadrant(float x, float y, boolean fillOutward);
	
	public abstract void drawExceptThirdQuadrant(float x, float y, boolean fillOutward);
	
	public abstract void drawExceptFourthQuadrant(float x, float y, boolean fillOutward);
	
	public abstract void drawFull(float x, float y);
	
	public void setGraphics(CustomGraphics g)
	{
		this.g = g;
	}
	
	public CustomGraphics getGraphics()
	{
		return g;
	}
}
