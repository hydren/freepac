package util;

public class Timer 
{
	public long interval = 0; public long lastTime = 0;
	
	public Timer(long interval) {
		this.interval = interval;
		this.lastTime = System.currentTimeMillis();
	}

	public boolean timeCompletedAndReset()
	{
		if(difference() > interval)
		{
			lastTime = System.currentTimeMillis(); 
			return true;
		}
		else return false;
	}
	
	public boolean timeCompleted()
	{
		if(difference() > interval)
			return true;
		
		else return false;
	}
	
	public boolean nearTimeCompletion()
	{
		if(difference() > (75*interval)/100)
			return true;
		else return false;
	}
	
	public boolean thirdPartTimeCompleted()
	{
		if(difference() > (30*interval)/100)
			return true;
		else return false;
	}
	
	public void reset()
	{
		this.lastTime = System.currentTimeMillis();
	}
	
	public long difference()
	{
		return System.currentTimeMillis() - lastTime;
	}
	
	/** Returns a integer between 0 (inclusive) and <i>numberOfCycles</i> (exclusive) according to the global time counter. 
	 * The returned integer indicates the current cycle specified by the arguments.  
	 * <br><br> Each <i>interval</i> milliseconds, the cycle changes to the next value. If the current cycle was the last one (<i>numberOfCycles</i>-1), the cycle reverts back to 0.*/
	public static int globalCycle(long interval, int numberOfCycles)
	{
		return (int) ((System.currentTimeMillis() % (interval * numberOfCycles)) / interval);
	}
}
