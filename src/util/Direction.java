package util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

public enum Direction 
{
	UP, DOWN, LEFT, RIGHT; 
	
	public Direction opposite() 
	{
		switch (this) 
		{
			case DOWN: return UP;
			case LEFT: return RIGHT;
			case RIGHT: return LEFT;
			case UP: return DOWN;
			default: return null;
		}
	}
	
	public boolean isHorizontal()
	{
		if(this.equals(LEFT) || this.equals(RIGHT))
			return true;
		else return false;
	}
	
	public boolean isVertical()
	{
		if(this.equals(UP) || this.equals(DOWN))
			return true;
		else return false;
	}

	static private Random random = new Random(new Date().getTime());
	
	/** A manipulable list of all possible directions. */
	public static List<Direction> list; static { list = new ArrayList<Direction>(); for(Direction d : Direction.values()) list.add(d); }
	public static List<Direction> horizontals; static { horizontals = new ArrayList<Direction>(); horizontals.add(LEFT); horizontals.add(RIGHT); }
	public static List<Direction> verticals; static { verticals = new ArrayList<Direction>(); verticals.add(UP); verticals.add(DOWN); }

	/** Shuffles the static directions list */
	public static void shuffle() { Collections.shuffle(list, random); Collections.shuffle(horizontals, random); Collections.shuffle(verticals, random);}
}
