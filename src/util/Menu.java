package util;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;

public class Menu 
{
	public static class Entry
	{
		public String label;
		public boolean enabled = true;
		
		public Entry(String lbl)
		{
			this.label = lbl;
		}
	}
	
	public ArrayList<Entry> entries = new ArrayList<Menu.Entry>();
	public Image bg=null;
	private Color fontColor=Color.pink, fontColorTrans = fontColor.addToCopy(new Color(0, 0, 0, -96)), selectedColor=Color.magenta, bgColor = null;
	public int cursorIndex=0;
	public float itemsHeight=16f, paddingTop=0;
	
	public Rectangle bounds;

	public Menu(Rectangle bounds)
	{
		this.bounds = bounds;
	}

	public void setFontColor(Color fontColor) {
		this.fontColor = fontColor;
		this.fontColorTrans = fontColor.addToCopy(new Color(0, 0, 0, -96));
	}

	public void setSelectedColor(Color selectedColor) {
		this.selectedColor = selectedColor;
	}

	public void setBgColor(Color bgColor) {
		this.bgColor = bgColor;
	}
	
	public void draw(StateBasedGame sbg, Graphics g)
	{
		if(bg != null)
			bg.draw();
		if(bgColor != null)
		{
			g.setColor(bgColor);
			g.fillRoundRect(bounds.getX(), bounds.getY(), bounds.getWidth(), bounds.getHeight(), (int)(bounds.getWidth()*bounds.getHeight())/5000);
		}
		
		float lineHeight = bounds.getHeight()/entries.size();
		if(lineHeight > itemsHeight) lineHeight = itemsHeight;
		
		for(int i = 0 ; i < entries.size() ; i++)
		{
			if(i == cursorIndex) g.setColor(fontColorTrans);
			else g.setColor(Color.transparent);
			g.fillRoundRect(bounds.getX(), bounds.getY() + i*lineHeight + paddingTop, bounds.getWidth(), lineHeight, 10);

			if(i == cursorIndex) g.setColor(selectedColor);
			else g.setColor(fontColor);
			g.drawString(entries.get(i).label, bounds.getX(), bounds.getY() + i*lineHeight + paddingTop);
		}
	}
}
