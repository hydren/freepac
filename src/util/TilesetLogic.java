package util;

import static states.PlayStageState.BLOCK_SIZE;

import entities.Block;
import entities.Block.Type;
import states.PlayStageState;
import util.drawing.FreePacWallDrawer;

public class TilesetLogic 
{
	private static final byte OTHER=0, WARP=1, WALL=2;
	private static Block get(PlayStageState state, int i, int j)
	{
		if(state.isSlotGridBoundsSafe(i, j))
			return state.grid[i][j];
		else return PlayStageState.OUTSIDE_MAP;
	}
	
	public static void parseAndDraw(PlayStageState state, Block block, FreePacWallDrawer drawer)
	{
		final int i = (int) (block.getY() / BLOCK_SIZE), j = (int) (block.getX() / BLOCK_SIZE);
		int u, v; //auxiliary variables
		
		//represents if the direction is free (available)
		final boolean 	nw,		no, 	ne, 
				 		we, /*center*/ 	ea, 
				 		sw, 	so, 	se;
		
		//AUX VAR. Represents if the direction was void-type (out of bounds, OUTSIDE_MAP-flagged or null-filled)
		final boolean xnw, xne, xsw, xse;
		//AUX VAR. Represents if the direction has a warp, wall or other
		final byte zno, zso, zwe, zea;
		
		u = i-1; v =  j ; no = get(state, u, v) != null; zno = ( no && get(state, u, v).type == Type.TELEPORT)? WARP : ( no && get(state, u, v).type == Type.WALL)? WALL: OTHER;
		u = i+1; v =  j ; so = get(state, u, v) != null; zso = ( so && get(state, u, v).type == Type.TELEPORT)? WARP : ( so && get(state, u, v).type == Type.WALL)? WALL: OTHER;
		u =  i ; v = j-1; we = get(state, u, v) != null; zwe = ( we && get(state, u, v).type == Type.TELEPORT)? WARP : ( we && get(state, u, v).type == Type.WALL)? WALL: OTHER;
		u =  i ; v = j+1; ea = get(state, u, v) != null; zea = ( ea && get(state, u, v).type == Type.TELEPORT)? WARP : ( ea && get(state, u, v).type == Type.WALL)? WALL: OTHER;

		u = i-1; v = j-1; nw = get(state, u, v) != null; xnw = (!nw || get(state, u, v)==PlayStageState.OUTSIDE_MAP);
		u = i-1; v = j+1; ne = get(state, u, v) != null; xne = (!ne || get(state, u, v)==PlayStageState.OUTSIDE_MAP);
		u = i+1; v = j-1; sw = get(state, u, v) != null; xsw = (!sw || get(state, u, v)==PlayStageState.OUTSIDE_MAP);
		u = i+1; v = j+1; se = get(state, u, v) != null; xse = (!se || get(state, u, v)==PlayStageState.OUTSIDE_MAP);
		
		float x = block.getX(), y = block.getY();
		
		if(state.rainbowColored)
		{
			drawer.wallColor = ColorExtra.getRandomColor();
			drawer.borderColor = ColorExtra.getContrastColor(drawer.wallColor);
		}
		
		if(no && so && ea && we) //case 0
		{
			if(!nw && ne && sw && se)  // special T case 6a
			{
				if(zno==WARP) drawer.drawBottomHalf(x, y); //warp horizontally adjacent
				else if(zwe==WARP) drawer.drawRightHalf(x, y); //warp vertically adjacent
				else drawer.drawExceptSecondQuadrant(x, y, !(xse && zso!=WALL && zea!=WALL)); // third argument decides to fill outward or not
			}
			else if(nw && !ne && sw && se)   // special T case 5a
			{
				if(zno==WARP) drawer.drawBottomHalf(x, y);
				else if(zea==WARP) drawer.drawLeftHalf(x, y);
				else drawer.drawExceptFirstQuadrant(x, y, !(xsw && zso!=WALL && zwe!=WALL));
			}
			else if(nw && ne && !sw && se)   // special T case 4a
			{
				if(zso==WARP) drawer.drawUpperHalf(x, y);
				else if(zwe==WARP) drawer.drawRightHalf(x, y);
				else drawer.drawExceptThirdQuadrant(x, y, !(xne && zno!=WALL && zea!=WALL));
			}
			else if(nw && ne && sw && !se)  // special T case 3a
			{
				if(zso==WARP) drawer.drawUpperHalf(x, y);
				else if(zea==WARP) drawer.drawLeftHalf(x, y);
				else drawer.drawExceptFourthQuadrant(x, y, !(xnw && zno!=WALL && zwe!=WALL));
			}
			else // normal case full
				drawer.drawFull(x, y);
		}
		
		if(!no && !so && we && ea) //case 1
		{
			if(block.adFence)
			{
				// XXX Kludges to check enemy house wall orientation (horizontal). Fails when not dealing with enemy house walls
				if(i == state.fenceExitSlot.getValue0()) // Case 1a (same height as fence exit)
					drawer.drawBottomHalf(x, y);
				else // Case 1b (should be enemy house bottom wall)
					drawer.drawUpperHalf(x, y); 
			}
			// FIXME how to deal with non-enemy house walls cases (horizontal)?
		}
		
		if(no && so && !we && !ea) //case 2
		{
			if(block.adFence)
			{
				// XXX Kludges to check enemy house wall orientation (vertical). Fails when not dealing with enemy house walls
				if(j < state.fenceExitSlot.getValue1()) // Case 2a (left of fence exit)
					drawer.drawRightHalf(x, y); 
				else // Case 2b (right of fence exit)
					drawer.drawLeftHalf(x, y);
			}
			// FIXME how to deal with non-enemy house walls cases (vertical)?
		}
		
		if(!no && so && ea && we) //case 1b (T check)
		{
			if(zea==WARP) drawer.drawThirdQuadrant(x, y);  //warp check
			else if(zwe==WARP) drawer.drawFourthQuadrant(x, y);  //warp check
			else drawer.drawBottomHalf(x, y);
		}
		
		if(no && !so && ea && we) //case 1a (T check)
		{
			if(zea==WARP) drawer.drawSecondQuadrant(x, y);  //warp check
			else if(zwe==WARP) drawer.drawFirstQuadrant(x, y);  //warp check
			drawer.drawUpperHalf(x, y);
		}
		
		if(no && so && !ea && we) //case 2a (T check)
		{
			if(zno==WARP) drawer.drawThirdQuadrant(x, y);  //warp check
			else if(zso==WARP) drawer.drawSecondQuadrant(x, y);  //warp check
			else drawer.drawLeftHalf(x, y);
		}
		
		if(no && so && ea && !we) //case 2b (T check)
		{
			if(zno==WARP) drawer.drawFourthQuadrant(x, y);  //warp check
			else if(zso==WARP) drawer.drawFirstQuadrant(x, y);  //warp check
			else drawer.drawRightHalf(x, y);
		}
		
		if(!no && so && !we && ea) //case 3
		{
			if(!se && !block.adFence)
				drawer.drawExceptFourthQuadrant(x, y, false); //case 3a
			else
				drawer.drawFourthQuadrant(x, y); //case 3b
		}
		
		if(!no && so && we && !ea) //case 4
		{
			if(!sw && !block.adFence)
				drawer.drawExceptThirdQuadrant(x, y, false); //case 4a
			else
				drawer.drawThirdQuadrant(x, y); //case 4b
		}
		
		if(no && !so && !we && ea) //case 5
		{
			if(!ne && !block.adFence)
				drawer.drawExceptFirstQuadrant(x, y, false); //case 5a
			else
				drawer.drawFirstQuadrant(x, y); //case 5b
		}
		
		if(no && !so && we && !ea) //case 6
		{
			if(!nw && !block.adFence)
				drawer.drawExceptSecondQuadrant(x, y, false); //case 6a
			else
				drawer.drawSecondQuadrant(x, y); //case 6b
		}
	}
}
