package util;

import static states.PlayStageState.BLOCK_SIZE;

import java.awt.FontFormatException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.javatuples.Pair;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.Font;
import org.newdawn.slick.Image;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.geom.Vector2f;

import entities.Actor;
import entities.Enemy;
import states.MenuState;
import states.PlayStageState;

public class Util 
{	
	public static final Vector2f ZERO_VECTOR = new Vector2f();
	
	public static void avoidDeveloperEmbarrassment()
	{
		PlayStageState.stageNumber=-1337;
		MenuState.noBG=true;
	}
	
	public static void printGrid(char[][] grid)
	{
		for(char[] arr : grid)
		{
			for(char i : arr)
			{
				System.out.print(i);
			}
			System.out.println();
		}
	}
	
	static void setDirAsLast(List<Direction> list, Direction direction)
	{
		list.remove(direction);
		list.add(direction);
	}
	
	public static SpriteSheet loadSheet(String ref, int tw, int th)
	{
		System.out.println("Loading sprite sheet "+ref);
		if(ref == null || ref.trim().length()==0) return null;
		try { return new SpriteSheet(ref, tw, th); } 
		catch (SlickException e) { e.printStackTrace(); }
		System.out.println("Failed loading "+ref);
		return null;
	}
	
	public static Image loadImage(String ref)
	{
		System.out.println("Loading image "+ref);
		if(ref == null || ref.trim().length()==0) return null;
		try { return new Image(ref); } 
		catch (SlickException e) { e.printStackTrace(); }
		System.out.println("Failed loading "+ref);
		return null;
	}
	
	public static Music loadMusic(String ref)
	{
		System.out.println("Loading music "+ref);
		if(ref == null || ref.trim().length()==0) return null;
		try { return new Music(ref, true); } 
		catch (SlickException e) { e.printStackTrace(); }
		System.out.println("Failed loading "+ref);
		return null;
	}
	
	public static Sound loadSound(String ref)
	{
		System.out.println("Loading sound "+ref);
		if(ref == null || ref.trim().length()==0) return null;
		try { return new Sound(ref); } 
		catch (SlickException e) { e.printStackTrace(); }
		System.out.println("Failed loading "+ref);
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static Font loadFont(String ref)
	{
		System.out.println("Loading font "+ref);
		if(ref == null || ref.trim().length()==0) return null;
		if(ref.endsWith(".ttf")) try
		{
			java.awt.Font awtFont;
			org.newdawn.slick.UnicodeFont uniFont;
			
			awtFont = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, org.newdawn.slick.util.ResourceLoader.getResourceAsStream(ref));
	        uniFont = new org.newdawn.slick.UnicodeFont(awtFont);
	        uniFont.addAsciiGlyphs();
	        uniFont.getEffects().add(new ColorEffect(java.awt.Color.white)); //You can change your color here, but you can also change it in the render{ ... }
	        uniFont.addAsciiGlyphs();
	        uniFont.loadGlyphs();
	        return uniFont;
		}
		catch(SlickException e) {e.printStackTrace();} catch(IOException e) {e.printStackTrace();} catch(FontFormatException e) {e.printStackTrace();}
		return null;
	}
	
	/** Derives a font with new style and size from the given font.
	 * <br>NOTE: It works ONLY WITH Slick2D UnicodeFont */
	@SuppressWarnings("unchecked")
	public static Font deriveFont(Font font, int awtFontStyle, float size) throws SlickException
	{
		java.awt.Font awtFont = ((UnicodeFont) font).getFont();
		awtFont = awtFont.deriveFont(awtFontStyle, size);
		UnicodeFont uniFont;
		uniFont = new org.newdawn.slick.UnicodeFont(awtFont);
        uniFont.addAsciiGlyphs();
        uniFont.getEffects().add(new ColorEffect(java.awt.Color.white)); //You can change your color here, but you can also change it in the render{ ... }
        uniFont.addAsciiGlyphs();
        uniFont.loadGlyphs();
        return uniFont;
	}
	
	public static float manhattanDistance(Pair<Integer, Integer> slot1, Pair<Integer, Integer> slot2)
	{
		return Math.abs(slot1.getValue0() - slot2.getValue0()) + Math.abs(slot1.getValue1() - slot2.getValue1());
	}
	
	public static float manhattanDistance(float[] p1, float[] p2)
	{
		if(p1.length != 2 || p2.length != 2) throw new RuntimeException("Invalid distance operation: one or both points have number of coordinates different than 2.");
		return Math.abs(p1[0] - p2[0]) + Math.abs(p1[1] - p2[1]);
	}
	
	public static float distance(float[] p1, float[] p2)
	{
//		if(p1.length != 2 || p2.length != 2) throw new RuntimeException("Invalid distance operation: one or both points have number of coordinates different than 2.")		
//		return (float) Math.sqrt((p1[0]-p2[0])*(p1[0]-p2[0]) + (p1[1]-p2[1])*(p1[1]-p2[1]));
		return new Vector2f(p1).distance(new Vector2f(p2));
	}
	
	/** Returns all available directions for this actor to move from this node's slot. <br><br> XXX Kind of code duplication from Actor.availableDirections() and Actor.isDirectionFree() */
	public static Direction[] slotAvailableDirections(Pair<Integer, Integer> slot, PlayStageState st, Actor actor)
	{
		final HashSet<Direction> dirs = new HashSet<Direction>();
		for(Direction d : Direction.values())
			if(st.isSlotFreeForActor(Util.getSlotAheadDir(slot, d), actor)) 
				dirs.add(d);
		
		return dirs.toArray(new Direction[0]);
	}
	
	public static List<Pair<Integer, Integer>> fastestPath(PlayStageState st, Actor actor, Enemy.Target target)
	{
		System.out.println("Starting A* pathfinding...");
		final Pair<Integer, Integer> targetSlot = Util.getSlotPosition(target.getCenterPoint()), startSlot = actor.getSlotPosition();
		HashSet<Pair<Integer, Integer>> openSet = new HashSet<Pair<Integer, Integer>>(), closedSet = new HashSet<Pair<Integer, Integer>>();
		HashMap<Pair<Integer, Integer>, Integer> cost = new HashMap<Pair<Integer, Integer>, Integer>(), distance = new HashMap<Pair<Integer, Integer>, Integer>();
		HashMap<Pair<Integer, Integer>, Pair<Integer, Integer>> cameFrom = new HashMap<Pair<Integer, Integer>, Pair<Integer, Integer>>();
		
		Pair<Integer, Integer> current = startSlot;
		openSet.add(current); // initially containing the start node
		cost.put(current, 0); // Cost from start along best known path.
		distance.put(current, (int) Util.manhattanDistance(current, targetSlot));
		
		while(!openSet.isEmpty()) // Continue until there is no more available square in the open list (which means there is no path)
		{
			//decide lowest cost node
			current = openSet.iterator().next();
			for(Pair<Integer, Integer> n : openSet)
				if(distance.get(n) < distance.get(current))
					current = n;  // Get the square with the lowest F score
			
			if(current.equals(targetSlot)) // PATH COMPLETE!
			{
				System.out.println("Path complete! Composing list...");
				final List<Pair<Integer, Integer>> total_path = new ArrayList<Pair<Integer,Integer>>();
				total_path.add(current);
				while(cameFrom.containsKey(current))
				{
					current = cameFrom.get(current);
					total_path.add(current);
				}
				Collections.reverse(total_path);
				System.out.println("Finished path composing. " + total_path.size() + " steps.");
				for(Pair<Integer, Integer> p : total_path) System.out.print(p);
				System.out.println();
				return total_path;
			}
			
			closedSet.add(current); // add the current square to the closed list & 
			openSet.remove(current); // remove it to the open list
			
			for(Direction dir : slotAvailableDirections(current, st, actor)) // Retrieve all its walkable adjacent squares
			{
				final Pair<Integer, Integer> neighborSlot = getSlotAheadDir(current, dir);

				if(closedSet.contains(neighborSlot)) // if this adjacent square is already in the closed list ignore it
					continue; // Go to the next adjacent square
				
				final int tentative_cost = cost.get(current) + 1; // length of this tentative path.
				
				if(!openSet.contains(neighborSlot)) // if its not in the open list
					openSet.add(neighborSlot); // Discover a new node
				
				else if(tentative_cost >= cost.get(neighborSlot))
					continue; // This is not a better path.
				
				cameFrom.put(neighborSlot, current);
				cost.put(neighborSlot, tentative_cost);
				distance.put(neighborSlot, tentative_cost + (int) Util.manhattanDistance(neighborSlot, targetSlot));
			}
		}
		System.out.println("No path found, fail");
		return null;
	}
	
	/** Returns the indexes (row, column) of the grid slot which the given point is located. 
	 *  <br><b> WARNING: </b> Don't use this with getLocation() when instead you should be using getCenter() */
	public static Pair<Integer, Integer> getSlotPosition(Vector2f pos)
	{
		final float block_size = (float) BLOCK_SIZE;
		return new Pair<Integer, Integer>((int) (pos.y/block_size), (int) (pos.x/block_size));
	}
	
	/** Returns the indexes (row, column) of the grid slot which the given point is located. */
	public static Pair<Integer, Integer> getSlotPosition(float[] centerCoords)
	{
		final float block_size = (float) BLOCK_SIZE;
		return new Pair<Integer, Integer>((int) (centerCoords[1]/block_size), (int) (centerCoords[0]/block_size));
	}
	
	/** Returns the position of the given slot's center point. */
	public static Vector2f getSlotCenterPoint(Pair<Integer, Integer> slot)
	{
		final float block_size = (float) BLOCK_SIZE;
		return new Vector2f(slot.getValue1()*BLOCK_SIZE + block_size*0.5f , slot.getValue0()*BLOCK_SIZE + block_size*0.5f);
	}
	
	/** Returns the slot corresponding to walking from the given slot one block in the given direction. */
	public static Pair<Integer, Integer> getSlotAheadDir(Pair<Integer, Integer> slot, Direction direction)
	{
		switch (direction) {
			case UP: return new Pair<Integer, Integer>(slot.getValue0()-1, slot.getValue1());
			case LEFT: return new Pair<Integer, Integer>(slot.getValue0(), slot.getValue1()-1);
			case RIGHT: return new Pair<Integer, Integer>(slot.getValue0(), slot.getValue1()+1);
			case DOWN: return new Pair<Integer, Integer>(slot.getValue0()+1, slot.getValue1());
			default: return null;
		}
	}
	
	/** Returns the slot corresponding to walking from the given slot one block in the given direction, n times */
	public static Pair<Integer, Integer> getSlotAheadDir(Pair<Integer, Integer> slot, Direction direction, int n)
	{
		Pair<Integer, Integer> resultSlot = slot;
		for(int i = 0; i < n; i++)
		{
			resultSlot = Util.getSlotAheadDir(resultSlot, direction);
		}
		return resultSlot;
	}
	
	public static Comparator<DisplayMode> DISPLAY_MODE_DIAGONAL_COMPARATOR = new Comparator<DisplayMode>() 
	{
		@Override
		public int compare(DisplayMode o1, DisplayMode o2) {
			double do1 = Math.sqrt(o1.getWidth()*o1.getWidth() + o1.getHeight()*o1.getHeight());
			double do2 = Math.sqrt(o2.getWidth()*o2.getWidth() + o2.getHeight()*o2.getHeight());
			if(do1 > do2) return 1;
			else if(do1 < do2) return -1;
			else return (o1.getFrequency() - o2.getFrequency() == 0? o1.getBitsPerPixel() - o2.getBitsPerPixel() : o1.getFrequency() - o2.getFrequency());
		}
	};
	
	/** Parses the given string and returns its double value. If the string is null or is not a valid double, the defaultValue is returned instead. */
	public static double parse(String raw, double defaultValue)
	{
		if(raw == null) return defaultValue;
		
		try
		{
			return Double.parseDouble(raw);
		}
		catch (NumberFormatException exp)
		{
			return defaultValue;
		}
	}
	
	/** Parses the given string as a comma-separated list of numbers. Up to n values is read and returned.
	 *  If less than n values can be parsed from the string, the remaining is filled with the default values given.
	 *  If n==0, null is returned. If the passed default values array is null, an array of n doubles of value 0 is assumed as default.
	 *  If only a single value can be read from the string, this function returns an array of size n filled with the single value read.
	 *  <br><br>
	 *  Note: if more than n values can be read from the string, up to n values are read and the rest is ignored.*/
	public static double[] parse(String rawValue, int n, double[] defaultValues)
	{
		if(n == 0) 
			return null;
		
		if(defaultValues == null) 
		{ 
			defaultValues = new double[n]; 
			Arrays.fill(defaultValues, 0); 
		}
		
		if(rawValue == null || rawValue.trim().length() == 0) 
			return defaultValues;
		
		String[] rawValues = rawValue.trim().split(",");
		double[] values = new double[n];
		if(rawValues.length == 1)
		{
			double Value = Double.parseDouble(rawValue);
			Arrays.fill(values, Value);
		}
		else
		{
			int i;
			for(i = 0; i < Math.min(rawValues.length, n); i++)
			{
				values[i] = Double.parseDouble(rawValues[i]);
			}
			if(i < n) for(; i < n; i++)
			{
				values[i] = defaultValues[i];
			}
		}
		
		return values;
	}
	
	public static int[] castIntAssignArray(double[] arrayD)
	{
		int[] array = new int[arrayD.length];
		for(int i = 0; i < arrayD.length; i++)
		{
			array[i] = (int) arrayD[i];
		}
		return array;
	}
	
	public static long[] castLongAssignArray(double[] arrayD)
	{
		long[] array = new long[arrayD.length];
		for(int i = 0; i < arrayD.length; i++)
		{
			array[i] = (long) arrayD[i];
		}
		return array;
	}
	
	public static float[] castFloatAssignArray(double[] arrayD)
	{
		float[] array = new float[arrayD.length];
		for(int i = 0; i < arrayD.length; i++)
		{
			array[i] = (float) arrayD[i];
		}
		return array;
	}
}
