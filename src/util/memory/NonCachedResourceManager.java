package util.memory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import util.Util;

public class NonCachedResourceManager extends ResourceManager
{
	public NonCachedResourceManager() throws FileNotFoundException, IOException 
	{
		resourcesFilenames = new HashMap<String, String>();
		
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream(new File("resources.properties")); prop.load(fis);
		for(Object obj : prop.keySet()) if(obj instanceof String) resourcesFilenames.put((String) obj, prop.getProperty((String) obj));
		fis.close();
		
		System.out.println("Entries found on resources.properties:\n");
		for(String str : resourcesFilenames.keySet()) System.out.println(str + "=" + resourcesFilenames.get(str));
	}
	
	/** Get the resource. It can be an Animation (prefix anim_), an image (prefix bg_, img_), a sound (prefix snd_) or a music (prefix mus_) */
	@Override
	public Object get(String ref)
	{
		Object resource = null;
		String refPath = resourcesFilenames.get(ref);
		if(refPath != null)
		{
			System.out.println("Loading registered resource " + ref +"...");
			if(ref.startsWith("anim_")) 
				resource = (assembleAnimation(ref, refPath));

			else if(ref.startsWith("img_") || ref.startsWith("bg_")) 
				resource = (Util.loadImage(refPath));

			else if(ref.startsWith("mus_")) 
				resource = (Util.loadMusic(refPath));
			
			else if(ref.startsWith("snd_")) 
				resource = (Util.loadSound(refPath));
			
			else if(ref.startsWith("fnt_"))
				resource = (Util.loadFont(refPath));
			
			else System.out.println("Type not recognized for resource "+ref);
		}
		else if(refPath == null && ref.endsWith("]")) // file is a height-indexed animation
		{
			String plainRef = ref.split("\\[")[ref.split("\\[").length-2]; ;
			System.out.println("Loading height-indexed animation " + plainRef+"...");
			refPath = resourcesFilenames.get(plainRef);
			String tmp = ref.split("\\[")[ref.split("\\[").length-1];
			tmp = tmp.split("\\]")[0];
			int index = Integer.parseInt(tmp);
			resource = assembleAnimationFromSheet(plainRef, refPath, index);
			System.out.println("Animations assembled. Loaded index " + index);
		}
		else // file is not registered on resources.properties, try to load directly
		{
			System.out.println("Loading unregistered resource " + ref+"...");
			if(ref.endsWith(".anim.jpg") || ref.endsWith(".anim.jpeg") || ref.endsWith(".anim.png")) // pre-sufix .anim assumes as animation (not image)
				resource = (assembleAnimation(ref));
			
			else if(ref.endsWith(".jpg") || ref.endsWith(".jpeg") || ref.endsWith(".png")) // assumes as image (not animation)
				resource = (Util.loadImage(ref));
			
			else if(ref.endsWith(".oga")) // assumes oga as sound (not music)
				resource = (Util.loadSound(ref));

			else if(ref.endsWith(".ogg")) // assumes ogg as music (not sound)
				resource = (Util.loadMusic(ref));
			
			else if(ref.endsWith(".ttf")) // assumes ttf as font
				resource = (Util.loadFont(ref));
		}
		return resource;
	}
	
	@Override
	public void disposeResource(Object resource) throws SlickException
	{
		if(resource instanceof Image) ((Image)resource).destroy();
		if(resource instanceof Animation) for(int i = 0; i < ((Animation)resource).getFrameCount(); i++) ((Animation)resource).getImage(i).destroy();
	}
	
}