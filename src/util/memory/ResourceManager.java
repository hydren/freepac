package util.memory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import util.Util;

public abstract class ResourceManager 
{
	/** Contains all resources filenames. */
	protected Map<String, String> resourcesFilenames = new HashMap<String, String>();
	
	public abstract Object get(String ref);
	
	public abstract void disposeResource(Object resource) throws SlickException;
	
	public String getProperty(String key)
	{
		return resourcesFilenames.get(key);
	}
	
	/** Creates a animation from definitions from resources.properties */
	protected Animation assembleAnimation(String ref, String refPath)
	{
		int tw=32, th=32, at=100;
		if(resourcesFilenames.get(ref+".size") != null) 
		{
			String[] tokens = resourcesFilenames.get(ref+".size").split(",");
			if(tokens.length == 2)
			{
				tw = Integer.parseInt(tokens[0]);
				th = Integer.parseInt(tokens[1]);
			}
			else tw = th = Integer.parseInt(resourcesFilenames.get(ref+".size"));
		}
		if(resourcesFilenames.get(ref+".time") != null) at = Integer.parseInt(resourcesFilenames.get(ref+".time"));
		return new Animation(Util.loadSheet(refPath, tw, th), at);
	}
	
	/** Creates a animation from definitions from the filename. The file should be named as 'somename'.'width'.'height'.'time'.'extension' */
	protected Animation assembleAnimation(String path)
	{
		int tw=32, th=32, at=100;

		String[] tokens = path.split(".");
		parsing: for(int i = tokens.length-2, offset=0; i >= 0; i--, offset++) // parses animation dimensions and time from values between dots
		{
			try { switch (offset) 
			{
				case 0: at = Integer.parseInt(tokens[i]); break;
				case 1: th = Integer.parseInt(tokens[i]); break;
				case 2: tw = Integer.parseInt(tokens[i]); break;
				default: break parsing;
			}}
			catch(NumberFormatException e) { System.out.println("Could not parse token \""+tokens[i]+"\" as Integer, ignoring"); }
		}
		return new Animation(Util.loadSheet(path, tw, th), at);
	}
	
	/** Creates animations from definitions from resources.properties. It will load all possibles rows (with height as specified in properties) as distinct animations.
	 * It will try to load all images horizontally. */
	protected Animation[] assembleAnimations(String ref, String refPath)
	{
		int tw=32, th=32, at=100;
		if(resourcesFilenames.get(ref+".size") != null) 
		{
			String[] tokens = resourcesFilenames.get(ref+".size").split(",");
			if(tokens.length == 2)
			{
				tw = Integer.parseInt(tokens[0]);
				th = Integer.parseInt(tokens[1]);
			}
			else tw = th = Integer.parseInt(resourcesFilenames.get(ref+".size"));
		}
		if(resourcesFilenames.get(ref+".time") != null) at = Integer.parseInt(resourcesFilenames.get(ref+".time"));
		SpriteSheet sheet = Util.loadSheet(refPath, tw, th);
		Animation[] anims = new Animation[sheet.getHeight()/th];
		
		for(int i = 0; i < anims.length; i++)
			anims[i] = new Animation(sheet, 0, i, (sheet.getWidth()/tw)-1, i, true, at, true);
		
		return anims;
	}
	
	/** Creates an animation from definitions from resources.properties. It will load all possibles rows (with height as specified in properties) as distinct animations.
	 * It will try to load all images horizontally. */
	protected Animation assembleAnimationFromSheet(String ref, String refPath, int index)
	{
		int tw=32, th=32, at=100;
		if(resourcesFilenames.get(ref+".size") != null) 
		{
			String[] tokens = resourcesFilenames.get(ref+".size").split(",");
			if(tokens.length == 2)
			{
				tw = Integer.parseInt(tokens[0]);
				th = Integer.parseInt(tokens[1]);
			}
			else tw = th = Integer.parseInt(resourcesFilenames.get(ref+".size"));
		}
		if(resourcesFilenames.get(ref+".time") != null) at = Integer.parseInt(resourcesFilenames.get(ref+".time"));
		SpriteSheet sheet = Util.loadSheet(refPath, tw, th);
		
		for(int i = 0; i < sheet.getHeight()/th; i++) if(i == index)
			return new Animation(sheet, 0, i, (sheet.getWidth()/tw)-1, i, true, at, true);
		
		return null;
	}
	
	/** Creates an animation from given definitions. It will load all possibles rows (with height as specified in properties) as distinct animations.
	 * It will try to load all images horizontally. */
	public static Animation[] assembleAnimationsFromSheet(String refPath, int width, int height, int interval)
	{
		int tw=width, th=height, at=interval;
		SpriteSheet sheet = Util.loadSheet(refPath, tw, th);
		
		ArrayList<Animation> anims = new ArrayList<Animation>();
		for(int i = 0; i < sheet.getHeight()/th; i++)
			anims.add(new Animation(sheet, 0, i, (sheet.getWidth()/tw)-1, i, true, at, true));
		
		return anims.toArray(new Animation[0]);
	}
}
