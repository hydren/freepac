package entities;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Vector2f;

import states.PlayStageState;
import util.memory.ResourceManager;

public class Item 
{
	public static Color PELLET_COLOR=Color.yellow, ENERGIZER_COLOR=Color.yellow, BONUS_PELLET_COLOR=Color.red;
	public static boolean WAVE_PELLETS = true, WAVE_BONUS_PELLETS = false;
	
	public enum Type
	{
		PELLET(1000), 
		ENERGIZER(250), 
		BONUS_PELLET(-1);
		
		/** The total time of this item animation */
		public long animTime;
		
		Type(long time) { animTime = time; };
		
		public Color getColor()
		{
			switch (this) {
				case PELLET: return PELLET_COLOR;
				case ENERGIZER: return ENERGIZER_COLOR;
				case BONUS_PELLET: return BONUS_PELLET_COLOR;
				default: return Color.white;
			}
		}
		
		public String getStandardIcon(int level)
		{
			switch (this) {
				case PELLET: return "img_pellet";
				case ENERGIZER: return "img_energizer";
				case BONUS_PELLET: return "img_goodie"+level;
				default: return null; //TODO should return a ? when the type is custom
			}
		}
	}

	public Circle body;
	
	/** The type of item. */
	public Type type;

	/** The icon to render this item */
	public Image icon;
	
	public int itemLevel;
	
	/** Creates a pellet centered at given point and same size as PlayStageState.BLOCK_SIZE/8  */
	public Item(float x, float y)
	{
		body = new Circle(x, y, PlayStageState.BLOCK_SIZE/8);
		type = Type.PELLET;
		icon = null;
	}
	
	/** Creates a item of the given type, centered at given point with the given radius.  */
	public Item(float x, float y, float radius, Type t) {
		body = new Circle(x, y, radius);
		type = t;
		icon = null;
	}
	
	/** Creates a item of the given type, centered at given point with the given radius.
	 *  This item will use the standard icon for this type (provided by the ResourceManager) instead of raw graphics. */
	public Item(float x, float y, float radius, Type t, ResourceManager manager)
	{
		this(x, y, radius, t);
		icon = (Image) manager.get(t.getStandardIcon(0));
	}
	
	/** Creates a item of the given type, centered at given point with the given radius.
	 *  This item will use the standard icon for this type (provided by the ResourceManager) instead of raw graphics. 
	 *  The level parameter is used to specify a different icon for the given type (i.e. a bonus pellet) according to the level (if supported at all).*/
	public Item(float x, float y, float radius, Type t, ResourceManager manager, int level)
	{
		this(x, y, radius, t);
		icon = (Image) manager.get(t.getStandardIcon(level));
		this.itemLevel = level;
	}
	
	/** Creates a item of the given type, centered at given point with the given radius.
	 *  This item will use the icon registered as 'imgRef' within the given ResourceManager. */
	public Item(float x, float y, float radius, Type t, ResourceManager manager, String imgRef)
	{
		this(x, y, radius, t);
		icon = (Image) manager.get(imgRef);
	}
	
	/** Creates a bonus pellet centered at given point with the given radius.
	 *  The icon is determined by the given level (and the given ResourceManager).
	 * */
	public Item(Vector2f center, float radius, int goodieLevel, ResourceManager manager) {
		this(center.x, center.y, radius, Type.BONUS_PELLET, manager, goodieLevel);
		this.itemLevel = goodieLevel;
	}
	
	/** Draws this item according to its type and the given map offset. */
	public void draw(Graphics g)
	{	
		Vector2f coolOffset = null;
		
		if( (type != Type.BONUS_PELLET && WAVE_PELLETS==true) 
		||  (type == Type.BONUS_PELLET && WAVE_BONUS_PELLETS==true) ) // optional offset effect
		{
			coolOffset = PlayStageState.computeOffsetSpecialEffect(body);
			body.setLocation(body.getLocation().add(coolOffset));
		}
		
		if(icon == null) // icon not specified, use native drawing figures
		{
			if(type == Item.Type.ENERGIZER) // item is a energizer, adjust the color with time to give a glow effect
			{
				long pwrUpAnimTime = System.currentTimeMillis() % type.animTime;
				if(pwrUpAnimTime < type.animTime/4) g.setColor(type.getColor());
				else if(pwrUpAnimTime < type.animTime/2) g.setColor(type.getColor().brighter());
				else if(pwrUpAnimTime < 3*type.animTime/4) g.setColor(type.getColor());
				else g.setColor(type.getColor().darker());
			}
			else g.setColor(type.getColor());
			g.fill(body);
			g.setColor(Color.black);
			g.draw(body);
		}
		else // icon specified, draw icon
		{
			icon.draw(body.getCenterX() - icon.getWidth()/2 + (coolOffset!=null?coolOffset.x:0), body.getCenterY() - icon.getHeight()/2 + (coolOffset!=null?coolOffset.y:0));
		}
		
		// undo optional offset effect (if it was applied)
		if(coolOffset != null) body.setLocation(body.getLocation().sub(coolOffset)); 
	}
}
