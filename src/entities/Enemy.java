package entities;

import static states.PlayStageState.BLOCK_SIZE;

import java.util.HashMap;

import org.javatuples.Pair;
import org.newdawn.slick.Animation;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

import states.PlayStageState;
import util.Direction;
import util.Timer;
import util.Util;
import util.memory.ResourceManager;

public class Enemy extends Actor
{
	public static final float ENEMY_SLIP_THRESHOLD = ((float) BLOCK_SIZE)/8f;
	
	/** Type of enemies. Each one have a behavior and a sprite. */
	public enum EnemyType
	{
		//default enemies
		Dummy(EnemyBehavior.RANDOM), // usually the slime
		Default_1(EnemyBehavior.CHASE), // usually the spider
		Default_2(EnemyBehavior.CHASE_AHEAD_2), // usually the skeleton
		Default_3(EnemyBehavior.CHASE_PROJECTION_FIRST_ENEMY_ON_PLAYER), // usually the scirita
		Default_4(EnemyBehavior.POKEY), // usually the sorcerer

		//classic behavior
		Classic_1(EnemyBehavior.CLASSIC_SHADOW), 
		Classic_2(EnemyBehavior.CLASSIC_SPEEDY), 
		Classic_3(EnemyBehavior.CLASSIC_BASHFUL), 
		Classic_4(EnemyBehavior.CLASSIC_POKEY);
		
		public EnemyBehavior behavior;
		EnemyType(EnemyBehavior mb) {this.behavior = mb;}
	}
	
	public static class CustomEnemySpecification 
	{
		public EnemyBehavior behavior;
		public String name, sprite_ref;
		public int sprite_tw, sprite_th, sprite_at;
		public boolean isFaster, isInvulnerable, isVariableSpeed;
	}
	
	/** A target for a enemy. It may be either a location or a body (Slicks2D's Shape). */
	public static class Target
	{
		private Vector2f staticLocation;
		private Shape dynamicTarget;
		
		public Target(Vector2f staticLocation)
		{
			this.staticLocation = staticLocation;
			this.dynamicTarget = null;
		}
		
		public Target(Shape dynamicTarget)
		{
			this.dynamicTarget = dynamicTarget;
			this.staticLocation = null;
		}
		
		public Target(Pair<Integer, Integer> slot) 
		{
			this.staticLocation = Util.getSlotCenterPoint(slot);
			this.dynamicTarget = null;
		}

		public Vector2f getCenterPoint()
		{
			if(dynamicTarget != null)
				return new Vector2f(dynamicTarget.getCenter());
			
			else return staticLocation;
		}
	}
	
	/** The type of enemy - used to specify sprite and behavior. */
	public EnemyType enemyType;
	
	/** The enemy's current target. */
	Target target;
	
	/** The last intersection that this enemy had taken a decision of direction. */
	Pair<Integer, Integer> lastIntersection = null;
	
	public Animation scaredSprite1, scaredSprite2;
	
	public HashMap<Direction, Animation> killedSprites;
	
	/** Flag to indicate that this enemy is in frightened mode */
	public boolean scared = false;
	
	/** Flag to indicate that this enemy is in scatter mode */
	public boolean scatter = true;
	
	/** Flag to indicate that this enemy has been killed. */
	public boolean eaten = false;
	
	/** Flag to indicate that the enemy should stay in the enemy house. */
	public boolean houseStaying = true;
	
	/** Indicates that this enemy passed the fence, thus cannot pass it again until someone dies */
	public boolean passedFence = false;
	
	public boolean isFaster = false, isVariableSpeed = false;
	
	public boolean noClip = false, invulnerable = false;
	
	// XXX kludge to use A star algorithm path caching
	public Object aux1 = null;
	
	// XXX kludge to elfin enemy
	public boolean aux2 = false;
	
	// XXX workaround to support custom enemies
	public CustomEnemySpecification customSpecification=null;
	
	public Enemy(PlayStageState state, EnemyType type, Circle body, Vector2f spawnPoint) throws Exception
	{
		super(state);
		this.body = body; 
		dir = Direction.LEFT;
		spawnLocation = spawnPoint;
		enemyType = type;
		if(state.player != null) target = new Target(state.player.body);
		
		spriteType = SpriteType.ASYMMETRIC;
		sprites = new HashMap<Direction, Animation>();
		sprites.put(Direction.RIGHT, (Animation) state.game.resources.get("anim_"+enemyType.name()+"[0]"));
		sprites.put(Direction.DOWN, (Animation) state.game.resources.get("anim_"+enemyType.name()+"[1]"));
		sprites.put(Direction.LEFT, (Animation) state.game.resources.get("anim_"+enemyType.name()+"[2]"));
		sprites.put(Direction.UP, (Animation) state.game.resources.get("anim_"+enemyType.name()+"[3]"));
		
		killedSprites = new HashMap<Direction, Animation>();
		killedSprites.put(Direction.RIGHT, (Animation) state.game.resources.get("anim_Dead[0]"));
		killedSprites.put(Direction.DOWN, (Animation) state.game.resources.get("anim_Dead[1]"));
		killedSprites.put(Direction.LEFT, (Animation) state.game.resources.get("anim_Dead[2]"));
		killedSprites.put(Direction.UP, (Animation) state.game.resources.get("anim_Dead[3]"));
		
		scaredSprite1 = (Animation) state.game.resources.get("anim_Scared");
		scaredSprite2 = (Animation) state.game.resources.get("anim_Scared2");
	}
	
	public Enemy(PlayStageState state, CustomEnemySpecification customSpec, Circle body, Vector2f spawnPoint) throws Exception
	{
		super(state);
		this.body = body; 
		dir = Direction.LEFT;
		spawnLocation = spawnPoint;
		enemyType = null;
		customSpecification = customSpec;
		if(state.player != null) target = new Target(state.player.body);
		
		isFaster = customSpecification.isFaster; isVariableSpeed = customSpecification.isVariableSpeed; invulnerable = customSpecification.isInvulnerable;
		Animation[] anims = ResourceManager.assembleAnimationsFromSheet(customSpecification.sprite_ref, customSpecification.sprite_tw, customSpecification.sprite_th, customSpecification.sprite_at);
 		
		spriteType = SpriteType.ASYMMETRIC;
		sprites = new HashMap<Direction, Animation>();
		sprites.put(Direction.RIGHT, anims[0]);
		sprites.put(Direction.DOWN, anims[1]);
		sprites.put(Direction.LEFT, anims[2]);
		sprites.put(Direction.UP, anims[3]);
		
		killedSprites = new HashMap<Direction, Animation>();
		killedSprites.put(Direction.RIGHT, (Animation) state.game.resources.get("anim_Dead[0]"));
		killedSprites.put(Direction.DOWN, (Animation) state.game.resources.get("anim_Dead[1]"));
		killedSprites.put(Direction.LEFT, (Animation) state.game.resources.get("anim_Dead[2]"));
		killedSprites.put(Direction.UP, (Animation) state.game.resources.get("anim_Dead[3]"));
		
		scaredSprite1 = (Animation) state.game.resources.get("anim_Scared");
		scaredSprite2 = (Animation) state.game.resources.get("anim_Scared2");
	}
	
	public Vector2f getTargetLocation()
	{
		return target.getCenterPoint();
	}
	
	public void setTarget(Actor a)
	{
		target = new Target(a.body);
	}
	
	public void setTarget(Shape s)
	{
		target = new Target(s);
	}
	
	public void setTarget(Vector2f v)
	{
		target = new Target(v);
	}
	
	/** Computes the Euclidean distance between this actor to its target when going from the given direction */
	float distanceToTargetFrom(Direction fromDir, Target target)
	{
		return target.getCenterPoint().distance( Util.getSlotCenterPoint(this.getSlotPositionAt(fromDir)));
	}
	
	/** Computes the Manhattan distance between this actor to its target when going from the given direction */
	float distanceToTargetFrom2(Direction fromDir, Target target)
	{
		return Util.manhattanDistance(Util.getSlotPosition(target.getCenterPoint()), this.getSlotPositionAt(fromDir));
	}
	
	protected void move_actor(float delta)
	{
		super.move(delta);
	}
	
	@Override
	public void move(float delta)
	{
		updateSpeed();
		if(customSpecification != null && customSpecification.behavior != null) customSpecification.behavior.action(this, delta);
		else enemyType.behavior.action(this, delta);
	}
	
	private void updateSpeed()
	{
		if(eaten) //speed up when in eyes-only state
			speed = Actor.SPEED_FACTOR * 1.5f;
		else if(state.slowZones[getSlotPosition().getValue0()][getSlotPosition().getValue1()] == true) //slow down when in a slow zone
			speed = Actor.SPEED_FACTOR * (float) Math.min(0.5, 0.4 + 0.005 * (float) (PlayStageState.stageNumber-1));
		else if(scared) //slow down when frightened
			speed = Actor.SPEED_FACTOR * (float) Math.min(0.6, 0.5 + 0.005 * (float) (PlayStageState.stageNumber-1));
		else if(!houseStaying && !passedFence) //slow down when leaving enemy house
			speed = Actor.SPEED_FACTOR * 0.4f;
		else //normal speed
		{
			speed = Actor.SPEED_FACTOR * (float) Math.min(0.95, 0.75 + 0.01 * (float) (PlayStageState.stageNumber-1));
			if(isVariableSpeed)
			{
				switch(Timer.globalCycle(125, 12))
				{
					default:
					case 0: break;
					case 1: speed *= 1.1; break;
					case 2: speed *= 1.2; break;
					case 3: speed *= 1.3; break;
					case 4: speed *= 1.2; break;
					case 5: speed *= 1.1; break;
					case 6: break;
					case 7: speed *= 0.9; break;
					case 8: speed *= 0.8; break;
					case 9: speed *= 0.7; break;
					case 10: speed *= 0.8; break;
					case 11: speed *= 0.9; break;				
				}
			}
			if(isFaster) speed *=1.5;
		}
	}
	
	@Override
	public void draw()
	{
		final float x = body.getCenterX(), 
					y = body.getCenterY();
		if(eaten)
		{
			final Animation currentAnim = killedSprites.get(dir);
			currentAnim.draw(x - currentAnim.getWidth()/2, y - currentAnim.getHeight()/2);
		}
		
		else if(scared)
		{
			if(state.powerModeTimer.nearTimeCompletion() && System.currentTimeMillis()%250 > 125 //when power mode near ending, blink each 125ms
			&& !state.paused && !state.playerDeathEventMode && !state.enemyDeathEventMode) //except when in one of this modes
				scaredSprite2.draw(x - scaredSprite2.getWidth()/2, y - scaredSprite2.getHeight()/2);
			else
				scaredSprite1.draw(x - scaredSprite1.getWidth()/2, y - scaredSprite1.getHeight()/2);
		}
		else
		{
			final Animation currentAnim = sprites.get(dir);			
			currentAnim.draw(x - currentAnim.getWidth()/2, y - currentAnim.getHeight()/2);
		}
	}
}
