package entities;

import static states.PlayStageState.BLOCK_SIZE;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.RoundedRectangle;

import states.PlayStageState;
import util.ColorExtra;
import util.TilesetLogic;
import util.drawing.CustomGraphics;
import util.drawing.FreePacWallDrawer;

public class Block extends RoundedRectangle {
	
	private static final long serialVersionUID = -1142772518229137943L;
	
	/** If this block is a warp, this is the twin of this warp. Otherwise it is null. */
	public Block twin=null;
	
	/** If this block is a warp, this indicates it this warp is vertical. Otherwise it is null. */
	public Boolean isVerticalWarp = null;
	
	public Color customColor;
	
	public static float wallLineThickness = 4.0f;
	public static FreePacWallDrawer wallDrawer = null;

	public enum Type
	{
		WALL(Color.blue),
		TELEPORT(Color.transparent),
		FENCE(Color.gray);
		
		public Color color;
		Type(Color c) { color = c; };
	}
	
	final public Type type;
	public boolean marked = false;
	public boolean adFence = false;

	public Block(float x, float y, float width, float height, Type t) {
		super(x, y, width, height, 5f);
		type = t;
		if(type == Type.TELEPORT)
			isVerticalWarp = false;
	}
	
	public void draw(PlayStageState state, CustomGraphics g)
	{
		switch (type) 
		{
			case WALL:
				drawTiled(state, g);
			break;
			
			case FENCE:
				drawFence(state, g);
				break;

			case TELEPORT:
				drawTeleport(state, g);
				break;
			
			default:  // block-like
				drawBlocky(state, g);
			break;
		}
	}
	
	/** Draws a block figure. */
	private void drawBlocky(PlayStageState state, Graphics g)
	{
		g.setColor(customColor==null? type.color : customColor);
		g.fill(this);
		g.setColor(Color.black);
		g.setLineWidth(1.0f);
		g.draw(this);
	}
	
	/** Draws a wall using auto-tiling */
	private void drawTiled(PlayStageState state, CustomGraphics g)
	{
		if(wallDrawer == null) wallDrawer = state.getWallDrawer(g);
		wallDrawer.setGraphics(g);
		TilesetLogic.parseAndDraw(state, this, wallDrawer);
	}
	
	/** Draws a fence. */
	private void drawFence(PlayStageState state, Graphics g)
	{
		g.setColor(customColor==null? type.color : customColor);
		g.fillRect(getX(), getY()+BLOCK_SIZE*0.6f, BLOCK_SIZE, BLOCK_SIZE/4f);
	}
	
	private void drawTeleport(PlayStageState state, Graphics g)
	{
		if(!isVerticalWarp)
		{
			final float minorOffset = (getX() > state.game.getContainer().getWidth()/2f?BLOCK_SIZE*0.25f:-BLOCK_SIZE*0.25f); //to align with wall
			Color c = new Color(ColorExtra.getRandomColor()); c.a = .5f; g.setColor(c);
			g.fillOval(getX()+BLOCK_SIZE * 0.25f + minorOffset, getY()+BLOCK_SIZE * 0.05f, BLOCK_SIZE * 0.5f, BLOCK_SIZE * 0.9f);
			c = new Color(ColorExtra.getRandomColor()); c.a = .5f; g.setColor(c);
			g.drawOval(getX()+BLOCK_SIZE * 0.25f + minorOffset, getY()+BLOCK_SIZE * 0.05f, BLOCK_SIZE * 0.5f, BLOCK_SIZE * 0.9f);
		}
		else
		{
			final float minorOffset = (getY() > state.game.getContainer().getHeight()/2f?BLOCK_SIZE*0.25f:-BLOCK_SIZE*0.25f); //to align with wall
			Color c = new Color(ColorExtra.getRandomColor()); c.a = .5f; g.setColor(c);
			g.fillOval(getX()+BLOCK_SIZE * 0.05f, getY()+BLOCK_SIZE * 0.25f + minorOffset, BLOCK_SIZE * 0.9f, BLOCK_SIZE * 0.5f);
			c = new Color(ColorExtra.getRandomColor()); c.a = .5f; g.setColor(c);
			g.drawOval(getX()+BLOCK_SIZE * 0.05f, getY()+BLOCK_SIZE * 0.25f + minorOffset, BLOCK_SIZE * 0.9f, BLOCK_SIZE * 0.5f);
		}
	}
}
