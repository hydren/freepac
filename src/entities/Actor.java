package entities;

import static states.PlayStageState.BLOCK_SIZE;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import org.javatuples.Pair;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Vector2f;

import states.PlayStageState;
import util.Direction;
import util.Util;

/** A generic actor. Could be either a Player or a Enemy. */
public abstract class Actor
{
	/** Defines the threshold by which is allowed for players to change direction when misaligned with the direction. */
	public static float SLIP_THRESHOLD = ((float)BLOCK_SIZE)/4f;
	
	/** Actors' speed factor. Changing this changes overall actors' walking speed. */
	public static float SPEED_FACTOR = 1f/6f;
	
	protected enum SpriteType {
		SYMMETRIC, SEMISYMMETRIC, ASYMMETRIC;
		public static SpriteType parseType(String str) {
			if(str == null || str.trim().length() == 0) return null;
			for(SpriteType t : SpriteType.values())
				if(t.name().equalsIgnoreCase(str.trim()))
					return t;
			return null;
		}
	}
	protected SpriteType spriteType=SpriteType.SYMMETRIC;

	//Parameters
	/** The actor's shape. Not related to sprite shape. */
	public Circle body;
	/** The actor's sprite animation */
	public HashMap<Direction, Animation> sprites;
	/** The actor's sprite animation */
	public Animation dyingSprite;
	/** The location to spawn when prompted to respawn */
	public Vector2f spawnLocation;
	/** The actor's base speed */
	public float speed;	
	
	//Aux. variables
	static Random random = new Random();
	/** The actor's current direction. */
	public Direction dir;
	
	//DEBUG
	public Color debugColor = Color.white;
	
	/** Internal reference to stage state */
	protected PlayStageState state;

	//==============================================================================================================
	
	/** Default settings for Actor */
	protected Actor(PlayStageState st)
	{
		state = st;
		speed = SPEED_FACTOR;
	}
	
	public abstract void draw();
	
	/** Returns the indexes (row, column) of the grid slot which this actor's body's center point is located. */
	public Pair<Integer, Integer> getSlotPosition()
	{
		return Util.getSlotPosition(body.getCenter());
	}
	
	/** Returns the indexes (row, column) of the grid slot which this actor's body's center point would be located if moving one block in the given direction. */
	public Pair<Integer, Integer> getSlotPositionAt(Direction d)
	{
		return Util.getSlotAheadDir(getSlotPosition(), d);
	}
	
	/** Returns the position of this actor's <b>slot</b>'s center position. <b>Note:</b> the actual center point of this actor is <i>body.getCenter()</i> */
	public Vector2f getSlotPositionCenterPoint()
	{
		return Util.getSlotCenterPoint(this.getSlotPosition());
	}
	
	/** Returns whether the specified direction is free for this actor. 
	 *  This method returns true if this actor's slot's neighbor slot at the specified direction is free. 
	 *  <br> See {@link #states.PlayStageState.isSlotFreeForActor()} */
	public boolean isDirectionFree(Direction d)
	{
		return state.isSlotFreeForActor(getSlotPositionAt(d), this);
	}
	
	/** Same as <b>!state.isSlotFreeForActor(getSlotPosition(), this)<b> */
	boolean isStuck()
	{
		return !state.isSlotFreeForActor(getSlotPosition(), this);
	}
	
	/** Returns true if the actor is within the range allowed to slip-in when trying to corner. */
	protected boolean isWithinSlipThreshold(float threshold)
	{
		Vector2f slotCenter = getSlotPositionCenterPoint();
		Vector2f centerPosition = new Vector2f(body.getCenter()[0], body.getCenter()[1]);
		return slotCenter.distance(centerPosition) < threshold;
	}
	
	/** Returns all available directions for this actor to move. */
	public Direction[] availableDirections()
	{
		final HashSet<Direction> dirs = new HashSet<Direction>();
		for(Direction d : Direction.values())
			if(isDirectionFree(d)) 
				dirs.add(d);
		
		return dirs.toArray(new Direction[0]);
	}
	
	/** Puts this actor centered at its slot position. */
	public void snapToGrid()
	{
		final Vector2f centerSlotPosition = getSlotPositionCenterPoint();
		this.body.setCenterX(centerSlotPosition.x); 
		this.body.setCenterY(centerSlotPosition.y);
	}
	
	public void reverseSafely()
	{
		if(dir == null) return;
		if(isDirectionFree(dir.opposite()))
			dir = dir.opposite();
		else
		{
			final Direction[] availableDirections = availableDirections();
			if(availableDirections.length > 0) dir = availableDirections[random.nextInt(availableDirections.length)];
		}
	}
	
	boolean isBeforeSlotCenterPoint()
	{
		switch (dir) 
		{
			case UP:
				if(body.getCenterY() > this.getSlotPositionCenterPoint().y)
					return true;
				else return false;
				
			case DOWN:
				if(body.getCenterY() < this.getSlotPositionCenterPoint().y)
					return true;
				else return false;
				
			case LEFT:
				if(body.getCenterX() > this.getSlotPositionCenterPoint().x)
					return true;
				else return false;
				
			case RIGHT:
				if(body.getCenterX() < this.getSlotPositionCenterPoint().x)
					return true;
				else return false;
			
			default: return true;
		}
	}
	
	//== Movement/Behavior =====================================================================================================================
	
	/** Default actor movement. Walks and stops at walls. */
	public void move(float delta)
	{
		if(isDirectionFree(dir))
		{
			walk(delta);
		}
		else if(isBeforeSlotCenterPoint())
		{
			walk(delta);
		}
		else
		{
			snapToGrid();
		}
	}
	
	protected void walk(float delta)
	{
		switch(dir)
		{
			case DOWN: 	body.setY(body.getY()+delta*speed); break; 
			case LEFT: 	body.setX(body.getX()-delta*speed); break;
			case RIGHT:	body.setX(body.getX()+delta*speed); break;
			case UP:	body.setY(body.getY()-delta*speed); break;
		}
	}

}
