package entities;

import java.util.HashMap;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Vector2f;

import states.PlayStageState;
import util.Direction;

public class PlayableCharacter extends Actor
{
	public PlayableCharacter(PlayStageState stg, Circle body, Vector2f spawnPoint, String spriteRef) {
		super(stg);
		this.spawnLocation = spawnPoint;
		this.body = body;
		dir = Direction.LEFT;
		
		dyingSprite = (Animation) stg.game.resources.get("anim_player_dying");
		spriteType = SpriteType.parseType(stg.game.resources.getProperty(spriteRef+".type")); if(spriteType == null) spriteType = SpriteType.SYMMETRIC;
		sprites = new HashMap<Direction, Animation>();
		if(spriteType == SpriteType.SYMMETRIC || spriteType == SpriteType.SEMISYMMETRIC)
		{
			sprites.put(null, (Animation) stg.game.resources.get(spriteRef));
			sprites.get(null).setPingPong(true);
		}
		else if(spriteType == SpriteType.ASYMMETRIC)
		{
			sprites.put(Direction.RIGHT, (Animation) stg.game.resources.get(spriteRef+"[0]"));
			sprites.put(Direction.DOWN, (Animation) stg.game.resources.get(spriteRef+"[1]"));
			sprites.put(Direction.LEFT, (Animation) stg.game.resources.get(spriteRef+"[2]"));
			sprites.put(Direction.UP, (Animation) stg.game.resources.get(spriteRef+"[3]"));
		}
	}
	
	@Override
	public void move(float dt)
	{
		//DEBUG
//		System.out.println("Wanted dir: "+state.wantedDir);
//		if(state.wantedDir!=null)System.out.println("Is direction free: "+isDirectionFree(state.wantedDir));
//		if(state.wantedDir!=null)System.out.println("Is actor within slip threshold: "+isActorWithinSlipThreshold());
		
		//expires wanted dir when specified in options
		if(state.wantedDirTimer != null && state.wantedDirTimer.timeCompletedAndReset())
			state.wantedDir = null;
		
		//update direction according to the user intentions
		if(state.wantedDir != null && isDirectionFree(state.wantedDir) && isWithinSlipThreshold(SLIP_THRESHOLD))
		{
			dir = state.wantedDir;
			state.wantedDir = null;
			this.snapToGrid();
		}
		super.move(dt);
	}
	
	/** Draws player animation */
	@SuppressWarnings("deprecation")
	@Override
	public void draw()
	{
		final Vector2f centerOffset;
		final Image currentFrame;
		if(state.playerDeathEventMode && state.playerDeathEventTimer.thirdPartTimeCompleted())
		{
			// draw dying player
			centerOffset = new Vector2f((dyingSprite.getWidth()-PlayStageState.BLOCK_SIZE)/2, (dyingSprite.getHeight()-PlayStageState.BLOCK_SIZE)/2); //needed to center player sprite
			dyingSprite.draw(body.getX()-centerOffset.x, body.getY()-centerOffset.y);
		}
		else if(!state.enemyDeathEventMode)
		{
			if(spriteType == SpriteType.SYMMETRIC)
			{
				currentFrame = sprites.get(null).getCurrentFrame();
				centerOffset = new Vector2f((currentFrame.getWidth()-PlayStageState.BLOCK_SIZE)/2, (currentFrame.getHeight()-PlayStageState.BLOCK_SIZE)/2); //needed to center player sprite
				switch (dir) {
					case RIGHT: 	currentFrame.setRotation(  0); break; 
					case LEFT: 		currentFrame.setRotation(180); break;
					case UP: 		currentFrame.setRotation(-90); break;
					case DOWN: 		currentFrame.setRotation( 90); break;
				}
				currentFrame.draw(body.getX()-centerOffset.x, body.getY()-centerOffset.y);
				sprites.get(null).updateNoDraw(); //XXX Workaround to trigger auto-update
			}
			else if(spriteType == SpriteType.SEMISYMMETRIC)
			{
				switch (dir) {
				default:case LEFT: 	currentFrame = sprites.get(null).getCurrentFrame().getFlippedCopy(true, false); break;
				case UP: 			currentFrame = sprites.get(null).getCurrentFrame().getFlippedCopy(true, false); currentFrame.setRotation(+90); break;
				case DOWN: 			currentFrame = sprites.get(null).getCurrentFrame().getFlippedCopy(true, false); currentFrame.setRotation(-90); break;
				case RIGHT: 		currentFrame = sprites.get(null).getCurrentFrame(); break; 
				}
				centerOffset = new Vector2f((currentFrame.getWidth()-PlayStageState.BLOCK_SIZE)/2, (currentFrame.getHeight()-PlayStageState.BLOCK_SIZE)/2); //needed to center player sprite
				currentFrame.draw(body.getX()-centerOffset.x, body.getY()-centerOffset.y);
				sprites.get(null).updateNoDraw(); //XXX Workaround to trigger auto-update
			}
			else if(spriteType == SpriteType.ASYMMETRIC)
			{
				currentFrame = sprites.get(dir).getCurrentFrame();
				centerOffset = new Vector2f((currentFrame.getWidth()-PlayStageState.BLOCK_SIZE)/2, (currentFrame.getHeight()-PlayStageState.BLOCK_SIZE)/2); //needed to center player sprite
				currentFrame.draw(body.getX()-centerOffset.x, body.getY()-centerOffset.y);
				sprites.get(dir).updateNoDraw(); //XXX Workaround to trigger auto-update
			}
		}
	}
	
	public Image getLifeIcon()
	{
		if(spriteType == SpriteType.SYMMETRIC || spriteType == SpriteType.SEMISYMMETRIC)
			return sprites.get(null).getImage(sprites.get(null).getFrameCount()-1);
		else if(spriteType == SpriteType.ASYMMETRIC)
			return sprites.get(Direction.RIGHT).getImage(sprites.get(Direction.RIGHT).getFrameCount()-1);
		else
			return null;
	}
}
