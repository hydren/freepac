package entities;

import static states.PlayStageState.BLOCK_SIZE;

import java.util.List;
import java.util.Random;

import org.javatuples.Pair;
import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

import entities.Block.Type;
import entities.Enemy.Target;
import states.PlayStageState;
import util.ColorExtra;
import util.Direction;
import util.Timer;
import util.Util;

/** Behaviors that a enemy may have. */
public abstract class EnemyBehavior 
{
	/** Performs a custom action exclusive for this enemy behavior. */
	protected abstract void customAction(Enemy self, float delta);
	
	/** Returns the actual resolved target by this behavior on the given enemy. <br>
	 *  <b>This method should be used only for debugging purposes as it degrades performance.</b> */
	public abstract Vector2f getResolvedTargetLocation(Enemy self);
	
	/** Performs the action dictated by this behavior on the given enemy and time delta */
	public void action(Enemy self, float delta)
	{
		if(self.sprites.get(self.dir).isStopped()) self.sprites.get(self.dir).start(); // this is needed due to some animation-stopping behaviors
		
		// standard enemy actions
		if(self.houseStaying)
			EnemyBehavior.STAY_HOME.customAction(self, delta);
		
		else if(self.eaten && self.passedFence)
			EnemyBehavior.RETURN_HOME.customAction(self, delta);
		
		else if(self.eaten && !self.passedFence)
		{
			self.aux1 = null; //XXX Kludge to clear a* path when entering enemy house
			EnemyBehavior.ENTER_HOME.customAction(self, delta);
		}
		
		else if(self.passedFence == false)
			EnemyBehavior.LEAVE_HOME.customAction(self, delta);
		
		else if(self.scared)
			EnemyBehavior.RANDOM.customAction(self, delta);
		
		else if(self.scatter)
			EnemyBehavior.SCATTER.customAction(self, delta);
		
		// custom action
		else
			customAction(self, delta);
	}
	
	/** (Debug) Prints a debug text block with the enemy current state */
	@Deprecated
	public String debug_getCurrentBehaviorDescription(Enemy self)
	{
		// standard enemy actions (debug messages)
		if(self.houseStaying)
			return "          In-house             ";
		
		else if(self.eaten && self.passedFence)
			return "Eaten, going to the enemy house";
		
		else if(self.eaten && !self.passedFence)
			return "  Eaten, entering enemy house  ";
		
		else if(self.passedFence == false)
			return "        Leaving home           ";
		
		else if(self.scared)
			return "         Frightened            ";
		
		else if(self.scatter)
			return "         Scattering            ";
		
		// custom action
		else
			return "       Custom behavior         ";
	}
	
	// ****************  special behaviors ****************************
	
	/** Special behavior for enemies inside the enemy house. Goes up and down indefinitely. */
	protected static EnemyBehavior STAY_HOME = new EnemyBehavior() 
	{
		@Override public void customAction(Enemy self, float delta) {
			moveUpAndDown(self, delta);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return null;
		}
	};
	
	/** Special behavior for enemies inside the enemy house. Makes the enemies exit the house by targeting its exit.  */
	protected static EnemyBehavior LEAVE_HOME = new EnemyBehavior() 
	{
		@Override public void customAction(Enemy self, float delta) {
			final Target houseExit = new Target(Util.getSlotAheadDir(self.state.fenceExitSlot, Direction.UP));
			moveFollowTarget(self, delta, houseExit, false);
			if(self.getSlotPosition().equals(Util.getSlotAheadDir(self.state.fenceExitSlot, Direction.UP))) // if enemy reached exit, mark as fence-passed
				self.passedFence = true;
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return new Target(Util.getSlotAheadDir(self.state.fenceExitSlot, Direction.UP)).getCenterPoint();
		}
	};
	
	/** Special behavior for killed enemies. Makes the enemies enter the house by targeting its "center".  */
	protected static EnemyBehavior ENTER_HOME = new EnemyBehavior() 
	{
		@Override public void customAction(Enemy self, float delta) {
			final Pair<Integer, Integer> houseCenterSlot = Util.getSlotAheadDir(self.state.fenceExitSlot, Direction.DOWN);  // maybe 2 slots down?
			moveFollowTarget(self, delta, new Target(houseCenterSlot), false);
			if(self.getSlotPosition().equals(houseCenterSlot)) // if enemy reached inside house, mark as not killed
				self.eaten = false;
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return Util.getSlotCenterPoint(Util.getSlotAheadDir(self.state.fenceExitSlot, Direction.DOWN));
		}
	};
		
	
	/** Special behavior for killed enemies. Makes the enemy to go to the house by targeting its entry.  */
	protected static EnemyBehavior RETURN_HOME = new EnemyBehavior()
	{
		@Override public void customAction(Enemy self, float delta) {
			final Pair<Integer, Integer> houseExitSlot = Util.getSlotAheadDir(self.state.fenceExitSlot, Direction.UP);
			moveFollowTargetAStarPath(self, delta, new Target(houseExitSlot), false);
			if(self.getSlotPosition().equals(Util.getSlotAheadDir(self.state.fenceExitSlot, Direction.UP))) // if enemy reached exit, mark as fence-passed
				self.passedFence = false;
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return new Target(Util.getSlotAheadDir(self.state.fenceExitSlot, Direction.UP)).getCenterPoint();
		}
	};
	
	//~~~~~~~~~~~~~ basic behaviors (classic) ~~~~~~~~~~~~~~
	
	/** Always chooses the direction that leads to the neighbor slot closest to the target */
	public static EnemyBehavior CHASE = new EnemyBehavior() 
	{	
		@Override public void customAction(Enemy self, float delta) {
			moveFollowTarget(self, delta, self.target, false);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return self.target.getCenterPoint();
		}
	};
	
	/** Follows a point in one of the corners of the map (defined according to enemy number) */
	public static EnemyBehavior SCATTER = new EnemyBehavior()
	{
		@Override public void customAction(Enemy self, float delta) {
			moveFollowTarget(self, delta, new Target(getResolvedTargetLocation(self)), false);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			switch (self.state.enemies.indexOf(self)%4) {
			case 0: return self.state.mapLeftTopPosition;
			case 1: return self.state.mapRightTopPosition;
			case 2: return self.state.mapRightBottomPosition;
			case 3: return self.state.mapLeftBottomPosition;
			default: return null;
			}
		}
	};
	
	/** Wanders randomly regarding to targets. This behavior is NOT scatter mode. */
	public static EnemyBehavior RANDOM = new EnemyBehavior() 
	{ 
		@Override public void customAction(Enemy self, float delta) {
			moveRandom(self, delta);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return null;
		}
	};
	 
	//~~~~~~~~~~~~~~ basic behaviors (new) ~~~~~~~~~~~~~~~~~
	
	/** Always chooses the direction that leads to the neighbor slot farthest to the target */
	public static EnemyBehavior AVOID = new EnemyBehavior() 
	{		
		@Override public void customAction(Enemy self, float delta) {
			moveFollowTarget(self, delta, self.target, true);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return self.target.getCenterPoint();
		}
	};
	
	/** Behaves closely like brownian motion. */
	public static EnemyBehavior DIZZY = new EnemyBehavior() 
	{
		@Override public void customAction(Enemy self, float delta) {
			moveDizzy(self, delta);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return null;
		}
	};
	 
	//############## enemy behaviors #############
	
	/** Targets and follows one tile ahead player. */
	public static EnemyBehavior CHASE_AHEAD = new EnemyBehavior() {
		
		@Override
		public void customAction(Enemy self, float delta) {
			moveFollowTarget(self, delta, new Target(this.getResolvedTargetLocation(self)), false);
		}

		// chase front: determinate position 2 slots ahead player (except when player faces up, which the target will be northwest of player)
		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return Util.getSlotCenterPoint(Util.getSlotAheadDir(self.state.player.getSlotPosition(), self.state.player.dir));
		}
	};
	
	/** Targets and follows two tiles ahead player. */
	public static EnemyBehavior CHASE_AHEAD_2 = new EnemyBehavior() {
		
		@Override
		public void customAction(Enemy self, float delta) {
			moveFollowTarget(self, delta, new Target(this.getResolvedTargetLocation(self)), false);
		}

		// chase front: determinate position 2 slots ahead player (except when player faces up, which the target will be northwest of player)
		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return Util.getSlotCenterPoint(Util.getSlotAheadDir(self.state.player.getSlotPosition(), self.state.player.dir, 2));
		}
	};
	
	/** Targets a mirrored vector difference between first enemy and 2 tiles ahead player . */
	public static EnemyBehavior CHASE_PROJECTION_FIRST_ENEMY_ON_PLAYER = new EnemyBehavior() {
		
		@Override public void customAction(Enemy self, float delta) {
			moveFollowTarget(self, delta, new Target(this.getResolvedTargetLocation(self)), false);
		}

		// targets a mirrored vector difference between first enemy and 2 tiles ahead player
		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			Vector2f mirrowed2AheadPlayer = new Vector2f(CHASE_AHEAD_2.getResolvedTargetLocation(self.state.enemies.get(0))).scale(2f);
			mirrowed2AheadPlayer.x -= self.state.enemies.get(0).body.getCenter()[0];
			mirrowed2AheadPlayer.y -= self.state.enemies.get(0).body.getCenter()[1];
			return mirrowed2AheadPlayer;
		}
	};
	
	/** When 8 tiles away from player, chases player. When closer, behaves like scatter mode. */
	public static EnemyBehavior POKEY = new EnemyBehavior() {
		
		@Override public void customAction(Enemy self, float delta) {
			if(Util.distance(self.body.getCenter(), self.state.player.body.getCenter()) > BLOCK_SIZE*8)
				EnemyBehavior.CHASE.customAction(self, delta);
			else
				EnemyBehavior.SCATTER.customAction(self, delta);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			if(Util.distance(self.body.getCenter(), self.state.player.body.getCenter()) > BLOCK_SIZE*8)
				return EnemyBehavior.CHASE.getResolvedTargetLocation(self);
			else
				return EnemyBehavior.SCATTER.getResolvedTargetLocation(self);
		}
	};
	
	/** When 16 tiles away from player, chases player. When closer, behaves like scatter mode. */
	public static EnemyBehavior POKEY_2X = new EnemyBehavior() {
		
		@Override public void customAction(Enemy self, float delta) {
			if(Util.distance(self.body.getCenter(), self.state.player.body.getCenter()) > BLOCK_SIZE*16)
				EnemyBehavior.CHASE.customAction(self, delta);
			else
				EnemyBehavior.SCATTER.customAction(self, delta);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			if(Util.distance(self.body.getCenter(), self.state.player.body.getCenter()) > BLOCK_SIZE*16)
				return EnemyBehavior.CHASE.getResolvedTargetLocation(self);
			else
				return EnemyBehavior.SCATTER.getResolvedTargetLocation(self);
		}
	};
	
	/** Follows a point in one of the corners of the map. The corner shifts every 1 minute.*/
	public static EnemyBehavior RANDOM_CORNER_SCATTER = new EnemyBehavior()
	{
		@Override public void customAction(Enemy self, float delta) {
			moveFollowTarget(self, delta, new Target(getResolvedTargetLocation(self)), false);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			switch (Timer.globalCycle(60000, 4)) {
			case 0: return self.state.mapLeftTopPosition;
			case 1: return self.state.mapRightTopPosition;
			case 2: return self.state.mapRightBottomPosition;
			case 3: return self.state.mapLeftBottomPosition;
			default: return null;
			}
		}
	};
	
	public static EnemyBehavior POKEY_SLOWER_WHEN_CLOSE_FASTER_WHEN_FAR = new EnemyBehavior() {
		
		@Override public void customAction(Enemy self, float delta) {
			if(Util.distance(self.body.getCenter(), self.state.player.body.getCenter()) > BLOCK_SIZE*8)
			{
				self.speed *= 1.6;
				EnemyBehavior.CHASE.customAction(self, delta);
				self.speed *= 0.625;
			}
			else
			{
				self.speed *= 0.625;
				EnemyBehavior.SCATTER.customAction(self, delta);
				self.speed *= 1.6;
			}
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			if(Util.distance(self.body.getCenter(), self.state.player.body.getCenter()) > BLOCK_SIZE*8)
				return EnemyBehavior.CHASE.getResolvedTargetLocation(self);
			else
				return EnemyBehavior.SCATTER.getResolvedTargetLocation(self);
		}
	};
	
	public static EnemyBehavior CHASE_SLOWER_WHEN_CLOSE = new EnemyBehavior() {
		
		@Override public void customAction(Enemy self, float delta) {
			
			if(Util.distance(self.body.getCenter(), self.state.player.body.getCenter()) < BLOCK_SIZE*8)
			{
				final float prevSpeed = self.speed;
				self.speed *= 0.625f;
				EnemyBehavior.CHASE.customAction(self, delta);
				self.speed = prevSpeed;
			}
			else EnemyBehavior.CHASE.customAction(self, delta);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return EnemyBehavior.CHASE.getResolvedTargetLocation(self);
		}
	};
	
	public static EnemyBehavior CHASE_AHEAD_SLOWER_WHEN_CLOSE = new EnemyBehavior() {
		
		@Override public void customAction(Enemy self, float delta) {
			
			if(Util.distance(self.body.getCenter(), self.state.player.body.getCenter()) < BLOCK_SIZE*8)
			{
				final float prevSpeed = self.speed;
				self.speed *= 0.625f;
				EnemyBehavior.CHASE_AHEAD.customAction(self, delta);
				self.speed = prevSpeed;
			}
			else EnemyBehavior.CHASE_AHEAD.customAction(self, delta);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return EnemyBehavior.CHASE_AHEAD.getResolvedTargetLocation(self);
		}
	};
	
	public static EnemyBehavior HALT_WHEN_LOST_TARGET = new EnemyBehavior() {
		
		@Override public void customAction(Enemy self, float delta) {
			
			boolean canSee = false;
			all: for(Direction d : Direction.values())
			{
				for(int i = 0; i < 16; i++)
				{
					final Pair<Integer, Integer> slot = Util.getSlotAheadDir(self.getSlotPosition(), d, i);
					if(!self.state.isSlotGridBoundsSafe(slot)) // out of bounds, dont need to look
						continue all;
					
					if(self.state.grid[slot.getValue0()][slot.getValue1()] != null && self.state.grid[slot.getValue0()][slot.getValue1()].type == Type.WALL) //walls obscures the way
						continue all;
					
					if(slot.equals(Util.getSlotPosition(self.target.getCenterPoint())))
					{
						canSee = true;
						self.dir = d;
						break all;
					}
				}
			}
			
			if(canSee)
			{
				final float prevSpeed = self.speed;
				self.speed *= 2;
				EnemyBehavior.CHASE.customAction(self, delta);
				self.speed = prevSpeed;
				if(self.sprites.get(self.dir).isStopped()) self.sprites.get(self.dir).start();
			}
			else
				self.sprites.get(self.dir).stop();
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return EnemyBehavior.CHASE.getResolvedTargetLocation(self);
		}
	};
	
	public static EnemyBehavior CHASE_FASTER_WHEN_ACQUIRE_TARGET = new EnemyBehavior() {
		
		@Override public void customAction(Enemy self, float delta) {
			
			boolean canSee = false;
			
			for(int i = 0; i < 16; i++)
			{
				final Pair<Integer, Integer> slot = Util.getSlotAheadDir(self.getSlotPosition(), self.dir, i);
				if(!self.state.isSlotGridBoundsSafe(slot)) // out of bounds, dont need to look
					break;
				
				if(self.state.grid[slot.getValue0()][slot.getValue1()] != null && self.state.grid[slot.getValue0()][slot.getValue1()].type == Type.WALL) //walls obscures the way
					break;
				
				if(slot.equals(Util.getSlotPosition(self.target.getCenterPoint())))
				{
					canSee = true;
					break;
				}
			}
			
			if(canSee)
			{
				final float prevSpeed = self.speed;
				self.speed *= 2;
				EnemyBehavior.CHASE.customAction(self, delta);
				self.speed = prevSpeed;
			}
			else
				EnemyBehavior.CHASE.customAction(self, delta);
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return EnemyBehavior.CHASE.getResolvedTargetLocation(self);
		}
	};
	
	public static EnemyBehavior CHASE_FASTER_WHEN_ACQUIRE_TARGET_INTERMITENT_TELEPORT = new EnemyBehavior() {
	
		@Override public void customAction(Enemy self, float delta) 
		{
			if(System.currentTimeMillis() % 90000 < 500) //time to warp
			{
				Random rd = new Random();
				Pair<Integer, Integer> slot = new Pair<Integer, Integer>(0, 0);
				do
				{
					slot.setAt0(rd.nextInt(self.state.grid.length));
					slot.setAt1(rd.nextInt(self.state.grid[0].length));
				} while(!self.state.isSlotGridBoundsSafe(slot)  //must be inside bounds
					  || self.state.grid[slot.getValue0()][slot.getValue1()] == PlayStageState.OUTSIDE_MAP  //must not be outside walkable area
					  || self.state.grid[slot.getValue0()][slot.getValue1()] != null); //must not be in a wall
				
				Vector2f newPos = Util.getSlotCenterPoint(slot);
				self.body.setCenterX(newPos.x); self.body.setCenterY(newPos.y);
				self.state.soundEnemyBeatenBuzzer.play();
			}
			
			if(Timer.globalCycle(5000, 5) == 4)
			{
				self.noClip = true;
				EnemyBehavior.CHASE.customAction(self, delta);
				self.noClip = false;
				self.aux2 =  true;
			}
			else
			{
				if(self.aux2)
				{
					self.snapToGrid();
					self.aux2 = false;
				}
				EnemyBehavior.CHASE_FASTER_WHEN_ACQUIRE_TARGET.customAction(self, delta);
			}
		}
		
		@Override public Vector2f getResolvedTargetLocation(Enemy self)
		{
			return EnemyBehavior.CHASE_FASTER_WHEN_ACQUIRE_TARGET.getResolvedTargetLocation(self);
		}
	};
	
	/** Always chooses the direction that leads to the neighbor slot closest to the target */
	public static EnemyBehavior CHASE_PASSING_WALLS_SLOW = new EnemyBehavior() 
	{	
		@Override public void customAction(Enemy self, float delta) {
			self.noClip = true;
			self.speed *= 0.625;
			moveFollowTarget(self, delta, self.target, false);
			self.noClip = false;
			self.speed *= 1.6;
		}

		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return self.target.getCenterPoint();
		}
	};
	
	//############## classic ghost behaviors #############
	
	/** Classic red ghost ("Blinky"). Behavior same as CHASE. */
	public static EnemyBehavior CLASSIC_SHADOW = CHASE;
	
	/** Classic pink ghost ("Pinky"). Targets and follows two tiles ahead player (except when player faces up, which makes Pinky targets two northwest player. */
	public static EnemyBehavior CLASSIC_SPEEDY = new EnemyBehavior() {
		
		@Override
		public void customAction(Enemy self, float delta) {
			moveFollowTarget(self, delta, new Target(this.getResolvedTargetLocation(self)), false);
		}

		// chase front: determinate position 2 slots ahead player (except when player faces up, which the target will be northwest of player)
		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			return Util.getSlotCenterPoint(Util.getSlotAheadDir(Util.getSlotAheadDir(self.state.player.getSlotPosition(), self.state.player.dir), self.state.player.dir==Direction.UP?Direction.LEFT:self.state.player.dir));
		}
	};
	
	/** Classic blue ghost ("Inky"). Targets a mirrored vector difference between first ghost and 2 tiles ahead player . */
	public static EnemyBehavior CLASSIC_BASHFUL = new EnemyBehavior() {
		
		@Override public void customAction(Enemy self, float delta) {
			moveFollowTarget(self, delta, new Target(this.getResolvedTargetLocation(self)), false);
		}

		// targets a mirrored vector difference between first ghost and 2 tiles ahead player
		@Override public Vector2f getResolvedTargetLocation(Enemy self) {
			Vector2f mirrowed2AheadPlayer = new Vector2f(CLASSIC_SPEEDY.getResolvedTargetLocation(self.state.enemies.get(0))).scale(2f);
			mirrowed2AheadPlayer.x -= self.state.enemies.get(0).body.getCenter()[0];
			mirrowed2AheadPlayer.y -= self.state.enemies.get(0).body.getCenter()[1];
			return mirrowed2AheadPlayer;
		}
	};
	 
	/** Classic orange ghost ("Clyde"). When 8 tiles away from player, chases player. When closer, behaves like scatter mode. */
	public static EnemyBehavior CLASSIC_POKEY = POKEY;
	
	// ================================  Operations  ========================================
	
	/** Continue current direction, choose direction closer (farther if reverse is true) to the target when collides or meets a intersection. 
	 *  If target is null, choose random direction instead (reverse is ignored). */
	protected void moveFollowTarget(Enemy self, float delta, Target target, boolean reverse)
	{
		final Direction[] availableDirections = self.availableDirections();
		
		//DEBUG
		if(availableDirections.length == 0) //stuck
			self.debugColor = Color.red;
		
		else if(availableDirections.length == 1) //dead end
		{
			self.dir = availableDirections[0]; //only now go back
			self.debugColor = Color.orange;
		}
		
		else if(self.isWithinSlipThreshold(Enemy.ENEMY_SLIP_THRESHOLD) && !self.getSlotPosition().equals(self.lastIntersection)) 
		{
			self.lastIntersection = self.getSlotPosition(); //remember last direction change
			
			if(availableDirections.length == 2) //corners or straight
			{
				Direction prevDir = self.dir;
				for(Direction d : availableDirections)
					if(d == prevDir.opposite()) continue; //don't go back
					else { self.dir = d; break;}
				
				if(prevDir!=self.dir) //corner-only
				{
					self.snapToGrid();
				}
				
				self.debugColor = Color.yellow;				
			}
			else //T's or 4-way intersections (availableDirections.length > 2)
			{
				if(target != null)
				{
					// choose new direction to want to move (the shortest possible)
					Direction shortest = null;
					for(Direction d : availableDirections)
					{
						if(d == self.dir.opposite()) continue; //don't go back
						if(shortest == null) { shortest = d; continue;}
						
						//test if given direction gets closer/farther than the current shortest
						if(reverse? self.distanceToTargetFrom2(d, target) > self.distanceToTargetFrom2(shortest, target): self.distanceToTargetFrom2(d, target) < self.distanceToTargetFrom2(shortest, target))
							shortest = d;
					}
					self.dir = shortest;
				}
				else  // since no target was specified, choose random direction (except backwards)
				{
					Direction randomDir = availableDirections[Enemy.random.nextInt(availableDirections.length)];
					if(availableDirections.length > 1) while(randomDir.equals(self.dir.opposite())) randomDir = availableDirections[Enemy.random.nextInt(availableDirections.length)];
					self.dir = randomDir;
				}
				self.snapToGrid();
				self.debugColor = Color.green;
			}
		}
		else self.debugColor = Color.white; //just passing thru
		
		self.move_actor(delta);
	}
	
	/** Moves by going random directions, but following classic player enemies' movement rules. In other words, behaves like moveFollowTarget but instead of the proximity criteria it chooses random directions. */
	protected void moveRandom(Enemy self, float delta)
	{
		moveFollowTarget(self, delta, null, false);
	}
	
	/** Goes up and down, alternating when colliding. If other direction is set, changes to either up or down. */
	protected void moveUpAndDown(Enemy self, float delta)
	{
		// safety corrections (maybe unnecessary)
		if(self.dir == Direction.RIGHT) self.dir = Direction.DOWN;
		if(self.dir == Direction.LEFT) self.dir = Direction.UP;
		
		if(!self.isDirectionFree(self.dir))
			self.dir = self.dir.opposite();

		self.move_actor(delta);
		self.debugColor = ColorExtra.violet;
	}
	
	protected void moveFollowTargetAStarPath(Enemy self, float delta, Target target, Boolean reverse)
	{
		@SuppressWarnings("unchecked")
		List<Pair<Integer, Integer>> path = (List<Pair<Integer, Integer>>) self.aux1;
		
		if(path == null || path.size() < 2) //needs new path (no current path or leftover path)
		{
			self.aux1 = path = Util.fastestPath(self.state, self, target); 
			if(path == null) //no path found
			{
				moveFollowTarget(self, delta, target, false);
				return;
			}
			else if(path.size() > 2) //path big enough, change direction to optimize enemy walk time
			{
				self.snapToGrid();
				for(Direction d : Direction.values())
					if(Util.getSlotAheadDir(path.get(0), d).equals(path.get(1)))
						self.dir = d;
			}
		}
		
		if(self.getSlotPosition().equals(path.get(0))) //if reach the step, seek next step
			path.remove(0);
		
		moveFollowTarget(self, delta, new Target(path.get(0)), false); //follow step
	}
	
	/** Continue current direction, choose random direction when collides. Brownian motions perceived in intersections. */
	protected void moveDizzy(Enemy self, float delta)
	{
		//chance of changing axis when possible
		if(Enemy.random.nextFloat() > 0.9f) switch(self.dir)
		{
			case UP:
			case DOWN:
				Direction.shuffle();
				for(Direction d : Direction.horizontals)
					if(self.isDirectionFree(d))
						self.dir = d;
				break;
			case LEFT:
			case RIGHT:	
				Direction.shuffle();
				for(Direction d : Direction.verticals)
					if(self.isDirectionFree(d))
						self.dir = d;
				break;
		}
		
		Vector2f previousLocation = self.body.getLocation();
		switch(self.dir)
		{
			case DOWN: 	self.body.setY(self.body.getY()+delta*self.speed); break;
			case LEFT: 	self.body.setX(self.body.getX()-delta*self.speed);	break;
			case RIGHT:	self.body.setX(self.body.getX()+delta*self.speed); break;
			case UP:	self.body.setY(self.body.getY()-delta*self.speed); break;
		}
		

		//if the movement does not lead to a collision, we're done here
		if(self.state.isSlotFreeForActor(self.getSlotPosition(), self)) return;

		//enemy collided with wall, try random direction
		
		//restore previous position and direction and try again
		self.body.setLocation(previousLocation);
		
		Direction prevDir = self.dir;
		Direction.shuffle();
		Direction.list.remove(prevDir.opposite()); Direction.list.add(prevDir.opposite()); //moves current's opposite direction to the end, making it the final choice
		
		//DEBUG
		//System.out.println("current dir: " + self.dir.name());
		//for(Direction direction : Direction.list) System.out.println("--"+direction.name());
		
		for(Direction direction : Direction.list)
		{
			switch (direction) 	{
				case DOWN: 	self.body.setY(self.body.getY()+delta*self.speed); self.dir = Direction.DOWN; 	break;
				case LEFT: 	self.body.setX(self.body.getX()-delta*self.speed);	self.dir = Direction.LEFT; 	break;
				case RIGHT:	self.body.setX(self.body.getX()+delta*self.speed); self.dir = Direction.RIGHT;	break;
				case UP:	self.body.setY(self.body.getY()-delta*self.speed); self.dir = Direction.UP; 	break;			
			}
			
			//if the last movement does not lead to a collision, we're done here
			if(self.state.isSlotFreeForActor(self.getSlotPosition(), self)) break;
			
			//restore previous position and direction and try again
			self.body.setLocation(previousLocation);
			self.dir = prevDir;
		}
		
		//DEBUG
		//System.out.println("-Picked: "+self.dir.name());
		//if(self.dir == prevDir.opposite()) System.out.println("Came back!");
		//case it is stuck... well, it is stuck...
	}
	
}
